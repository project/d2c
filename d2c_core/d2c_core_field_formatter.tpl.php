<?php 
foreach ($items as $item) {
  $code = $item['code'];
  $label = $item['label'];
?>

<div class="d2c-core-field-qr-code-formatter">
<?php if ($show_label) { ?>
  <img width="<?= $code_width ?>" src=" <?= d2c_settings_code_image_path($code) ?>" title="<?= $label ?>"/>
<?php } else { ?>
  <img width="<?= $code_width ?>" src=" <?= d2c_settings_code_image_path($code) ?>"/>
<?php } ?>
<?php if ((user_access('download qr codes')) && ($code) && ($show_download_links)) {?>
  <?php $base_icon_path = base_path() . drupal_get_path('module', 'd2c_core') . '/assets/'; ?>
  <?php $base_url_path = "admin/config/d2c/codes/{$code->local_id}/"; ?>
  <div style=" display: block; clear: both;"></div>
  <div class="icons" style="width: <?= $code_width ?>px; text-align: center;">
    <a href="<?= url($base_url_path . 'png') ?>"><img src="<?= $base_icon_path ?>png.png"/></a>
    <a href="<?= url($base_url_path . 'pdf') ?>"><img src="<?= $base_icon_path ?>pdf.png"/></a>
    <a href="<?= url($base_url_path . 'svg') ?>"><img src="<?= $base_icon_path ?>svg.png"/></a>
  </div>
<?php } ?>  
</div>

<?php 
}
?>

<div style=" display: block; clear: both;"></div>
