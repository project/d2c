<?php

define('D2C_BASE_URL', 'http://d2c.es');

/**
 * This class encapsulates all methods offered by dot2code's web services REST API.
 *
 * To make a request to dot2code's servers create an instance of this class and call
 * one of the available methods. Example:
 * @code
 *   $api = new D2CAPI();
 *   $api->get_locales();
 * @endcode
 *
 * To make requests to any method in dot2code's web services REST API that depend on
 * a certain D2C domain, you will have to supply the D2C domain's identifier and, if
 * authentication is required, the D2C domain's secret key, while creating the instance
 * of the class. Example:
 * @code
 *  $api = new D2CAPI('-lKo87', '69mwAYrq8cDXMbZw0KaMgPGvR8V56bgxHSK3UzOZQ0VgZlaHyE');
 *  $api->get_domain();
 * @endcode
 */
class D2CAPI {

  /**
   * Constants for the different D2C QR code types.
   */
  const CODE_TYPE_ANONYMOUS = 0;
  const CODE_TYPE_DISPOSABLE = 1;
  const CODE_TYPE_SINGLE_USER = 2;
  const CODE_TYPE_REUSABLE = 3;
  
  /**
   * Constants for the different display modes.
   */
  const DISPLAY_MOBILE = 0;
  const DISPLAY_DESKTOP = 1;
  
  /**
   * Constant with the number of seconds for which a D2C User request will be valid.
   */
  const TIMESTAMP_VALID_WINDOW = 60;

  /**
   * @var Identifier for the current D2C domain.
   */
  private $domain_id;

  /**
   * @var Secret key for the current D2C domain.
   */
  private $secret_key;
  
  
  /**
   *  Class methods
   */
  
  /**
   * Returns the list of possible D2C QR code types.
   * 
   * @return
   *   An associative array with all possible types and their human-readable names.
   */
  public static function code_types() {
    return array(
      D2CAPI::CODE_TYPE_ANONYMOUS => t('Anonymous'),
      D2CAPI::CODE_TYPE_REUSABLE => t('Reusable'),
      D2CAPI::CODE_TYPE_SINGLE_USER => t('Single-user'),
      D2CAPI::CODE_TYPE_DISPOSABLE => t('Disposable'),
    );
  }
  
  /**
   * Returns the list of possible display modes for an incoming D2C User.
   * 
   * @return
   *   An array with the list of display modes.
   */
  public static function display_modes() {
    return array(
      D2CAPI::DISPLAY_MOBILE => 'mobile',
      D2CAPI::DISPLAY_DESKTOP => 'pc'
    );
  }
  
  /**
   *  Instance methods
   */

  /**
   * Class constructor.
   *
   * @param $domain_id
   *   (optional) A string with the identifier for the current D2C domain.
   * @param $secret_key
   *   (optional) A string with the secret key for the current D2C domain.
   */
  public function __construct($domain_id = NULL, $secret_key = NULL) {
    $this->domain_id = $domain_id;
    $this->secret_key = $secret_key;
  }

  /**
   * Gets the list of available locales in dot2code.
   * 
   * @return
   *   An array with the list of available locales (localized language name), indexed by ISO lang codes.
   */
  public function get_locales() {
    $op = 'get_locales';
    $locales = array();
    foreach($this->send($op) as $locale) {
      $locales[$locale['symbol']] = $locale['name'];
    }
    return $locales;
  }

  /**
   * Gets the list of available profile fields in dot2code's user profiles, with all their information.
   *
   * @param $locale
   *   A string with the locale that will be used to return the user profile field names. Use get_locales() to fetch available values.
   *   Defaults to 'en' (English).
   *
   * @return
   *   An array with the list of available profile fields (localized field name), indexed by their ids. Those ids
   *   will be the ones used in other requests to dot2code's servers to refer to certain profile fields.
   */
  public function get_profile_fields($locale='en') {
    $op = 'get_profile_fields';
    $values = array('locale' => $locale);
    return $this->send($op, $values);
  }

  /**
   * Gets the names for all available profile fields in dot2code's user profiles.
   * 
   * @param $locale
   *   A string with the locale that will be used to return the user profile field names. Use get_locales() to fetch available values.
   *   Defaults to 'en' (English).
   *
   * @return
   *   An array with the list of available profile fields (localized field name), indexed by their ids. Those ids
   *   will be the ones used in other requests to dot2code's servers to refer to certain profile fields.
   */
  public function get_profile_field_names($locale='en') {
    $profile_fields = array();
    foreach($this->get_profile_fields($locale) as $field) {
      $profile_fields[$field['name']] = $field['human_name'];
    }
    return $profile_fields;
  }

  /**
   * Requests a new D2C domain.
   * 
   * @param $domain_info
   *   An associative array containing:
   *   - name: A string with the name for the D2C domain to create.
   *   - backend_url: A string with the URL where the D2C domain will redirect incoming user requests.
   *   - community: A boolean indicating if the D2C domain will be a community domain (true) or a premium domain (false). Defaults to TRUE.
   *   - type: An integer between 1 and 6 with the D2C domain type to be requested. Defaults to 6 (smallest site).
   *   - duration: (optional) An integer with the duration in days for the D2C domain.
   *   - email: A string with the email address where dot2code will send all notifications.
   *   - header: (optional) A string with the path to a file with the image to show in dot2code's login page when a non authenticated user wants
   *       to enter your D2C domain. Normally this should be your site logo. Recommended size: 250 x 75 pixels.
   *   - locale: A string with the preferred locale to be used within notifications and API calls. Use get_locales() to fetch available values.
   *       Defaults to 'en' (English).
   *   - repeatable: A boolean. If false, users may enter only once through any of its D2C QR codes. Defaults to TRUE.
   *   - start: (optional) A string with the activation date for the domain (format=dd-mm-YY).
   *   - profile_entries: (optional). An array with all profile entries for which each user will have to provide a value to access your D2C domain.
   *
   * @param $comments
   *   (optional) A string that will be sent to dot2code's admins along with your request. Try to explain in few words what kind of use you
   *   will give to your D2C domain, the kind of page you will associate it to, etc. This is very important, specially if you ask for a domain
   *   type of 1 to 5, so the administrators can decide if this type is indeed the one that best suits your needs.
   *
   * @return
   *   An indexed array with all configuration info for the domain.
   */
  public function create_domain($domain_info, $comments="") {
    $op = 'create_domain';
    $values = array(
      'domain[name]' => $domain_info['name'],
      'domain[backend_url]' => $domain_info['backend_url'],
      'domain[community]' => isset($domain_info['community']) && $domain_info['community'] == FALSE ? '0' : '1',
      'domain[domain_type]' => isset($domain_info['type']) ? $domain_info['type'] : 6,
      'domain[duration]' => @($domain_info['duration']),
      'domain[email]' => $domain_info['email'],
      'domain[locale]' => isset($domain_info['locale']) ? $domain_info['locale'] : 'en',
      'domain[repeatable]' => isset($domain_info['repeatable']) && $domain_info['repeatable'] == FALSE ? '0' : '1',
      'domain[start]' => @($domain_info['start']),
      'domain[profile_entries][]' => isset($domain_info['profile_entries']) ? $domain_info['profile_entries'] : array(),
      'comments' => $comments,
    );
    if (isset($domain_info['header'])) {
      $values['domain[header]'] = "@{$domain_info['header']}";
    }
    return $this->send($op, $values);
  }

  /*
   * Asks dot2code's servers to send an e-mail with the secret_key for the current domain_id.
   *
   * The e-mail will be sent to the e-mail address that was provided while creating the D2C domain.
   */
  public function send_secret_key() {
    $op = 'send_secret_key';
    $this->send($op);
  }

  /**
   * Gets the configuration info for the current D2C domain.
   *
   * @return
   *  An indexed array with all configuration info for the domain.
   */
  public function get_domain() {
    $op = 'get_domain';
    return $this->send($op);
  }

  /**
   * Edits a d2c domain.
   *
   * @param $domain_info
   *   An associative array containing values for some of the following keys:
   *   - email: A string with the email address where dot2code will send all notifications.
   *   - locale: A string with the preferred locale to be used within notifications and API calls. Use get_locales() to fetch available values.
   *
   * @return
   *   An indexed array with all configuration info for the domain.
   */
  public function edit_domain($domain_info) {
    $op = 'edit_domain';
    $values = array();
    if (isset($domain_info['locale'])) {
      $values['domain[locale]'] = $domain_info['locale'];
    }
    if (isset($domain_info['email'])) {
      $values['domain[email]'] = $domain_info['email'];
    }
    return $this->send($op, $values);    
  }

  /**
   * Closes the current D2C domain.
   *
   * @return
   *   An indexed array with all configuration info for the domain. Status property should equal to 'close' if everything went ok.
   */
  public function delete_domain() {
    $op = 'delete_domain';
    return $this->send($op);
  }

  /**
   * Creates a D2C QR code for the current D2C domain.
   *
   * If asking for a reusable code and an equivalent one (same configuration) is already available, the equivalent code
   * will be returned without creating a new one.
   *
   * @param $url
   *   A string with the url this qr code will point to.
   * @param $extra_data
   *   An associative array with extra info to embed into the qr (variable => value).
   * @param $force_new
   *   A boolean indicating wether if a new code will always be returned or if old codes may be reused under certain
   *   circumstances (only anonymous and reusable codes).
   *
   * @return
   *   An indexed array with the configuration info for the D2C QR code.
   */
  public function create_qr_code($type = D2CAPI::CODE_TYPE_REUSABLE, $extra_data = array(), $force_new = FALSE) {
    $op = 'create_qr_code';
    $values = array();
    $values["code[code_type]"] = $type;
    foreach ($extra_data as $var => $val) {
      $values["code[extra_data][$var]"] = $val;
    }
    $values['force_new'] = $force_new;
    $result = $this->send($op, $values);
    return $result;
  }
  
  /**
   * Gets the specifications of a D2C QR code.
   *
   * @param $id
   *   A string with the D2C QR code id.
   *
   * @return
   *   An associative array with all configuration info for the D2C QR code.
   */
  public function get_qr_code_info($id) {
    $op = 'get_qr_code_info';
    $values = array('qr_code_id' => $id);
    $result = $this->send($op, $values);
    return $result;
  }

  /**
   * Gets the image file for a D2C QR code.
   *
   * @param $format
   *   A string with the desired image format (png | pdf).
   *
   * @return
   *   The image returned by dot2code's servers (image resource for PNGs, raw data for PDFs).
   */
  public function get_qr_code_image($id, $format = 'png') {
    if (!in_array($format, array('png','pdf'))) {
      $format = 'png';
    }
    $op = 'get_qr_code_image';
    $values = array('qr_code_id' => $id, 'qr_code_format' => $format);
    if($format == 'png') {
      return @imagecreatefromstring($this->send($op, $values));
    }
    else {
      return $this->send($op, $values);
    }
  }

  /**
   * Deletes a D2C QR code.
   *
   * @param $id
   *   A string with the D2C QR code id.
   *
   * @return
   *  An associative array with all configuration info for the D2C QR code that has just been deleted.
   */
  public function delete_qr_code($id) {
    $op = 'delete_qr_code';
    $values = array('qr_code_id' => $id);
    $result = $this->send($op, $values);
    return $result;
  }

  /**
   * Starts the registration process in dot2code for a certain user.
   *
   * The user will still have to confirm its new account after receiving the welcome message from dot2code. Otherwise, the registration
   * process will not succeed.
   *
   * @param $user_email
   *   A string with the email address that will identify the user.
   * @param $user_locale
   *   The user's preferred locale. Defaults to 'en' (English). Use get_locales() to fetch available values.
   */
  public function register_user($user_email, $user_locale='en') {
    $op = 'register_user';
    $values = array('entry_id' => 'email', 'value' => $user_email, 'user_locale' => $user_locale);
    $this->send($op, $values);
  }

  /**
   * Authenticates a user request coming from dot2code to your D2C domain.
   *
   * @param $auth_token
   *   A string with the authentication token that came as param in the request.
   *
   * @result
   *  A boolean telling if the user request is legitimate or not.
   */
  public function authenticate_user($auth_token) {
    $op = 'authenticate_user';
    $values = array('auth_token' => $auth_token);    
    $result = $this->send($op, $values);
    return $result;
  }

  /**
   * Sends a request to dot2code's servers with the current domain configuration.
   * 
   * @param $op
   *   A string with the operation to call.
   * @param $values
   *   (optional) An array with all data to include in the request.
   * 
   * @return
   *   An indexed array with the parsed data from the response or a raw string value depending on the called operation.
   */
  private function send($op, $values=array()) {
    return D2CTransport::get_instance()->send($op, $values, $this->domain_id, $this->secret_key);
  }

}

/**
 *
 */
class D2CTransport {
 
  /**
   * @var The D2CTransport singleton reference.
   */
  private static $instance = NULL;

  /**
   * Class constructor.
   */
  private function __construct() {
  }

  /**
   * Gets the singleton object reference.
   *
   * @return
   *   A D2CTransport object.
   */
  public static function get_instance() {
    if (self::$instance === NULL) {
      self::$instance = new D2CTransport();
    }
    return self::$instance;
  }

  /**
   * Calls a D2C API method.
   *
   * @param $op
   *   A string with the operation to be performed.
   * @param $values
   *   An array with the values to include in the POST/GET request.
   * @param $domain_id
   *   NULL or a string with the D2C domain identifier.
   * @param $secret_key
   *   NULL or a string with the secret key associated to the D2C domain identifier.
   *
   * @return
   *   A SimpleXMLElement object when the result of the request is parsed or a string
   *   value.
   *
   * @throws D2CTransportException.  
   */
  public function send($op, $values, $domain_id, $secret_key) {
    $request = $this->get_request_data($op, $values, $domain_id, $secret_key);
    $raw_result = $this->curl_request($request, $values);
    if ($request['parse_result']) {
      $prev_use_internal_errors = libxml_use_internal_errors(TRUE);
      try {
        $result = $this->simple_xml_to_array(new SimpleXMLElement($raw_result));
      } catch (Exception $e) {
        libxml_use_internal_errors($prev_use_internal_errors);
        throw new D2CTransportException("Got an exception while parsing an API request ({$request['url']}) result: {$e->getMessage()}");
      }
      libxml_use_internal_errors($prev_use_internal_errors);
    }
    else {
      $result = $raw_result;
    }
    return $result;
  }

  /**
   * Recursively transforms a SimpleXMLElement object to an indexed array.
   *
   * @param $element
   *   The SimpleXMLElement object to transform into array.
   *
   * @return
   *   An indexed array.
   */
  private function simple_xml_to_array($element) {
    if (is_a($element, 'SimpleXMLElement')) {
      if (count($element->children()) == 0) { // $element->count() is not supported in PHP prior to 5.3.0
        if ($element->attributes()->type == 'boolean') {
          return ((string) $element) == 'true';
        }
        elseif ($element->attributes()->nil == 'true') {
          return NULL;
        }
        elseif ($element->attributes()->type == 'array') {
          return array();
        }
        return (string) $element;
      }
      else {
        $result = array();
        foreach($element->children() as $e => $v) {
          if($element->attributes()->type == 'array') {
            array_push($result, $this->simple_xml_to_array($v));
          }
          else {
            $result[$e] = $this->simple_xml_to_array($v);
          }
        }
        return $result;
      }
    }
    else {
      return $element;
    }
  }

  /**
   * Gets request data for an API operation.
   *
   * @param $op
   *   A string with the API operation.
   * @param $values
   *   An array with the request params, some of them needed to construct some URLs.
   * @param $domain_id
   *   NULL or a string with the D2C domain identifier.
   * @param $secret_key
   *   NULL or a string with the secret key associated to the D2C domain identifier.
   *
   * @return
   *   An associate array with the keys 'url' , 'method', 'auth' and 'parse_result'.
   *   It also may include the keys 'auth_token' and 'timestamp' if the request
   *   needs authentication (auth==TRUE) and 'post_data' with data to send in the
   *   body of POST and PUT requests.
   *
   * @throws D2CTransportException.
   */
  private function get_request_data($op, &$values, $domain_id, $secret_key) {
    $base_url = '/api/v1';
    switch ($op) {
      // Domain ops.
      case 'create_domain':
        $request = array('url' => "{$base_url}/domains.xml",
                         'method' => 'POST',
                         'auth' => FALSE,
                         'parse_result' => TRUE);
        break;
     case 'get_domain':
        $request = array('url' => "{$base_url}/domains/{$domain_id}.xml",
                         'method' => 'GET',
                         'auth' => TRUE,
                         'parse_result' => TRUE);
        break;
     case 'edit_domain':
        $request = array('url' => "{$base_url}/domains/{$domain_id}.xml",
                         'method' => 'PUT',
                         'auth' => TRUE,
                         'parse_result' => TRUE);
        break;
     case 'delete_domain':
        $request = array('url' => "{$base_url}/domains/{$domain_id}.xml",
                         'method' => 'DELETE',
                         'auth' => TRUE,
                         'parse_result' => TRUE);
        break;
     case 'send_secret_key':
        $request = array('url' => "{$base_url}/domains/{$domain_id}/secret_key.xml",
                         'method' => 'POST',
                         'auth' => FALSE,
                         'parse_result' => FALSE);
        break;
     // Code ops.
     case 'create_qr_code':
        $request = array('url' => "{$base_url}/domains/{$domain_id}/codes.xml",
                         'method' => 'POST',
                         'auth' => TRUE,
                         'parse_result' => TRUE);
        break;

     case 'get_qr_code_info':
        $qr_code_id = $values['qr_code_id'];
        unset($values['qr_code_id']);
        $request = array('url' => "{$base_url}/domains/{$domain_id}/codes/{$qr_code_id}.xml",
                         'method' => 'GET',
                         'auth' => TRUE,
                         'parse_result' => TRUE);
        break;
     case 'get_qr_code_image':
        $qr_code_id = $values['qr_code_id'];
        unset($values['qr_code_id']);
        $qr_code_format = $values['qr_code_format'];
        unset($values['qr_code_format']);
        $request = array('url' => "{$base_url}/domains/{$domain_id}/codes/{$qr_code_id}.{$qr_code_format}",
                         'method' => 'GET',
                         'auth' => TRUE,
                         'parse_result' => FALSE);
        break;
     case 'delete_qr_code':
        $qr_code_id = $values['qr_code_id'];
        unset($values['qr_code_id']);
        $request = array('url' => "{$base_url}/domains/{$domain_id}/codes/{$qr_code_id}.xml",
                         'method' => 'DELETE',
                         'auth' => TRUE,
                         'parse_result' => TRUE);
        break;
    // General D2C ops.
     case 'get_profile_fields':
        $request = array('url' => "{$base_url}/profile_entries.xml",
                         'method' => 'GET',
                         'auth' => FALSE,
                         'parse_result' => TRUE);
        break;
     case 'get_locales':
        $request = array('url' => "{$base_url}/locales.xml",
                         'method' => 'GET',
                         'auth' => FALSE,
                         'parse_result' => TRUE);
        break;
     // User registration.
     case 'register_user':
        $request = array('url' => "{$base_url}/domains/{$domain_id}/register_user.xml",
                         'method' => 'POST',
                         'auth' => TRUE,
                         'parse_result' => TRUE);
        break;
     // User authentication.
     case 'authenticate_user':
        $request = array('url' => "{$base_url}/domains/{$domain_id}/authenticate_user.xml",
                         'method' => 'POST',
                         'auth' => TRUE,
                         'parse_result' => TRUE);
        break;
     // Default.
     default:
       throw new D2CTransportException("An unknown API operation has been requested ($op)");
    }
    if (!($request['method'] === 'POST' || $request['method'] === 'PUT') && (count($values) > 0)) {
      $request['url'] .= '?' . http_build_query($values);
    }
    if ($request['auth']) {
      if ($domain_id) {
        if ($secret_key) {
          $this->set_request_auth_data($request, $values, $domain_id, $secret_key);
        }
        else {
          throw new D2CTransportException("An authenticated API operation ($op) without a D2C secret key has been requested");
        }
      }
      else {
        throw new D2CTransportException("An authenticated API operation ($op) without a D2C domain id has been requested");
      }
    }
    else {
      $request['auth'] = NULL;
    }
    return $request;
  }

  /**
   * Creates auth token for authenticated API ops.
   */
  private function set_request_auth_data(&$request, $values, $domain_id, $secret_key) {
    $request['timestamp'] = REQUEST_TIME;
    $body_str = '';
    //  HTTP-Verb Path\n (relative path)
    //  Timestamp\n
    //  Content-MD5 (of the post vars if any)
    $source_string = "{$request['method']} {$request['url']}\n{$request['timestamp']}";
    if (($request['method'] === 'POST' || $request['method'] === 'PUT') && (count($values) > 0)) {
      ksort($values, SORT_STRING);
      foreach($values as $key => $value) {
        if (preg_match('/\[\]$/', $key)) {
          foreach($value as $v) {
            $v = rawurlencode($v);
          }
          $body_str .= '&' . rawurlencode($key) . '=' . implode('&' . rawurlencode($key) . '=', $value);
        }
        elseif (preg_match('/^@(.*)/', $value, $matches)) {
          $body_str .= '&' . rawurlencode($key) . '=' . rawurlencode(base64_encode(md5(get_file_contents($matches[1]))));
        }
        else {
          $body_str .= '&' . rawurlencode($key) . '=' . rawurlencode($value);
        }
      }
      $source_string.= "\n" . base64_encode(md5(ltrim($body_str, '&'), TRUE));
    }
    $request['auth'] = base64_encode(hash_hmac('sha1', $source_string, $secret_key, TRUE));
  }

  /**
   * Sends a D2C API request using CURL.
   */
  function curl_request($request, $values) {
    // Prepare request.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, variable_get('d2c_base_url', D2C_BASE_URL) . $request['url']);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request['method']);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    $headers = array();
    if ($request['auth']) {
      $headers = array("Timestamp: {$request['timestamp']}", "Authorization: D2C {$request['auth']}");
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_FAILONERROR, FALSE);
    curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE); // Just to check if sent headers are OK.
    if ($request['method'] === 'POST' or $request['method'] === 'PUT') {
      d2c_curl_setopt_custom_postfields($ch, $values, $headers);
    }
    // Do request.
    $result = curl_exec($ch);
    // Check request result.
    $error = curl_error($ch);
    if ($error) {
      @curl_close($ch);
      throw new D2CTransportException("Got an error while handling an API request ({$request['url']}): {$error}");
    }
    else {
      $info = curl_getinfo($ch);
      if ((int)$info['http_code'] >= 400) {
        @curl_close($ch);
        throw new D2CTransportException("Got a {$info['http_code']} HTTP error code while handling an API request ({$request['url']})");
      }
    }
    curl_close($ch);
    return $result;
  }

}

/**
 * General D2C API Transport exception.
 */
class D2CTransportException extends Exception {

  /**
   * Class constructor.
   */
  public function __construct($message) {
    parent::__construct($message);
  }

}

/**
 * Custom function to fill POST data in a CURL request with support for both array and file parameters.
 */
function d2c_curl_setopt_custom_postfields($ch, $postfields, $headers = null) {
  
  //Format postfields
  $fields = array();
  foreach ($postfields as $key => $value) {
    if (is_array($value)) {
      foreach ($value as $v) {
        $fields[] = array($key, $v);
      }
    } else {
      $fields[] = array($key, $value);
    }
  }
  
  //Check if there are files to send in the postfields
  $sending_files = FALSE;
  foreach ($fields as $field) {
    list($key, $value) = $field;
    if (strpos($value, '@') === 0) { //File fields start with @
      $sending_files = TRUE;
      break;
    }
  }
  
  //If there are no files to send or no postfields at all, use simple curl method  
  if (count($postfields) == 0 or !$sending_files) {
    $postfields = http_build_query($postfields, '', '&');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($headers, array('Expect: ')));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
    return;
  }
  
  //There are files to send, format post to use multipart method
  $boundary_suffix = ((int) microtime() * 10000000) . rand(10000,99999);  
  $boundary = '----------------------------' . $boundary_suffix;

  $body = array();
  $crlf = "\r\n";

  foreach ($fields as $field) {
    list($key, $value) = $field;
    if (strpos($value, '@') === 0) {
      preg_match('/^@(.*?)$/', $value, $matches);
      list($dummy, $filename) = $matches;
      $body[] = '--' . $boundary;
      $body[] = 'Content-Disposition: form-data; name="' . $key . '"; filename="' . basename($filename) . '"';
      $body[] = 'Content-Type: application/octet-stream';
      $body[] = '';
      $body[] = file_get_contents($filename);
    } else {
      $body[] = '--' . $boundary;
      $body[] = 'Content-Disposition: form-data; name="' . $key . '"';
      $body[] = '';
      $body[] = $value;
    }
  }
  $body[] = '--' . $boundary . '--';
  $body[] = '';
  $contentType = 'multipart/form-data; boundary=' . $boundary;
  $content = join($crlf, $body);
  $contentLength = strlen($content);

  curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($headers, array(
      'Content-Length: ' . $contentLength,
      'Expect: ',
      'Content-Type: ' . $contentType)
    )
  );

  curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
}
