<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

require_once('d2c_domains_actions.inc');
require_once('d2c_campaigns_actions.inc');
require_once('d2c_codes_actions.inc');
require_once('d2c_core_field.inc');
require_once('vcard.inc');

/**
 * Implements hook_help
 */
function d2c_core_help($path, $arg) {
  $output = '';
  switch ($path) {
    case "admin/help#d2c_core":
      $output = '<p>' . t("The D2C Core module provides integration with dot2code's QR code management system (http://dot2code.com).") . '</p>';
      break;
  }
  return $output;
}

/**
 * Implements hook_perm.
 */
function d2c_core_permission() {
  // A single permission is created for all settings administration in this module.
  return array(
    'administer d2c settings' => array(
      'title' => t('Administer d2c settings'),
    ),
    'download qr codes' => array(
      'title' => t('Download existing QR codes in non-default formats such as PDF, SVG, PNG, etc.'),
    ),
  );
}

/**
 * Implements hook_menu
 */
function d2c_core_menu() {
  
  // Settings page.
  $items['admin/config/d2c'] = array(
    'title' => 'D2C dashboard',
    'description' => 'D2C dashboard',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_form'),
    'access arguments' => array('administer d2c settings'),
    'type' => MENU_NORMAL_ITEM
  );
  // Settings page (menu tab).
  $items['admin/config/d2c/settings'] = array( 
    'title' => 'Settings',
    'description' => 'Settings for D2C Core module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_form'),
    'access arguments' => array('administer d2c settings'),
    'type' => MENU_NORMAL_ITEM | MENU_DEFAULT_LOCAL_TASK
  );  
  
  /**
   * D2C domains actions
   */
  
  // List of available domains.
  $items['admin/config/d2c/domains'] = array(
    'title' => 'Domains',
    'description' => 'Manage your D2C domains',
    'page callback' => 'd2c_settings_domains',
    'access arguments' => array('administer d2c settings'),
    'weight' => 1,
    'type' => MENU_NORMAL_ITEM | MENU_LOCAL_TASK
  );
  // Create a new D2C domain in the local system.
  $items['admin/config/d2c/domains/new'] = array(
    'title' => 'New D2C domain',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_new_domain_form'),
    'access arguments' => array('administer d2c settings'),
  );
  // Connect a local D2C domain with a real (remote) D2C domain.
  $items['admin/config/d2c/domains/%/connect'] = array(
    'title' => 'Connect your local domain with a D2C domain',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_connect_domain_form', 4),
    'access arguments' => array('administer d2c settings'),
  );
  // Request a new D2C domain from dot2code's servers.
  $items['admin/config/d2c/domains/%/request'] = array(
    'title' => 'Request a new D2C domain',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_request_domain_form', 4),
    'access arguments' => array('administer d2c settings'),
  );
  // Send secret key to e-mail address.
  $items['admin/config/d2c/domains/%/send_secret'] = array(
    'title' => 'Send secret key for D2C domain',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_send_domain_secret_form', 4),
    'access arguments' => array('administer d2c settings'),
  );
  // Select the default domain.
  $items['admin/config/d2c/domains/%/set_as_default'] = array(
    'page callback' => 'd2c_set_as_default_domain',
    'page arguments' => array(4),
    'access arguments' => array('administer d2c settings'),
    'type' => MENU_CALLBACK,
  );
  // Show configuration for a D2C domain.
  $items['admin/config/d2c/domains/%'] = array(
    'title' => 'D2C Domain properties',
    'page callback' => 'd2c_settings_domain',
    'page arguments' => array(4),
    'access arguments' => array('administer d2c settings'),
  );
  // Edit a D2C domain.
  $items['admin/config/d2c/domains/%/edit'] = array(
    'title' => 'Edit D2C domain properties',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_edit_domain_form', 4),
    'access arguments' => array('administer d2c settings'),
  );
  // Deactivate a D2C domain in dot2code's servers.
  $items['admin/config/d2c/domains/%/deactivate'] = array(
    'title' => 'Deactivate D2C domain',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_deactivate_domain_form', 4),
    'access arguments' => array('administer d2c settings'),
  );
  // Reactivate a D2C domain in dot2code's servers.
  $items['admin/config/d2c/domains/%/reactivate'] = array(
    'title' => 'Reactivate D2C domain',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_reactivate_domain_form', 4),
    'access arguments' => array('administer d2c settings'),
  );
  // Delete a D2C domain from the local system.
  $items['admin/config/d2c/domains/%/delete'] = array(
    'title' => 'Delete D2C domain',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_delete_domain_form', 4),
    'access arguments' => array('administer d2c settings'),
  );
  
  /**
   * D2C Campaigns actions
   */
  
  // List of available campaigns.
  $items['admin/config/d2c/campaigns'] = array(
    'title' => 'Campaigns',
    'description' => 'Create and manage campaigns',
    'page callback' => 'd2c_settings_campaigns',
    'access arguments' => array('administer d2c settings'),
    'weight' => 2,
    'type' => MENU_NORMAL_ITEM | MENU_LOCAL_TASK
  );
  // Create a new D2C campaign.
  $items['admin/config/d2c/campaigns/new'] = array(
    'title' => 'New D2C campaign',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_new_campaign_form'),
    'access arguments' => array('administer d2c settings'),
  );
  // Edit a D2C campaign.
  $items['admin/config/d2c/campaigns/%/edit'] = array(
    'title' => 'Edit D2C campaign properties',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_edit_campaign_form', 4),
    'access arguments' => array('administer d2c settings'),
  );
  // Delete a D2C campaign from the database.
  $items['admin/config/d2c/campaigns/%/delete'] = array(
    'title' => 'Delete D2C campaign',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_delete_campaign_form', 4),
    'access arguments' => array('administer d2c settings'),
  );

  /**
   * QR Codes actions
   */
  
  // List of available QR codes.
  $items['admin/config/d2c/codes'] = array(
    'title' => 'QR codes',
    'description' => 'Create and manage QR codes',
    'page callback' => 'd2c_settings_codes',
    'access arguments' => array('administer d2c settings'),
    'weight' => 3,
    'type' => MENU_NORMAL_ITEM | MENU_LOCAL_TASK
  );
  // Create a new QR code.
  $items['admin/config/d2c/codes/new'] = array(
    'title' => 'New D2C code',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_new_code_form'),
    'access arguments' => array('administer d2c settings'),
  );
  // Toggle activation of a QR code.
  $items['admin/config/d2c/codes/%/toggle_activation'] = array(
    'page callback' => 'd2c_settings_toggle_activation_code',
    'page arguments' => array(4),
    'access arguments' => array('administer d2c settings'),
  );
  // Edit a QR code.
  $items['admin/config/d2c/codes/%/edit'] = array(
    'title' => 'Edit QR code properties',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_edit_code_form', 4),
    'access arguments' => array('administer d2c settings'),
  );
  // Delete a QR code from the database.
  $items['admin/config/d2c/codes/%/delete'] = array(
    'title' => 'Delete QR code',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('d2c_settings_delete_code_form', 4),
    'access arguments' => array('administer d2c settings'),
  );
  // Download large PNG version of a QR code.
  $items['admin/config/d2c/codes/%/png'] = array(
    'title' => t('Download PNG'),
    'page callback' => 'd2c_settings_download_code',
    'page arguments' => array(4, 'png'),
    'access arguments' => array('download qr codes'),
    'type' => MENU_CALLBACK
  );
  // Download large PDF version of a QR code.
  $items['admin/config/d2c/codes/%/pdf'] = array(
    'title' => t('Download PDF'),
    'page callback' => 'd2c_settings_download_code',
    'page arguments' => array(4, 'pdf'),
    'access arguments' => array('download qr codes'),
    'type' => MENU_CALLBACK
  );
  // Download large SVG version of a QR code.
  $items['admin/config/d2c/codes/%/svg'] = array(
    'title' => t('Download SVG'),
    'page callback' => 'd2c_settings_download_code',
    'page arguments' => array(4, 'svg'),
    'access arguments' => array('download qr codes'),
    'type' => MENU_CALLBACK
  );  
  // Autocomplete a QR code name text field.
  $items['admin/config/d2c/codes/autocomplete'] = array(
    'page callback' => 'd2c_core_d2c_codes_autocomplete',
    'access arguments' => array('administer d2c settings'),
    'type' => MENU_CALLBACK
  );
  
  // Autocompletes a node reference text field.
  $items['admin/config/d2c/autocomplete_node'] = array(
    'page callback' => 'd2c_settings_autocomplete_node',
    'access arguments' => array('administer d2c settings'),
    'type' => MENU_CALLBACK
  );

  // Resets all data about D2C in the current Drupal session. 
  $items['d2c/reset'] = array(
    'page callback' => 'd2c_reset',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK
  );
  
  // Access path for users coming from dot2code. 
  $items['d2c/%'] = array(
    'page callback' => 'd2c_user_request',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK
  );
  
  // Direct access to a QR code. 
  $items['d2c/codes/%'] = array(
    'page callback' => 'd2c_core_user_code_request',
    'page arguments' => array(2),
    'access arguments' => array('administer d2c settings'),
    'type' => MENU_CALLBACK
  );
  
  return $items;
  
}

/**
 * Implements hook_cron
 */
function d2c_core_cron() {
  // Reload all domain information from dot2code's servers each day.
  if (d2c_core_get_last_reload_time() <= (REQUEST_TIME - 24 * 60 * 60)) {
    $domains = D2CDomain::all();
    foreach ($domains as $domain) {
      try {
        $domain->remote_reload();
      }
      catch(D2CTransportException $e) {}
    }
    d2c_core_set_last_reload_time(REQUEST_TIME);
  }
  
  // Remove old (1 h.) uncommitted codes.
  $uncommitted_codes = D2CCode::all(D2CCode::StatusUncommitted);
  foreach ($uncommitted_codes as $code) {
    if ($code->local_created_at < (REQUEST_TIME - 60 * 60)) {
      watchdog('d2c_core', 'Auto-removed uncommitted QR code with local id #' . $code->local_id, NULL, WATCHDOG_NOTICE);
      $code->destroy();
    }
  }
}

/**
 * Implements hook_theme
 */
function d2c_core_theme() {
  return array(
    'd2c_core_action_links' => array(
      'variables' => array('links' => array()),
      'template' => 'd2c_core_action_links',
    ),
    'd2c_core_field_widget' => array(
      'variables' => array('code' => NULL),
      'template' => 'd2c_core_field_widget',
    ),
    'd2c_core_field_formatter' => array(
      'variables' => array('items' => array(),
                           'code_width' => NULL, 'show_download_links' => NULL, 'show_label' => NULL),
      'template' => 'd2c_core_field_formatter',
    ),
  );
}

/**
 * Implements hook_file_download
 */
function d2c_core_file_download($uri) {
  // Let all QR code images to be seen.
  if (strpos('/' . $uri, d2c_settings_get_files_path(D2CCode::CodeFilesDirname)) === 0) {
    return array('Content-type: '. file_get_mimetype($uri));
  }
}

/**
 * Implements hook_node_delete().
 */
function d2c_core_node_delete($node) {
  // On node deletion, also delete QR codes whose target is this node.
  db_delete('d2c_code')
    ->condition('behaviour', D2CCode::NodeRedirectionBehaviour)
    ->condition('behaviour_params', sprintf('\'%%nid%%"%d"%%\'', $node->nid), 'LIKE')
    ->execute();
}

/**
 * Implements hook_boot.
 * 
 * Incoming D2C packets should be decrypted & checked asap. While the incoming request
 * is not handled, the raw information about the incoming packet and its status in
 * temporarily stored in the current session.
 */
function d2c_core_boot() {
  // If this is an incoming D2C request, decrypt & check the D2C packet.
  if ((!empty($_GET['q'])) && (!empty($_GET['d2c']))) {
    $_SESSION['d2c_error'] = NULL;
    $_SESSION['d2c_data'] = NULL;
    $_SESSION['d2c_domain'] = NULL;
    $_SESSION['d2c_code'] = NULL;
    if (preg_match('/^d2c\/([0-9]+)$/', $_GET['q'], $matches) == 1) {
      $local_domain_id = $matches[1];
      $domain = D2CDomain::find($local_domain_id);
      if ($domain) {
        $data = d2c_decrypt($domain, $_GET['d2c']);
        if (!empty($data)) {
          $local_id = $data['local_id'];
          $code = D2CCode::find($local_id);
          if (($code) && ($code->active)) {
            // Timestamp and Auth token are checked if configured to do so.
            if (!d2c_core_check_timestamp($data)) {
              $_SESSION['d2c_error'] = 'timestamp';
            }
            elseif (!d2c_core_check_auth_token($data, $domain)) {
              $_SESSION['d2c_error'] = 'auth_token';
            }
            else {
              // No error has arised.
              $_SESSION['d2c_error'] = NULL;
              $_SESSION['d2c_data'] = $data;
              $_SESSION['d2c_domain'] = $domain;
              $_SESSION['d2c_code'] = $code;
            }
          }
          else {
            // Unknown D2C code.
            $_SESSION['d2c_error'] = '404';
          }
        }
        else {
          // Broken D2C packet.
          $_SESSION['d2c_error'] = 'data';
        }
      }
      else {
        // Unknown D2C domain.
        $_SESSION['d2c_error'] = '404';
      }
    }
    else {
      // This is not an incoming D2C request.
      d2c_reset_temporary_data();
    }
  } else {
    // This is not an incoming D2C request.
    d2c_reset_temporary_data();
  }
}

/**
 * Implements custom_theme hook.
 * 
 * This will set the theme for all requests in a D2C session.
 */
function d2c_core_custom_theme() {
  $display_mode = NULL;
  $code_id = NULL;
  $is_d2c_request = FALSE;

  // Is this a valid incoming D2C request?
  if (d2c_is_valid_incoming_d2c_request()) {
    $is_d2c_request = TRUE;
    $display_mode = $_SESSION['d2c_data']['display_mode'];
    $code_id = $_SESSION['d2c_data']['local_id'];
  }
  // Is this a request part of a D2C session?
  elseif (d2c_is_valid_d2c_request()) {
    $d2c_user = d2c_current_user();
    $is_d2c_request = TRUE;
    $display_mode = $d2c_user->display_mode;
    $code_id = $d2c_user->code;
  }

  // Theme selection.
  if ($is_d2c_request === TRUE) {
    // Is there any module overriding the D2C theme to be used for this request?
    $module_themes = module_invoke_all('d2c_core_theme_hook', $code_id);
    if (!empty($module_themes)) {
      return array_pop($module_themes);
    }
    else {
      $d2c_theme = d2c_settings_get_display_mode_associated_theme($display_mode);
      if (!empty($d2c_theme)) {
        return $d2c_theme;
      }
    }
  }
}


/**
 * Page callbacks
 */


/**
 * Page callback for admin/config/d2c
 *
 * List of available domains.
 */
function d2c_settings_form($form) {

  $domains = D2CDomain::all();
  
  if (empty($domains)) {
    drupal_set_message(t('In order to use dot2code\'s services you first have to apply for a D2C domain. Click <a href="@new">here</a> and request now your free domain.', array('@new' => url('admin/config/d2c/domains/new'))), 'error');
  }
  
  global $user;

  $form = array();

  // D2C settings
  $form['d2c_settings'] = array(
    '#title' => t('D2C Core settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE
  );
  
  $form['d2c_settings']['files_path'] = array(
    '#title' => t('Files path'),
    '#type' => 'textfield',
    '#description' => t('This is the path (under %path) where all files used by this module will be stored. <b>Note that changing this path in a running system will move the old directory to the new one and update all the codes\' references in the database.</b>', array('%path' => drupal_realpath(file_default_scheme().'://'))),
    '#required' => TRUE,
    '#default_value' => variable_get('d2c_settings_files_path', 'd2c')
  );
  
  // Theme settings
  $themes_list = array(t('Default'));
  $themes = list_themes();
  foreach ($themes as $theme_name => $theme) {
    if ($theme->status == 1) {
      $themes_list[] = $theme_name;
    }
  }
  foreach(D2CAPI::display_modes() as $display_mode) {
    $form['d2c_settings'][$display_mode . '_theme'] = array(
       '#type' => 'select',
       '#title' => t('Custom theme for ' . $display_mode . ' users'),
       '#options' => drupal_map_assoc($themes_list),
       '#default_value' => d2c_settings_get_display_mode_associated_theme($display_mode)
   );
  }
  
  // Locale settings
  $form['d2c_settings']['d2c_locale_usage'] = array(
    '#title' => t('Set language in user\'s session based on locale info included in incoming D2C requests'),
    '#type' => 'checkbox',
    '#default_value' => d2c_settings_get_d2c_locale_usage(),
    '#description' => t('This will set the language in the user\'s session when arriving from D2C. Remember to enable and configure the session-based language detection <a href="@url">here</a>.', array('@url' => url('admin/config/regional/language/configure')))
  );
  
  // Timestamp check settings
  $form['d2c_settings']['d2c_timestamp_check_enabled'] = array(
    '#title' => t('Always check timestamps to avoid old requests being used.'),
    '#type' => 'checkbox',
    '#default_value' => d2c_core_settings_get_timestamp_check_enabled(),
    '#description' => t('This will enable checking of D2C access tokens timestamps and drop old packets.')
  );
  
  // Timestamp check settings
  $form['d2c_settings']['d2c_auth_token_check_enabled'] = array(
    '#title' => t('Always check D2C auth tokens to validate D2C users.'),
    '#type' => 'checkbox',
    '#default_value' => d2c_core_settings_get_auth_token_check_enabled(),
    '#description' => t('This will make requests to dot2code\'s servers to verify the identity of each incoming D2C user. Enable this feature if you want to avoid replay of hijacked D2C access tokens.')
  );
  
  // Code appearance settings
  $form['d2c_settings']['d2c_raw_codes_enabled'] = array(
    '#title' => t('Use raw QR codes, without any decoration by D2C.'),
    '#type' => 'checkbox',
    '#default_value' => d2c_core_settings_get_raw_codes_enabled()
  );

  foreach (module_implements('d2c_core_custom_settings_hook') as $module) {
    $form = array_merge($form, module_invoke($module, 'd2c_core_custom_settings_hook'));
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#submit' => array('d2c_settings_submit'),
    '#value' => t('Send')
  );

  return $form;

}

function d2c_settings_submit(&$form, &$form_state) {
  
  d2c_settings_set_files_path($form_state['values']['files_path']);
  
  foreach(D2CAPI::display_modes() as $display_mode) {
    print_r($form_state['values'][$display_mode . '_theme']);
    $theme = $form_state['values'][$display_mode . '_theme'];
    if ($theme == t('Default')) {
      $theme = '';
    }
    d2c_settings_set_display_mode_associated_theme($display_mode, $theme);
  }

  d2c_settings_set_d2c_locale_usage($form_state['values']['d2c_locale_usage']);
  
  d2c_core_settings_set_timestamp_check_enabled($form_state['values']['d2c_timestamp_check_enabled']);
  
  d2c_core_settings_set_raw_codes_enabled($form_state['values']['d2c_raw_codes_enabled']);
  
  d2c_core_settings_set_auth_token_check_enabled($form_state['values']['d2c_auth_token_check_enabled']);
  
  module_invoke_all('d2c_core_process_custom_settings_hook', $form_state['values']);
  
  drupal_set_message(t('Settings have been updated.'));
  
}

/**
 * Page callback for admin/config/d2c/autocomplete_node
 * 
 * Provides node search results to an AJAX interface. May be used for node references autocompletion in text fields.
 * Searches can be made by node id or by node title.
 * 
 * @param $string
 *   The current search string.
 */
function d2c_settings_autocomplete_node($string = '') {
  $matches = array();
  if ($string) {
    
    $query = db_select('node', 'n')->addTag('node_access');
    $query = $query
      ->fields('n', array('nid','title'))
      ->condition(db_or()->where('LOWER(title) LIKE LOWER(:string)', array(':string' => $string . '%'))->condition('nid', $string))
      ->range(0,10);
    $result = $query->execute();
    foreach($result as $node) {
    // Add a class wrapper for a few required CSS overrides.
      $matches[$node->nid] = "<div class=\"reference-autocomplete\">". check_plain($node->title) ." [nid:$node->nid]</div>";
    }
  }
  drupal_json_output($matches);
}

/**
 * Page callback for d2c/%
 *
 * Access path for users coming from dot2code. Note that almost all the information
 * required by this function should be found in the current session.s
 */
function d2c_user_request($local_domain_id) {
  $out = '';
  if (empty($_SESSION['d2c_error'])) {
    // Extract input data found in the current session.
    $d2c_data = $_SESSION['d2c_data'];
    $code = $_SESSION['d2c_code'];
    $domain = $_SESSION['d2c_domain'];
    // Preinitialization of the D2C session.
    d2c_core_session_preinitialization_hook($d2c_data, $code, $domain);
    // Other modules might prepare their own states for the process to come.
    module_invoke_all('d2c_core_session_preinitialization_hook', $d2c_data, $code, $domain);
    // Modules may do further initialization actions.
    module_invoke_all('d2c_core_session_initialization_hook', $d2c_data, $code, $domain);
    // Some requests may not return, so clean up session before.
    d2c_reset_temporary_data();
    // The code is finally requested.
    d2c_core_user_code_request($code);
  }
  else {
    switch ($_SESSION['d2c_error']) {
      case '404':
        drupal_set_message(t('Page not found'), 'error');
        break;
      case 'data':
        drupal_set_message(t('Forbidden access: This token contains invalid data.'), 'error');
        $out .= implode(module_invoke_all('d2c_core_session_error_hook', 'data'));
        break;
      case 'timestamp':
        drupal_set_message(t('Forbidden access: This token has expired.'), 'error');
        $out .= implode(module_invoke_all('d2c_core_session_error_hook', 'timestamp'));
        break;
      case 'auth_token':
        drupal_set_message(t('Forbidden access: This token has already been used.'), 'error');
        $out .= implode(module_invoke_all('d2c_core_session_error_hook', 'auth_token'));
        break;
      default:
    }
    // Clean up session.
    d2c_reset_temporary_data();
  }
  // Done!
  return $out;
}

/**
 * Page callback for d2c/codes/%
 *
 * Direct access to a QR code.
 */
function d2c_core_user_code_request($code) {
  // Fetch code (if needed).
  if (!($code instanceof D2CCode)) {
    $local_id = $code;
    $code = D2CCode::find($local_id);
  }
  
  // d2c_core_user_access_hook is invoked. If any of the implementations returns FALSE, the user
  // won't be allowed to access the resource.
  $results = module_invoke_all('d2c_core_user_access_hook', $code);
  foreach ($results as $result) {
    if (!$result) {
      drupal_set_message(t('Forbidden access'), 'error');
      return '';
    }
  }
    
  // d2c_core_user_request_hook is invoked.
  return implode(module_invoke_all('d2c_core_user_request_hook', $code));
}

/**
 * Resets all data about D2C in the current session. 
 */
function d2c_reset() {
  d2c_reset_temporary_data();
  d2c_reset_current_user();
  d2c_reset_current_language();
  drupal_goto();
}

/**
 * Helper functions
 */

/**
 * Preinitialization of session info regarding the incoming user and language to be used.
 */
function d2c_core_session_preinitialization_hook($d2c_data, $code, $domain) {
  // Set language in user's session.
  if ((drupal_multilingual()) && (d2c_settings_get_d2c_locale_usage()) && (!empty($d2c_data['locale']))) {
    $locale = $d2c_data['locale'];
    $languages = language_list();
    if (@$languages[$locale]) {
      d2c_set_current_language($languages[$locale]);
    }
  }
  
  // Set D2C User and QR code info in session.
  $d2c_user = (object) $d2c_data;
  $d2c_user->code = $code->local_id;
  $d2c_user->domain = $domain->local_id;
  d2c_set_current_user($d2c_user);
}

/**
 * Implements d2c_core_user_access_hook.
 * 
 * Checks some preconditions to find out if the D2C user should be allowed to view the content. 
 */
function d2c_core_d2c_core_user_access_hook($code) {
  
  global $user;
  
  if (!user_access('administer d2c settings', $user)) {
    if ((@$code->behaviour_params['selected_roles']) && 
        ($code->behaviour_params['selected_roles'] != 'all') &&
        (!array_sum(array_intersect_key($code->behaviour_params['selected_roles'], $user->roles)))) {
      if ($user->uid == 0) {
        drupal_goto('user', "destination=d2c/codes/" . $code->local_id);
      }
      else {
        return false;
      }
    }
  }
  
}

/**
 * Implements d2c_core_user_request_hook.
 * 
 * Execute QR code configured behaviour.
 */
function d2c_core_d2c_core_user_request_hook($code) {
  global $language;

  switch ($code->behaviour) {
    // Node redirection. User is redirected to the configured node.
    // User's language is taken into account to build the correct URL.
    case D2CCode::NodeRedirectionBehaviour:
      $node = node_load($code->behaviour_params['nid']);
      // If a translation is given for the current language, use it instead.
      if (module_exists('translation') && !empty($node->tnid)) {
        $t_nodes = translation_node_get_translations($node->tnid);
        $t_nid = $t_nodes[$language->language]->nid;
        if ($t_nid && ($t_nid != $node->nid) && ($t_node = node_load($t_nid))) {
          $node = $t_node;
        }
      };
      $path = 'node/' . ($node ? $node->nid : $code->behaviour_params['nid']);
      // If user is anonymous and has no access to the node, redirect to login form. 
      if (!user_is_logged_in() && $node && !node_access('view', $node)) {
        drupal_set_message(t('Please login in order to access this content.'), 'warning');
        drupal_goto('user', array('query' => array('destination' => $path)));
      }
      // Otherwise, redirect the user to the node.
      else {
       drupal_goto(url($path, array('absolute' => TRUE, 'language' => $language)));
      }
      break;      
    // URL redirection. User is redirected to the configured URL. Here we are not
    // using drupal_goto since we want to skip the URL validation to allow redirects
    // to other URI schemes such as market://.
    case D2CCode::URLRedirectionBehaviour:
      header('Location: ' . $code->behaviour_params['url'], TRUE, 302);
      drupal_exit();
      break;
    // Send file. A static file is served to the user.
    case D2CCode::SendFileBehaviour:
      if ($file = file_load($code->behaviour_params['fid'])) {
        file_transfer($file->uri, array(
          'Content-Type' => $file->filemime,
          'Content-Disposition' => "attachment; filename=$file->filename",
          'Content-Length' => $file->filesize)
        );
      }
      break;
    // VCard. A VCard is generated and sent to the user.
    case D2CCode::VCardBehaviour:
      $vc = new vcard();
      $vc->data = array_merge($vc->data, $code->behaviour_params);
      $vc->download();
      drupal_exit();
      break;
    // Custom behaviour
    case D2CCode::CustomBehaviour:      
      break;
  }
  
  return '';
  
}

/**
 * Checks if the current request is part of a D2C session. This method can be used even
 * before the D2C module initialization, with the guarantee of the correcteness
 * of the D2C packet.
 */
function d2c_is_valid_d2c_request() {
  return ((d2c_is_valid_incoming_d2c_request()) || (d2c_current_user() != NULL));
}

/**
 * Checks if the current request is an incoming D2C request. If this functions returns TRUE
 * it is ensured that the packect has been decrypted and its contents validated.
 */
function d2c_is_valid_incoming_d2c_request() {
  return ((!empty($_SESSION['d2c_data'])) && (empty($_SESSION['d2c_error'])));

}

/**
 * Resets all data about D2C in the current session. 
 */
function d2c_reset_temporary_data() {
  if (isset($_SESSION)) {
    $keys = array('d2c_data', 'd2c_error', 'd2c_domain', 'd2c_code');
    foreach ($keys as $key) {
      unset($_SESSION[$key]);
    }
  }
}

/**
 * Returns all retrieved D2C information for the current connected D2C user (if any).
 * 
 * @return
 *   An object with all information or NULL if the current user didn't came from dot2code.
 */
function d2c_current_user() {
  return isset($_SESSION['d2c_user']) ? $_SESSION['d2c_user'] : NULL;
}

/**
 * Sets D2C information for the current connected D2C user.
 * 
 * @param $user
 *   An object with the information.
 */
function d2c_set_current_user($user) {
  $_SESSION['d2c_user'] = $user;
}

function d2c_reset_current_user() {
  unset($_SESSION['d2c_user']);
}

/**
 * Sets D2C language for the current connected D2C user.
 */
function d2c_set_current_language($lang) {
  global $language;
  $language = $lang;
  $param = variable_get('locale_language_negotiation_session_param', 'language');
  $_SESSION[$param] = $language->language;
}

function d2c_reset_current_language() {
  unset($_SESSION[variable_get('locale_language_negotiation_session_param', 'language')]);
}

/**
 * Returns the user profile for the current D2C user (if any).
 * 
 * @return
 *   An indexed array with values for all available profile entries.
 */
function d2c_current_user_profile() {
  $profile = array();
  if (($d2c_user = d2c_current_user()) && ($domain = D2CDomain::find($d2c_user->domain))) {
    foreach($domain->profile_entries as $pe) {
      if ($d2c_user->$pe) {
        $profile[$pe] = $d2c_user->$pe;
      }
    }
  }
  return $profile;
}

/**
 * Check the timestamp included in the D2C user request.
 * 
 * @param $d2c_data
 * 
 * @return
 *   Boolean indicating success or failure of the condition.
 */
function d2c_core_check_timestamp($d2c_data) {
  if (d2c_core_settings_get_timestamp_check_enabled() && (REQUEST_TIME > ((int) $d2c_data['timestamp']) + D2CAPI::TIMESTAMP_VALID_WINDOW)) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Check the auth_token included in the D2C user request.
 * 
 * @param $d2c_data
 * 
 * @return
 *   Boolean indicating success or failure of the condition.
 */
function d2c_core_check_auth_token($d2c_data, $domain) {
  if (d2c_core_settings_get_auth_token_check_enabled() && !($domain->check_auth_token($d2c_data['auth_token']))) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Builds the default files path where all dynamic files for this module will be stored.
 * 
 * This path is configurable by users with the d2c administration role.
 * 
 * @param $subpath
 *   A string with a subpath that will be appended to the built path. This way it is easy
 *   to get full paths to subfolders.
 *   
 * @return
 *   A string with the stream uri to the desired path.
 */
function d2c_settings_get_files_path($subpath = NULL) {
  $path = file_default_scheme() . '://' . variable_get('d2c_settings_files_path', 'd2c');
  if ($subpath) {
    $result_path = $path . '/' . $subpath;
  }
  else {
    $result_path = $path;
  }
  file_prepare_directory($result_path, FILE_MODIFY_PERMISSIONS | FILE_CREATE_DIRECTORY);
  return $result_path;
}

/**
 * Sets the default files path where all dynamic files for this module will be stored.
 * 
 * @param $path
 *   A string with the new path.
 */
function d2c_settings_set_files_path($path) {
  $old_dir_uri = d2c_settings_get_files_path();
  $path = rtrim($path, '/ ');
  $new_dir_uri = file_build_uri($path);
  if($old_dir_uri == $new_dir_uri) {
    return;
  }
  variable_set('d2c_settings_files_path', $path);
  
  //If there was an old dir, rename it to the new dir
  if (file_exists(drupal_realpath($old_dir_uri))) {
      rename(drupal_realpath($old_dir_uri), drupal_realpath($new_dir_uri));
  } 
  
  //Update all codes' URIs to point to the new dir
  $fids = db_query('SELECT fid FROM {file_managed} WHERE uri LIKE :old_dir_uri', array(':old_dir_uri' => db_like($old_dir_uri) . '/%'))->fetchCol();
  $files = file_load_multiple($fids);
  foreach($files as $file){
    $file->uri = str_replace($old_dir_uri, $new_dir_uri, $file->uri);
    file_save($file);
  }
}

function d2c_settings_delete_files_path() {
  variable_del('d2c_settings_files_path');
}

/**
 * Display types' associated themes variables. 
 */

function d2c_settings_get_display_mode_associated_theme($display_mode) {
  return variable_get('d2c_settings_' . $display_mode .'_theme', '');
}

function d2c_settings_set_display_mode_associated_theme($display_mode, $theme) {
  variable_set('d2c_settings_' . $display_mode .'_theme', $theme);
}

function d2c_settings_delete_display_modes_associated_themes() {
  foreach(D2CAPI::display_modes() as $display_mode) {
    variable_del('d2c_settings_' . $display_mode .'_theme');
  }
}

/**
 * D2C locale usage variable. 
 */

function d2c_settings_get_d2c_locale_usage() {
  return variable_get('d2c_settings_d2c_locale_usage', FALSE);
}

function d2c_settings_set_d2c_locale_usage($d2c_locale_usage) {
  variable_set('d2c_settings_d2c_locale_usage', $d2c_locale_usage);
}

function d2c_settings_delete_d2c_locale_usage() {
  variable_del('d2c_settings_d2c_locale_usage');
}


/**
 * Auth token check enabled variable.
 */

function d2c_core_settings_get_auth_token_check_enabled() {
  return variable_get('d2c_core_settings_auth_token_check_enabled', FALSE);
}

function d2c_core_settings_set_auth_token_check_enabled($enabled) {
  variable_set('d2c_core_settings_auth_token_check_enabled', $enabled);
}

function d2c_core_settings_delete_auth_token_check_enabled() {
  variable_del('d2c_core_settings_auth_token_check_enabled');
}

/**
 * Timestamp check enabled variable.
 */

function d2c_core_settings_get_timestamp_check_enabled() {
  return variable_get('d2c_core_settings_timestamp_check_enabled', TRUE);
}

function d2c_core_settings_set_timestamp_check_enabled($enabled) {
  variable_set('d2c_core_settings_timestamp_check_enabled', $enabled);
}

function d2c_core_settings_delete_timestamp_check_enabled() {
  variable_del('d2c_core_settings_timestamp_check_enabled');
}

/**
 * Code appearance variable.
 */
function d2c_core_settings_get_raw_codes_enabled() {
  return variable_get('d2c_core_settings_raw_codes_enabled', TRUE);
}

function d2c_core_settings_set_raw_codes_enabled($enabled) {
  variable_set('d2c_core_settings_raw_codes_enabled', $enabled);
}

function d2c_core_settings_delete_raw_codes_enabled() {
  variable_del('d2c_core_settings_raw_codes_enabled');
}

/**
 * Max code width.
 */
function d2c_core_settings_get_max_code_width() {
  return variable_get('d2c_core_settings_max_code_width', 186);
}

function d2c_core_settings_set_max_code_width($width) {
  variable_set('d2c_core_settings_max_code_width', $width);
}

function d2c_core_settings_delete_max_code_width() {
  variable_del('d2c_core_settings_max_code_width');
}

/**
 * Last reload time variable.
 * 
 * Keeps the last date when a full reload of all D2C domains info was made.
 */

function d2c_core_get_last_reload_time() {
  return variable_get('d2c_core_last_reload_time', 0);
}

function d2c_core_set_last_reload_time($time) {
  variable_set('d2c_core_last_reload_time', $time);
}

function d2c_core_delete_last_reload_time() {
  variable_del('d2c_core_last_reload_time');
}

/**
 * Decrypts text using AES 256, CBC mode. Key and IV calculated d2c-style!
 * 
 * @param $domain
 *   The D2CDomain instance that will be used for the decoding.
 * @param $enc
 *   A string with the (url-safe) base64-encoded binary data.
 *   
 * @return
 *   An associative array with all decoded data. 
 */
function d2c_decrypt($domain, $enc) {
  
  $secret_key = $domain->secret_key;
  
  if(!$secret_key) {
    return array();
  }
  
  // From base64 url-safe encoding to plain base64.
  $enc = str_replace(array('-', '_'), array('+', '/'), $enc);
  $enc = base64_decode($enc); // Assumes base64-encoded binary data.
    
  // KEY is a hash of the secret key. IV is the last 16 chars in the secret key.
  $key = substr(hash("sha256", $secret_key, false), 0, 32);
  $iv = substr($secret_key, strlen($secret_key) - 16);
  
  $decrypted = mcrypt_decrypt('rijndael-128', $key, $enc, 'cbc', $iv);

  $d2c_query_array = explode('&', urldecode(trim($decrypted)));
  
  $d2c_data = array();
  
  foreach($d2c_query_array as $pair) {
    list($k, $v) = explode('=', $pair);
    if ($k && check_plain($v)) {
      $d2c_data[$k] = check_plain($v);
    }
  } 
  
  return $d2c_data;
}
