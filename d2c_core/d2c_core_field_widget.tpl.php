<?php $base_icon_path = base_path() . drupal_get_path('module', 'd2c_core') . '/assets/'; ?>
<?php $base_url_path = "admin/config/d2c/codes/{$code->local_id}/"; ?>
<div class="d2c-core-field-qr-code-widget">
  <img class="code" src=" <?= d2c_settings_code_image_path($code) ?>"/>
  <div style=" display: block; clear: both;"></div>
<?php if (user_access('download qr codes')) {?>
  <div class="icons">
    <a href="<?= url($base_url_path . 'png') ?>"><img src="<?= $base_icon_path ?>png.png"/></a>
    <a href="<?= url($base_url_path . 'pdf') ?>"><img src="<?= $base_icon_path ?>pdf.png"/></a>
    <a href="<?= url($base_url_path . 'svg') ?>"><img src="<?= $base_icon_path ?>svg.png"/></a>
  </div>
  <div style=" display: block; clear: both;"></div>
<?php } ?>  
<?php if (user_access('administer d2c settings')) {?>
  <div class="actions">
    <a href="<?= url($base_url_path . 'edit') ?>"><?= t('customize') ?></a>
  </div>
<?php } ?>  
</div>
