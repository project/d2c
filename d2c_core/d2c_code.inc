<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

module_load_include('inc', 'd2c_core', 'd2c_api');
module_load_include('php', 'd2c_core', 'd2c_qr/D2CQRCodeGenerator');

/**
 * Instances of this class will represent a QR code.
 * 
 * Internal state will include both administrative information (with relevance only locally, like a
 * textual description or a local identifier to do local searches) and a copy of the configuration
 * information about the mapped remote QR code, if any (the same information that is keeped in
 * dot2code unless the local data has become unsynchronised).
 */
class D2CCode {

  /**
   * Constants for the different QR code types.
   */
  const NodeRedirectionBehaviour = 0;
  const URLRedirectionBehaviour = 1;
  const SendFileBehaviour = 2;
  const VCardBehaviour = 3;
  const CustomBehaviour = 4;
  
  /**
   * Constants for the different QR code creation statuses.
   */
  const StatusUncommitted = 0; // Depending on creation time, this code could be purged out.
  const StatusLocallyCommitted = 1; // (default) The remote save is still pending for this code.
  const StatusCommitted = 2; // The code is created both locally & remotely.
  
  const UploadedFileDirname = 'uploads';
  const CodeFilesDirname = 'codes';
  
  
  /**
   * @var A local identifier for the current QR code.
   */
  public $local_id;
  
  /**
   * @var The name of the current QR code. Just for administrative purposes.
   */
  public $name;
  
  /**
   * @var A local description for the QR code. Just for administrative purposes. 
   */
  public $description;
    
  /**
   * @var QR code's real identifier.
   */
  public $id;
  
  /**
   * @var QR code's type as documented in dot2code. This will decide the kind of access control
   * that will be executed in dot2code for all incoming users.
   */
  public $code_type;
  
  /**
   * @var Whether this QR code is locally active or not (only active QR codes will let users
   * access the application).
   */
  public $active;
  
  /**
   * @var QR code's kind of local behaviour that will be executed when a user accesses the domain.
   */
  public $behaviour;
  
  /**
   * @var QR code's kind of local behaviour that will be executed when a user accesses the domain.
   */
  public $behaviour_params;
  
  /**
   * @var Reference to a D2C campaign to which this QR code has been asigned, if any.
   */
  public $campaign_id;
  
  /**
   * @var Reference to the D2C domain in which this QR code has been created.
   */
  public $domain_id;
  
  /**
   * @var Current creation status of the code.
   */
  public $status;
  
  /**
   * @var Local creation timestamp.
   */
  public $local_created_at;
  
  /**
   * @var Remote creation timestamp.
   */
  public $created_at;
  
  
  /**
   *  Class methods
   */
  
  public static function uploaded_files_path() {
    d2c_settings_get_files_path('uploaded');
  }
  
  /**
   * Returns the list of D2C codes in the local database.
   * 
   * @param $status
   *   Status filtering.
   * 
   * @return
   *   An array of D2CCode instances.
   */
  public static function all($status = D2CCode::StatusCommitted) {
    $codes = array();
    $result = db_query("SELECT * FROM {d2c_code} WHERE status=$status ORDER BY created_at DESC");
    foreach($result->fetchAllAssoc('local_id') as $code){
      $codes[] = new D2CCode($code);
    }
    return $codes;
  }
  
  /**
   * Finds D2C codes using their local ids in the local database.
   * 
   * @param $local_ids
   *   An array of integers with the local ids for the target QR codes, or a simple integer if only searching for one. 
   *   
   * @return
   *   An array of D2CCode instances for all codes found. If searched for a single code, a D2CCode instance will be returned
   *   if it has been found. Null will be returned otherwise.
   */
  public static function find($local_ids) {
    $single = FALSE;
    if(!is_array($local_ids)) {
    	$local_ids = array($local_ids);
      $single = TRUE;
    }
    $codes = array();
    if (!empty($local_ids)) {
      $result = db_select('d2c_code')->fields('d2c_code')   //SELECT * FROM d2c_code
          ->condition('local_id', $local_ids, 'IN') //WHERE local_id IN (...)
          ->orderBy('created_at','DESC')
          ->execute();
      foreach($result as $attributes) {
        $codes[$attributes->local_id] = new D2CCode((array) $attributes);
      } 
    }   
    if (!$single) {
      return $codes;
    } else if (count($codes) == 1) {
      return reset($codes);
    } else {
      return NULL;
    }
  }
  
  /**
   * Returns the list of D2C codes in the local database, filtering by D2C domain and/or D2C campaign.
   * 
   * @param $domain_id
   *   (optional)
   * @param $campaign_id
   *   (optional)
   * @param $behaviour
   *   (optional)
   * @param $code_type
   *   (optional)
   * @param $page
   * @param $per_page
   * 
   * @return
   *   An array of D2CCode instances.
   */
  public static function filter($domain_id = NULL, $campaign_id = NULL, $name = NULL, $behaviour = NULL, $code_type = NULL, $active = NULL, $page = 0, $per_page = 10, $extra = 0, $committed_only = TRUE) {
    $codes = array();
    $query = db_select('d2c_code')->fields('d2c_code'); //SELECT * FROM d2c_code
    
    if ($committed_only === TRUE) {
      $query = $query->condition('status', D2CCode::StatusCommitted);
    }
    if (!is_null($name) and !($name == '')) {
      $query = $query->condition('name', '%' . db_like($name) . '%', 'LIKE');
    }
    if ($domain_id) {
      $query = $query->condition('domain_id', $domain_id);      
    }
    if (!is_null($campaign_id) and !($campaign_id == '')) {
      if ($campaign_id) {
        $query = $query->condition('campaign_id', $campaign_id);
      } else {
        $query = $query->condition(db_or()->condition('campaign_id', NULL)->condition('campaign_id', ''));
      }
    }
    if (!is_null($behaviour) and !($behaviour == '')) {
      $query = $query->condition('behaviour', $behaviour);
    }
    if (!is_null($code_type) and !($code_type == '')) {
      $query = $query->condition('code_type', $code_type);
    }
    if (!is_null($active) and !($active == '')) {
      $query = $query->condition('active', $active);
    }
    $query = $query->orderBy('created_at', 'DESC');
    $query = $query->range($page * $per_page, $per_page + $extra); 
    
    $result = $query->execute();    
    foreach($result->fetchAllAssoc('local_id') as $code){
      $codes[] = new D2CCode($code);
    }
    return $codes;
  }
  
  /**
   * Deletes a D2C code, both in dot2code's servers and in the local database.
   * 
   * @param $local_id
   *   An integer with the local id of the QR code to delete.
   */
  public static function delete($local_id) {
    if ($code = D2CCode::find($local_id)) {
      $code->destroy();
    }
  }
  
  /**
   * Returns the list of possible behaviours for a QR code.
   * 
   * @return
   *   An associative array with all possible behaviours a QR code may
   *   have and their human-readable names.
   */
  public static function behaviours() {
    return array(
      D2CCode::NodeRedirectionBehaviour => t('Node redirection'),
      D2CCode::URLRedirectionBehaviour => t('URL redirection'),
      D2CCode::SendFileBehaviour => t('Send file'),
      D2CCode::VCardBehaviour => t('VCard'),
      D2CCode::CustomBehaviour => t('Custom'),
    );
  }
  
  
  /**
   *  Instance methods
   */
  
  /**
   * Class constructor.
   *
   * Default values will be given to all attributes unless explicit values are provided for them.
   *
   * @param $attributes
   *   (optional) An associative array with values for one or more of the QR code's attributes. 
   */
  public function __construct($attributes = array()) {
    $this->set_defaults();
    $this->set_attributes($attributes);
  }
  
  /**
   * Sets values for all provided attributes.
   * 
   * Unkown attributes will be ignored.
   * 
   * @param $attributes
   *   An associative array with values for one or more of the QR code's attributes.
   */
  public function set_attributes($attributes) {
    foreach($attributes as $attr => $value) {
      try {
        // Behaviour params data is unserialized if needed.
        if (($attr == 'behaviour_params') and is_string($value)) {
          $value = unserialize($value);
        }
        // Changing the behaviour params and a file was already defined for SendFileBehaviour.
        if ($attr == 'behaviour_params' && $this->behaviour_params && @$this->behaviour_params['fid']) {
          // fid = 0: Old file is preserved. Otherwise, the old file is deleted.
          if ($value['fid'] == 0) {
            $value['fid'] = $this->behaviour_params['fid'];
          } elseif ($old_file = file_load($this->behaviour_params['fid'])) {
            db_delete('file_managed')->condition('fid', $old_file->fid)->execute();
            db_delete('file_usage')->condition('fid', $old_file->fid)->execute();
            // We must be careful. If a new file was given and it has replaced the old one, a delete operation is not needed.
            $new_file = file_load($value['fid']);
            if (!$new_file or ($new_file->uri != $old_file->uri)) {
              file_unmanaged_delete($old_file->uri);
            }           
          }
        }
        $this->$attr = $value;
      }
      catch (Exception $e) {}
    }
  }
  
  /**
   * Returns all values in this instance for the list of attributes returned by function attribute_names().
   * 
   * @return
   *   An associative array with attribute names as keys and their current values.
   */
  public function attributes() {
    $attributes = array();
    foreach(D2CCode::attribute_names() as $attribute) {
      $attributes[$attribute] = $this->$attribute;
    }
    return $attributes;
  }
  
  /**
   * Saves the instance to the local database. This can be both an INSERT (for new instances) or an UPDATE.
   * 
   * @return
   *   Failure to write a record will return FALSE. Otherwise SAVED_NEW or SAVED_UPDATED is returned
   *   depending on the operation performed.
   */
  public function save() {
    return drupal_write_record('d2c_code', $this, $this->local_id? "local_id" : array());
  }
  
  /**
   * Sends all current QR code's data to dot2code.
   * 
   * This can be both a QR code creation operation (for new instances) or a QR code edit operation (for
   * already existent QR codes in dot2code). On success, the status of the code is updated.
   */
  public function remote_save() {
    $domain = $this->domain();
    if($domain) {
      if (($this->id) && ($this->status == D2CCode::StatusCommitted)) {
        // @todo Edition of QR codes already stored remotely
        // $api = new D2CAPI($domain->id, $domain->secret_key);
        // $this->set_attributes($api->edit_code($this->attributes()));
      }
      else {
        $api = new D2CAPI($domain->id, $domain->secret_key);
        // We do a first save to the database to store any local changes and get
        // a proper local_id (if we don't have one yet).
        $this->save();
        try {
          $this->set_attributes($api->create_qr_code($this->code_type, array('local_id' => $this->local_id), TRUE));
          $this->status = D2CCode::StatusCommitted;
        } catch (D2CTransportException $e) {
          $this->local_destroy();
          throw $e;
        }
      }
      $this->save();
    }
  }
  
  /**
   * Locally activates this QR code.
   */
  public function activate() {
    $this->active = true;
    $this->save();
  }
  
  /**
   * Locally deactivates this QR code.
   */
  public function deactivate() {
    $this->active = false;
    $this->save();
  }
  
  /**
   * Deletes this QR code both locally and in dot2code's servers.
   */
  public function destroy() {
    if ($this->status == D2CCode::StatusCommitted) {
      $domain = $this->domain();
      if($domain) {
        $api = new D2CAPI($domain->id, $domain->secret_key);
        $api->delete_qr_code($this->id);
      }
    }
    $this->local_destroy();
  }
  
  /**
   * Local deletion of the QR code.
   */
  public function local_destroy() {
    module_invoke_all('d2c_core_delete_code_hook', $this->local_id);
    db_delete('d2c_code')->condition('local_id', $this->local_id)->execute();
    // QR code image is deleted.
    if ($image_uri = $this->image()) {
      $image_file_fid = db_query('SELECT fid FROM {file_managed} WHERE uri = :uri', array(':uri' => $image_uri))->fetchField();
      $file = file_load($image_file_fid);
      file_delete($file);
    }
    // Any attached file is also deleted.
    if (@$this->behaviour_params['fid'] and ($uploaded_file = file_load($this->behaviour_params['fid']))) {
      file_delete($uploaded_file);
    }
  }
  
  /**
   * D2C campaign to which this QR code has been asigned, if any.
   * 
   * @return
   *   An instance of D2CCampaign or NULL if this QR Code has not been assigned
   *   to any D2C campaign.
   */
  public function campaign() {
    if ($this->campaign_id) {
      return D2CCampaign::find($this->campaign_id);
    }
    else {
      return NULL;
    }
  }

  /**
   * D2C domain in which this QR code has been created.
   * 
   * @return
   *   An instance of D2CDomain or NULL if the D2C domain
   *   hasn't been yet assigned.
   */
  public function domain() {
    if ($this->domain_id) {
      return D2CDomain::find($this->domain_id);
    }
    else {
      return NULL;
    }
  }
  
  public function image() {
    if ($this->status != D2CCode::StatusCommitted) {
      return NULL;
    }
    global $user;
    $dir_uri = d2c_settings_get_files_path(D2CCode::CodeFilesDirname);
    $file_name = $this->local_id . '.png';
    $file_uri = "$dir_uri/$file_name";
    if (is_file($file_uri)) {
      return $file_uri;
    }
    else {
      $success = FALSE;
      $file_path = drupal_realpath($file_uri);
      if (d2c_core_settings_get_raw_codes_enabled()) {
        if (D2CQRCodeGenerator::getInstance()->png($this->id,
                                                   array('output' => $file_path,
                                                         'margin' => 1,
                                                         'max_code_side' => d2c_core_settings_get_max_code_width())) === TRUE) {
          $success = TRUE;
        }
      }
      else {
        try {
          $domain = $this->domain();
          $api = new D2CAPI($domain->id, $domain->secret_key);        
          $image = $api->get_qr_code_image($this->id, 'png');
          if (imagepng($image, $file_path)) {
            $success = TRUE;
          }
        }
        catch(D2CTransportException $e) {
        }
      }
      // Register file as stored locally.
      if ($success === TRUE) {
        $file_path = drupal_realpath($file_uri);
        // Any old entry in the files table for this QR code image is removed to avoid duplicates.
        $old_fid = db_query('SELECT fid FROM {file_managed} WHERE uri = :uri', array(':uri' => $file_uri))->fetchField();
        if ($old_fid) {
          db_delete('file_managed')->condition('fid', $old_fid)->execute();
          db_delete('file_usage')->condition('fid', $old_fid)->execute();
        }
        $file = new stdClass();
        $file->uid = $user->uid;
        $file->filename = $file_name;
        $file->uri = $file_uri;
        $file->filemime = file_get_mimetype($file_name);
        $file->filesize = filesize($file_uri);
        $file->status = FILE_STATUS_PERMANENT;
        $file->timestamp = REQUEST_TIME;
        file_save($file);
        return $file_uri;
      }
      return NULL;
    }
  }

  public function stream($format) {
    if ($this->status == D2CCode::StatusCommitted) {
      if (D2CQRCodeGenerator::getInstance()->$format($this->id,
                                                     array('name' => $this->name . ' (' . $this->id . ').' . $format,
                                                           'margin' => 2,
                                                           'max_code_side' => 1024)) !== TRUE) {
        header('HTTP/1.0 500 Internal error while generating code');
      }
    }
    else {
      header('HTTP/1.0 404 Not found');
    }
  }  
  
  /**
   *  Private methods
   */
  
  /**
   * Sets default values for the attributes in this instance.
   */
  private function set_defaults() {
    $this->local_created_at = REQUEST_TIME;
    $this->status = D2CCode::StatusLocallyCommitted;
    $this->code_type = D2CAPI::CODE_TYPE_REUSABLE;
    $this->active = TRUE;
    $default_domain = D2CDomain::default_domain();
    if ($default_domain) {
      $this->domain_id = $default_domain->local_id;
    }
  }
  
  /**
   * Returns the list of D2C domain attributes that dot2code can understand.
   * 
   * @return
   *   An array with the name of all attributes used in remote operations.
   */
  private static function attribute_names() {
    return array('id', 'code_type'); // @todo extra_data
  }
  
}

?>
