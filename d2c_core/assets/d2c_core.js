(function ($) {
  Drupal.behaviors.d2cCoreBehavior = {
    attach: function(context){
      /*
       * JS for code_behaviour_selection
       */
      // When a different behaviour type is selected, its configuration fields are shown.
      $('.d2c-code-behaviour-selection').bind('change', function() {
    	var wrapper = $(this).closest('.d2c-code-behaviour-wrapper');
        $(wrapper).find('.behaviour_param').hide();
        $(wrapper).find('#behaviour_params_for_' + this.value).show();
      });

      //
      $('table.d2c-codes input.toggle').bind('click', function() {
        window.location = "?q=admin/config/d2c/codes/" + this.id.substring(7) + "/toggle_activation";
      });
      
      /*
       * Helper functions
       */
      // Clear a form.
      $.fn.clearForm = function() {
        return this.each(function() {
          var type = this.type, tag = this.tagName.toLowerCase();
          if (tag == 'form') 
            return $(':input', this).clearForm();
          if (type == 'text' || type == 'password' || tag == 'textarea') 
            this.value = '';
          else 
            if (type == 'checkbox' || type == 'radio') 
              this.checked = false;
            else 
              if (tag == 'select') 
                this.selectedIndex = -1;
        });
      };
    }
  }
})(jQuery);