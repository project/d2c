<?php foreach($links as $link) { ?>
  <a href="<?php print $link['target'] ?>">
    <?php $icon = base_path() . drupal_get_path('module', 'd2c_core') . '/assets/' . $link['title'] . '.png' ?>
    <img src="<?php print $icon ?>" alt="<?php print t($link['title']) ?>" title="<?php print t($link['title']) ?>" />
  </a><br/>
<?php } ?>