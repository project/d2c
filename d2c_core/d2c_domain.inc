<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

module_load_include('inc', 'd2c_core', 'd2c_api');

/**
 * Instances of this class will represent a D2C domain in the local database.
 *
 * This local domains may or may not have been mapped to a real (remote) D2C domain at dot2code.
 * 
 * Internal state will include both administrative information (with relevance only locally, like a
 * textual description or a local identifier to do local searches) and a copy of the configuration
 * information about the mapped remote D2C domain, if any (the same information that is keeped in
 * dot2code unless the local data has become unsynchronised).
 */
class D2CDomain {
  
  /**
   * @var A local identifier for the current D2C domain.
   */
  public $local_id;
  
  /**
   * @var The name of the current D2C domain. This value is used both locally
   *   for administrative purposes and in dot2code.
   */
  public $name;
  
  /**
   * @var A local description for the D2C domain. Just for administrative purposes. 
   */
  public $description;
    
  /**
   * @var D2C domain's real identifier.
   */
  public $id;
  
  /**
   * @var D2C domain's secret_key.
   */
  public $secret_key;
  
  /**
   * Other D2C domain's attributes.
   */
  public $backend_url;
  public $community;
  public $duration;
  public $email;
  public $header;
  public $locale;
  public $repeatable;
  public $status;
  public $domain_type;
  public $start;  
  public $profile_entries;
  
  
  /**
   *  Class methods
   */
  
  /**
   * Gets the list of available locales in dot2code.
   * 
   * @param $update
   *   A boolean value that indicates if a cached value may be used. Defaults to FALSE (a request to dot2code's servers will always be made).
   *
   * @return
   *   An array with the list of available locales (localized language name), indexed by ISO lang codes.
   */
  public static function locales($update=FALSE) {
    $locales = variable_get('d2c_core_locales', FALSE);
    if ($locales && !$update) {
      return $locales;
    }
    else {
      $api = new D2CAPI();
      try {
        $locales = $api->get_locales();
      }
      catch(D2CTransportException $e) {
        drupal_set_message(t('There has been an error trying to fetch the available locales in dot2code. You may try again later, or use the default English locale.'), 'error');
        return array('en' => 'English');
      }
      variable_set('d2c_core_locales', $locales);
      return $locales;
    }
  }

  /**
   * Gets the list of available profile fields in dot2code.
   * 
   * @param $update
   *   A boolean value that indicates if a cached value may be used. Defaults to FALSE (a request to dot2code's servers will always be made).
   *
   * @return
   *   An array with the list of available profile fields (localized field name), indexed by their ids. Those ids
   *   will be the ones used in other requests to dot2code's servers to refer to certain profile fields.
   */
  public static function profile_fields($update=FALSE) {
    $fields = variable_get('d2c_core_profile_fields', FALSE);
    if ($fields && !$update) {
      return $fields;
    }
    else {
      $api = new D2CAPI();
      try {
        $fields = $api->get_profile_field_names();
      }
      catch(D2CTransportException $e) {
        drupal_set_message(t('There has been an error trying to fetch the available profile fields in dot2code. You may try again later.'), 'error');
        return array();
      }
      variable_set('d2c_core_profile_fields', $fields);
      return $fields;
    }
  }
  
  /**
   * Returns the list of D2C domains in the local database.
   * 
   * @return
   *   An array of D2CDomain instances.
   */
  public static function all() {
    $domains = array();
    $result = db_query("SELECT * FROM {d2c_domain}");
    foreach ($result->fetchAllAssoc('local_id') as $domain){
      $domains[] = new D2CDomain($domain);
    }
    return $domains;
  }
  
  /**
   * Finds a certain D2C domain in the local database.
   * 
   * @param $local_id
   *   An integer with the local id for the target D2C domain. 
   *   
   * @return
   *   A D2CDomain instance if the domain was found. Null, otherwise.
   */
  public static function find($local_id) {
    $result = db_query("SELECT * FROM {d2c_domain} WHERE local_id = :local_id", array(':local_id'=>$local_id));
    if ($result) {
      $attributes = $result->fetchAssoc();
      if ($attributes) {
        $attributes['profile_entries'] = unserialize($attributes['profile_entries']);
        return new D2CDomain($attributes);
      }
    }
    return NULL;
  }
  
  /**
   * Deletes a D2C domain from the local database and all its QR codes.
   * 
   * @param $local_id
   *   An integer with the local id of the D2C domain to delete.
   */
  public static function delete($local_id) {
    if ($domain = D2CDomain::find($local_id)) {
      $domain->destroy();
    }
  }
  
  /**
   * Returns the default domain, if there is any available, properly connected to a
   * remote D2C domain and not in a closed status.
   * 
   * @return
   *   A D2CDomain instance if a default domain has been found. Null otherwise.
   */
  public static function default_domain() {
    $default_domain_local_id = variable_get("d2c_core_default_domain", NULL);
    if($default_domain_local_id) {
      $default_domain = D2CDomain::find($default_domain_local_id);
      if($default_domain && $default_domain->is_usable()) {
        return $default_domain;
      }
    }
    return D2CDomain::set_default_domain();
  }
  
  /**
   * Sets the first eligible D2C domain (that is, anyone that is not closed) as default.
   * 
   * @return
   *   A D2CDomain instance of the D2C domain that has been set as the default one.
   */
  public static function set_default_domain() {
    variable_del("d2c_core_default_domain");
    foreach(D2CDomain::all() as $domain) {
      $domain->set_as_default();
      if (variable_get('d2c_core_default_domain', NULL)) {
        return $domain;
      }
    }
    return NULL;
  }
  
  /**
   * Returns all usable D2C domains (that is, anyone that is properly connected and not closed).
   * 
   * @return
   *   An array of D2CDomain instances.
   */
  public static function usable_domains() {
    $domains = D2CDomain::all();
    $filter = create_function('$d', 'return $d->is_usable();');
    return array_filter($domains, $filter);
  }
  
  
  /**
   *  Instance methods
   */
  
  /**
   * Class constructor.
   *
   * Default values will be given to all attributes unless explicit values are provided for them.
   *
   * @param $attributes
   *   (optional) An associative array with values for one or more of the D2C domain's attributes. 
   */
  public function __construct($attributes = array()) {
    $this->set_defaults();
    $this->set_attributes($attributes);
  }
  
  /**
   * Sets values for all provided attributes.
   * 
   * Unkown attributes will be ignored.
   * 
   * @param $attributes
   *   An associative array with values for one or more of the D2C domain's attributes.
   */
  public function set_attributes($attributes) {
    foreach($attributes as $attr => $value) {
      try {
        $this->$attr = $value;
      }
      catch (Exception $e) {}
    }
  }
  
  /**
   * Returns all values in this instance for the list of attributes returned by function attribute_names().
   * 
   * @return
   *   An associative array with attribute names as keys and their current values.
   */
  public function attributes() {
    $attributes = array();
    foreach(D2CDomain::attribute_names() as $attribute) {
      $attributes[$attribute] = $this->$attribute;
    }
    return $attributes;
  }
  
  /**
   * Deletes this D2C Domain locally.
   */
  public function destroy() {
    db_delete('d2c_domain')->condition('local_id', $this->local_id)->execute();
    db_delete('d2c_code')->condition('domain_id', $this->local_id)->execute();
    if ($this->header && ($file = file_load($this->header))) {
      file_delete($file);
    }
  }
  
  /**
   * Returns an array with all QR codes generated by this domain.
   * 
   * @return
   *   An array of D2CCode instances.
   */
  public function codes() {
    $codes = array();
    $result = db_query('SELECT * FROM {d2c_code} WHERE domain_id = :local_id', array(':local_id' => $this->local_id));
    foreach ($result->fetchAllAssoc('local_id') as $code){
      $codes[] = new D2CCode($code);
    }
    return $codes;
  }
  
  /**
   * Returns the number of QR codes generated by this domain, from this application.
   * 
   * @return
   *   An int with the number of QR codes in this domain.
   */
  public function used_codes() {
    $query = db_select('d2c_code');
    $count = $query->
      condition('domain_id',$this->local_id)->
      condition('status', D2CCode::StatusCommitted)->
      countQuery()->execute()->fetchField();
    return $count;
  }
  
  /**
   * Returns the number of QR codes that this domain can still generate.
   * 
   * This is an approximation that takes into account the maximum amount of
   * QR codes that a D2C domain of the current type can generate. It won't
   * be accurate if this DC2 domain generates QR codes from outside the scope
   * of this application.
   * 
   * @return
   *   An int with the number of QR codes left for this D2C domain. 0 if the
   *   local D2C domain is not connected to a real remote D2C domain in dot2code yet.
   */
  public function codes_left() {
    if ($this->domain_type) {
      return pow(64, (8 - $this->domain_type)) - $this->used_codes();
    } else {
      return 0;
    }
  }
  
  /**
   * Returns the path to the header image file, if any is available for this domain.
   * 
   * @return
   *   A string with the path to the file.
   */
  public function header_image() {
    if ($this->header && ($file = file_load($this->header))) {
      return drupal_realpath($file->uri);
    }
    else {
      return NULL;
    }
  }
  
  /**
   * Determines if the D2C domain can be used to generate QR codes at its current status.
   * 
   * @return
   *   A boolean indicating wether the D2C domain is usable or not.
   */
  public function is_usable() {
    return $this->status == 'accepted' || $this->status == 'active';
  }
  
  /**
   * Sets this domain as default unless it is in a closed status.
   */
  public function set_as_default() {
    if ($this->is_usable()) {
      variable_set("d2c_core_default_domain", $this->local_id);
    }
  }
  
  /**
   * Saves the instance to the local database. This can be both an INSERT (for new instances) or an UPDATE.
   * 
   * @return
   *   Failure to write a record will return FALSE. Otherwise SAVED_NEW or SAVED_UPDATED is returned
   *   depending on the operation performed.
   */
  public function save() {
    return drupal_write_record('d2c_domain', $this, $this->local_id? "local_id" : array());
  }
  
  /**
   * Sends all current D2C domain's data to dot2code.
   * 
   * This can be both a D2C domain creation operation (for new instances) or a D2C domain edit operation (for
   * already existent D2C domains in dot2code).
   * 
   * @param $comments
   *    (optional) A string with comments to send to dot2code along with the D2C domain's data. This is only
   *    useful when creating a new D2C domain and it is ignored otherwise.
   */
  public function remote_save($comments = '') {
    $attributes = $this->attributes();
    if ($header_image = $this->header_image()) {
      $filepath = $_SERVER['DOCUMENT_ROOT'] . '/' . $header_image;
      $attributes['header'] = $filepath;
    } else {
    	unset($attributes['header']);
    }
    if ($this->id) {
      $api = new D2CAPI($this->id, $this->secret_key);
      
      $this->set_attributes($api->edit_domain($attributes));    
    }
    else {
      $api = new D2CAPI();
      $this->set_attributes($api->create_domain($attributes, $comments));
    }
    $this->save();
  }

  
  /**
   * Sets values for one or more attributes and saves the changes both locally and remotely (only if needed).
   * 
   * @param $attributes
   *   An associative array with values for one or more of the D2C domain's attributes.
   */
  public function update($attributes) {
    // Values for attributes that don't change are discarded.
    foreach($attributes as $attr => $value) {
      if ($value == $this->$attr) {
        unset($attributes[$attr]);
      }
    }
    $this->set_attributes($attributes);
    if (count(array_intersect(array_keys($attributes), D2CDomain::attribute_names())) == 0) {
      // Only administrative data has been changed. A local save is made.
      $this->save();
    }
    else {
      // There has been changes on D2C domain's attributes that have to be replicated remotely.
      // A remote_save is made.
      $this->remote_save();
    }
  }
  
  /**
   * Reloads the information about the current D2C domain from dot2code's servers.
   * 
   * It also stores the retrieved values in the local database by calling save(). 
   */
  public function remote_reload() {
    if ($this->id && $this->secret_key) {
      $api = new D2CAPI($this->id, $this->secret_key);
      $this->set_attributes($api->get_domain());
      $this->save();
    }
  }
  
  /**
   * Asks dot2code's servers to send an e-mail with the secret key for this D2C domain.
   * 
   * The e-mail will be sent to the configured e-mail address for this D2C domain and only
   * if a valid id (a real domain id, not a local id) has already been set up for this instance.
   */
  public function send_secret_key() {
    if ($this->id) {
      $api = new D2CAPI($this->id);
      $api->send_secret_key();
    }
  }
  
  /**
   * Deactivates this D2C domain in dot2code's servers.
   * 
   * It also updates the local information in the database and deactivates all QR codes for this domain.
   */
  public function deactivate() {
    $api = new D2CAPI($this->id, $this->secret_key);
    $this->set_attributes($api->delete_domain());
    $this->save();
    db_update('d2c_code')
      ->fields(array('active' => 0))
      ->condition('domain_id', $this->local_id)
      ->execute();
  }

  /**
   * Reactivates this D2C domain in the local database if the D2C domain has already been reactivated in dot2code's servers.
   * 
   * It also reactivates all QR codes for this domain.
   */
  public function reactivate() {
    $this->remote_reload();
    if ($this->status == 'active') {
      db_update('d2c_code')
        ->fields(array('active' => 1))
        ->condition('domain_id', $this->local_id)
        ->execute();      
    }
  }
  
  /**
   * Checks an auth token to see if its still valid.
   * 
   * A request to dot2code's servers will be made.
   * 
   * @param $auth_token
   *   A string with the auth token to check.
   *   
   * @result
   *   A boolean indicating if the operation was successful or not. 
   */
  public function check_auth_token($auth_token) {
  	$result = FALSE;
    $api = new D2CAPI($this->id, $this->secret_key);
    $result = $api->authenticate_user($auth_token);
    if (!$result) {
      // Maybe is this a valid GET request but duplicated by a crappy client? (look else condition).
    	if (($cached_result = cache_get($auth_token, 'cache')) && ($cached_result->expire > REQUEST_TIME)) {
        $result = TRUE;
      }
    } else {
    	// Cache D2C API result locally for a small amount of time (10 secs). Some browsers, specially
    	// mobile ones, send duplicate GET requests to Drupal.
    	cache_set($auth_token, TRUE, 'cache', REQUEST_TIME + 10);
    }
    return $result;
  }
  
  /**
   *  Private methods
   */
  
  /**
   * Sets default values for the attributes in this instance.
   */
  private function set_defaults() {
    $this->name = variable_get('site_name', '');
    $this->community = TRUE;
    $this->domain_type = 6;
    $this->email = variable_get('site_mail', '');
    $this->locale = 'en';
    $this->repeatable = TRUE;
    $this->profile_entries = array();
  }
  
  /**
   * Returns the list of D2C domain attributes that dot2code can understand.
   * 
   * @return
   *   An array with the name of all attributes used in remote operations.
   */
  private static function attribute_names() {
    return array('id', 'name', 'header', 'backend_url', 'community', 'domain_type', 'duration',
      'email', 'locale', 'repeatable', 'start', 'profile_entries');
  }
  
}
