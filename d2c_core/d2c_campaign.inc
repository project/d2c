<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

/**
 * Instances of this class will represent a D2C campaign.
 *
 * A D2C campaign is nothing but an easy way of organizing your QR codes.
 */
class D2CCampaign {
  
  /**
   * @var A numeric (autoincremental) identifier.
   */
  public $id;
  
  /**
   * @var A name that will be seen by administrators.
   */
  public $name;
  
  /**
   * @var A textual description.
   */
  public $description;
    
  /**
   *  Class methods
   */
  
  /**
   * Returns the list of D2C campaigns in the local database.
   * 
   * @return
   *   An array of D2CCampaign instances.
   */
  public static function all() {
    $campaigns = array();
    $result = db_query("SELECT * FROM {d2c_campaign}");
    foreach($result->fetchAllAssoc('id') as $campaign) {
      $campaigns[] = new D2CCampaign($campaign);
    }
    return $campaigns;
  }
  
  /**
   * Finds a certain D2C campaign in the database.
   * 
   * @param $local_id
   *   An integer with the id for the target D2C campaign. 
   *   
   * @return
   *   A D2CCampaign instance if the campaign was found. Null, otherwise.
   */
  public static function find($id) {
    $result = db_query("SELECT * FROM {d2c_campaign} WHERE id = :id", array(':id' => $id));
    $attributes = $result->fetchAssoc();
    if ($attributes) {
      return new D2CCampaign($attributes);
    }
    return NULL;
  }
   
  
  /**
   *  Instance methods
   */
  
  /**
   * Class constructor.
   *
   * @param $attributes
   *   (optional) An associative array with values for one or more of the D2C campaign's attributes. 
   */
  public function __construct($attributes = array()) {
    $this->set_attributes($attributes);
  }
  
  /**
   * Sets values for all provided attributes.
   * 
   * Unkown attributes will be ignored.
   * 
   * @param $attributes
   *   An associative array with values for one or more of the D2C campaign's attributes.
   */
  public function set_attributes($attributes) {
    foreach($attributes as $attr => $value) {
      try {
        $this->$attr = $value;
      }
      catch (Exception $e) {}
    }
  }
  
  /**
   * Returns all values in this instance for the list of attributes returned by function attribute_names().
   * 
   * @return
   *   An associative array with attribute names as keys and their current values.
   */
  public function attributes() {
    $attributes = array();
    foreach(D2CCampaign::attribute_names() as $attribute) {
      $attributes[$attribute] = $this->$attribute;
    }
    return $attributes;
  }

  /**
   * Returns an array with all QR codes assigned to this campaign.
   * 
   * @return
   *   An array of D2CCode instances.
   */
  public function codes() {
    $codes = array();
    $result = db_query('SELECT * FROM {d2c_code} WHERE campaign_id = :id', array(':id' => $this->id));
    foreach($result->fetchAllAssoc('local_id') as $code) {
      $codes[] = new D2CCode($code);
    }
    return $codes;
  }
    
  /**
   * Returns the number of QR codes assigned to this category.
   * 
   * @return
   *   An int with the number of QR codes in this category.
   */
  public function codes_number() {
    $query = db_select('d2c_code');
    $count = $query->condition('campaign_id', $this->id)->countQuery()->execute()->fetchField();
    return $count;
  }
  
  /**
   * Saves the instance to the local database. This can be both an INSERT (for new instances) or an UPDATE.
   * 
   * @return
   *   Failure to write a record will return FALSE. Otherwise SAVED_NEW or SAVED_UPDATED is returned
   *   depending on the operation performed.
   */
  public function save() {
    return drupal_write_record('d2c_campaign', $this, $this->id? 'id' : array());
  }
  
  /**
   * Sets values for one or more attributes and saves the changes in the database.
   * 
   * @param $attributes
   *   An associative array with values for one or more of the D2C campaign's attributes.
   */
  public function update($attributes) {
    $this->set_attributes($attributes);
    return $this->save();
  }
  
  /**
   * Deletes the D2C campaign from the database.
   * 
   * @param cascade
   *   A boolean. If TRUE, all QR codes assigned to the campaign will be also deleted.
   *   If FALSE, they will only be unassigned.
   */
  public function destroy($cascade = FALSE) {
    db_delete('d2c_campaign')->condition('id', $this->id)->execute();
    if ($cascade) {
      foreach($this->codes() as $code) {
        $code->destroy();
      }
    } else {
      db_update('d2c_code')
        ->fields(array('campaign_id' => NULL))
        ->condition('campaign_id', $this->id)
        ->execute();
    }
  }
  
  
  /**
   *  Private methods
   */
  
  /**
   * Returns the list of D2C campaigns' available attributes.
   * 
   * @return
   *   An array with the name of all attributes.
   */
  private static function attribute_names() {
    return array('id', 'name', 'description');
  }
  
}
