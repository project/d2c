<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

module_load_include('inc', 'd2c_core', 'd2c_domain');

/**
 * Page callback for admin/config/d2c/domains
 *
 * List of available domains.
 */
function d2c_settings_domains() {

  $domains = D2CDomain::all();
  
  if (empty($domains)) {
    $out = '<p>' . t('In order to use dot2code\'s services, you first have to apply for a D2C domain. Please click in the <a href="@new">New D2C domain</a> link below and create a new domain.', array('@new' => url('admin/config/d2c/domains/new'))) . '</p>';
  }
  else {
    
    drupal_add_css(drupal_get_path('module','d2c_core') . '/assets/d2c_core.css');
    
    $default_domain = D2CDomain::default_domain();
    $default_domain_local_id = $default_domain ? $default_domain->local_id : NULL;
    $header = array(t('Default'), t('Name'), t('D2C Id'), t('Status'), t('# Codes (used / left)'), array('colspan' => 2));
    $rows = array();
    foreach($domains as $domain) {
      $radio_attributes = array('onclick' => 'window.location = "'.url('admin/config/d2c/domains/' . $domain->local_id . '/set_as_default').'"');
      if (!$domain->is_usable()) {
        $radio_attributes['disabled'] = 'disabled';
      }
      $row = array(theme('radio', array('element' => array(
        '#name' => 'default',
        '#id' => 'default_' . $domain->local_id,
        '#return_value' => TRUE,
        '#attributes' => $radio_attributes,
        '#parents' => array(),
        '#value' => ($domain->local_id == $default_domain_local_id))))
      );
      $row[] = l($domain->name, 'admin/config/d2c/domains/' . $domain->local_id);
      $row[] = $domain->id;
      $row[] = t($domain->status);
      $row[] = $domain->used_codes() . " / " . $domain->codes_left();
      
      $action_links = array();      
      if ($domain->secret_key == NULL) {
        $action_links[] = array('title' => 'connect', 'target' => url("admin/config/d2c/domains/{$domain->local_id}/connect"));
      }
      else {
        if ($domain->status != 'closed') {
          $action_links[] = array('title' => 'edit', 'target' => url("admin/config/d2c/domains/{$domain->local_id}/edit"));
        }
        if (module_exists('d2c_analytics_main_consumer')) {
          $action_links[] = array('title' => 'statistics', 'target' => url('admin/config/d2c/stats', array('query' => array('type' => 'domain', 'id' => $domain->local_id))));
        }
        if ($domain->status != 'closed') {
          $action_links[] = array('title' => 'deactivate', 'target' => url("admin/config/d2c/domains/{$domain->local_id}/deactivate"));
        }
      }
      if ($domain->secret_key == NULL || $domain->status == 'new' || $domain->status == 'closed') {
        if($domain->status == 'closed') {
          $action_links[] = array('title' => 'reactivate', 'target' => url("admin/config/d2c/domains/{$domain->local_id}/reactivate"));
        }
        $action_links[] = array('title' => 'delete', 'target' => url("admin/config/d2c/domains/{$domain->local_id}/delete"));
      }
      $row[] = array('data' => theme('d2c_core_action_links', array('links' => $action_links)), 'class' => 'links');
      $rows[] = $row;
    }
    $out = theme('table', array('header' => $header, 'rows' => $rows));
  }
  $new_domain_link = l(t('New D2C domain'), 'admin/config/d2c/domains/new');
  return $out . '<div class="new_object_link">' . $new_domain_link . '</div>';
  
}

/**
 * Page callback for admin/config/d2c/domains/new
 *
 * Create a new D2C domain in the local system.
 */
function d2c_settings_new_domain_form($form) {
  $domain = new D2CDomain();

  $form = array();

  // Domain
  $form['d2c_domain'] = array(
    '#type' => 'fieldset',
    '#title' => t("D2C Domain's administrative settings"),
    '#description' => ''
  );  
  // Domain name
  $form['d2c_domain']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('A suitable name for your domain. This will be what users will see in their visited domains history page. Defaults to your site name.'),    
    '#default_value' => $domain->name,
    '#required' => TRUE
  );  
  // Domain description
  $form['d2c_domain']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('A brief description about your domain. Just for administrative purposes (only users with the administrative role will see it).')
  );
  // Submit button    
  $form['d2c_domain']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
    '#submit' => array('d2c_settings_new_domain_submit'),
  );

  return $form;
}

function d2c_settings_new_domain_submit($form, &$form_state){
  
  $domain = new D2CDomain($form_state['values']);

  if ($domain->save()) {
    drupal_set_message(t('Your D2C domain has been created.'));
    $form_state['redirect'] = 'admin/config/d2c/domains/' . $domain->local_id . '/connect';
  }
  else {
    drupal_set_message(t('There has been an error trying to create the D2C domain. Is there any problem with the database?'), 'error');
  } 
  
}

/**
 * Page callback for admin/config/d2c/domains/%/connect
 *
 * Connect a local D2C domain with a real (remote) D2C domain.
 */
function d2c_settings_connect_domain_form($form, &$form_state, $local_id) {

  $domain = D2CDomain::find($local_id);

  if ($domain->id) {
    $description = t("Once your domain has been activated you will receive an e-mail with all required data. Insert that data into the form below to start using your D2C domain. If you have lost your mail, you can ask for your secret key, <a href=\"@here\">here</a>. The activation of your domain can take some time. Please, be patient.", array('@here' => url('admin/config/d2c/domains/' . $domain->local_id . '/send_secret')));
  }
  else {
    $description = '<p>' . t("Next step is to connect the local domain you have created with a real D2C domain at dot2code's servers.") . '</p>';
    $description .= '<p><b>' . t("Click <a href=\"@here\">here</a> to register your own D2C domain at dot2code.", array('@here' => url("admin/config/d2c/domains/$local_id/request/"))) . "</b></p>";
    $description .= '<p>' . t("If you already own a D2C domain you can insert its data in the form below, but beware that in order for it to work properly with this module, its backend_url has to be configured as <strong>%url</strong> (if necessary, you can ask for a change of configuration for your domain sending an e-mail to <a href=\"mailto:!mail\">!mail</a>)", array('%url' => url('d2c/' . $domain->local_id, array('absolute' => TRUE)), '!mail' => 'domains@dot2code.com')) . '</p>';
  }
  
  $form = array();
  
  // Domain
  $form['d2c_domain'] = array(
    '#type' => 'fieldset',
    '#title' => t('Domain Settings'),
    '#description' => $description
  );
  
  // Local id
  $form['d2c_domain']['local_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $domain->local_id
  );
  
  // Name
  $form['d2c_domain']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),   
    '#default_value' => $domain->name,
    '#disabled' => TRUE
  );
  
  // Id
  $form['d2c_domain']['id'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain id'),
    '#description' => t('The id of your D2C domain.'),    
    '#default_value' => $domain->id,
    '#required' => TRUE
  );
  
  // Secret key
  $form['d2c_domain']['secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret key'),
    '#description' => t('The secret key that will be used when communicating to dot2code\'s servers.'),    
    '#default_value' => $domain->secret_key,
    '#required' => $domain->id
  );

  $form['d2c_domain']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Connect'),
    '#submit' => array('d2c_settings_connect_domain_submit'),
  );

  return $form;
  
}

function d2c_settings_connect_domain_submit($form, &$form_state) {
  
  $domain = D2CDomain::find($form_state['values']['local_id']);
  $domain->id = $form_state['values']['id'];
  $domain->save();
  
  if ($form_state['values']['secret_key']) {
    try {
      $domain->secret_key = $form_state['values']['secret_key'];
      $domain->remote_reload();
      drupal_set_message(t('Secret key was validated. You may now use your D2C domain.'));
      $domain->save();
      $domain->set_as_default();
      $form_state['redirect'] = 'admin/config/d2c/domains';
    }
    catch(D2CTransportException $e) {
      watchdog('d2c_core', 'There has been an error trying to validate a secret key: ' . $e->getMessage(), NULL, WATCHDOG_ERROR);
      drupal_set_message(t('There has been an error trying to validate your secret key.'), 'error');
    }
  }
  else {
    $form_state['redirect'] = 'admin/config/d2c/domains/' . $domain->local_id . '/connect';
  }
  
}

/**
 * Page callback for admin/config/d2c/request_domain
 *
 * Request a new D2C domain from dot2code's servers.
 */
function d2c_settings_request_domain_form($form, &$form_state, $local_id) {
  
  $domain = D2CDomain::find($local_id);

  $form = array('#attributes' => array('enctype' => 'multipart/form-data'));

  // Domain
  $form['d2c_domain'] = array(
    '#type' => 'fieldset',
    '#title' => t('Domain Settings'),
    '#description' => ''
  );
  
  // Id
  $form['d2c_domain']['local_id'] = array(
    '#type' => 'hidden',    
    '#default_value' => $local_id
  );
  // Domain name
  $form['d2c_domain']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain name'),
    '#description' => t('A suitable name for your domain. This will be what users will see in their visited domains history page.'),    
    '#default_value' => $domain->name,
    '#required' => TRUE
  );
  // Header file
  $form['d2c_domain']['header'] = array(
    '#type' => 'file',
    '#title' => t('Header image'),
    '#description' => t('A image to be shown in authentication and register pages when a non connected visitor tries to access your D2C domain. Recommended size: 250 x 75 pixels.')
  );
  // Domain Type (4..6)
  $types = array(4 => t('Type 4 (large domains, 17.039.360 different QR codes available)'), 5 => t('Type 5 (medium domains, 266.240 different QR codes available)'), 6 => t('Type 6 (small domains, 4.096 different QR codes available)'));
  $form['d2c_domain']['type'] = array(
    '#type' => 'select',
    '#title' => t('Domain type'),
    '#description' => t('The type of your D2C domain regarding the number of QR codes that it will be able to generate. Please, select the type that fits better your real needs. <b>And remember you\'ll always be able to apply for new domains if you run out of codes</b>. For larger domains, please, contact us directly.'),
    '#default_value' => $domain->domain_type,
    '#required' => TRUE,
    '#options' => $types
  );
  // Community
  $form['d2c_domain']['community'] = array(
    '#type' => 'select',
    '#title' => t('Domain mode'),
    '#description' => t('There are two types of domains: community and premium. First ones are free but they only grant you access to a single piece of data about your visitors (their e-mail, and only for those users that have a confirmed e-mail address), whereas the premium ones have full access to all D2C domains potential and guarantees the existance of all profile data that you need.'),
    '#default_value' => $domain->community,
    '#required' => TRUE,
    '#options' => array(TRUE => t('Community'), FALSE => t('Premium')),
    '#attributes' => array('onchange' => "(function ($) { $('#profile-entries').slideToggle('slow'); })(jQuery);")
  );
  // Repeatable
  $form['d2c_domain']['repeatable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Repeatable?'),
    '#description' => t('Indicates if users will be able to access the domain more than once.'),
    '#default_value' => $domain->repeatable,
    '#required' => TRUE
  );
  // Start
  $form['d2c_domain']['start'] = array(
    '#type' => 'textfield',
    '#title' => t('Start'),
    '#description' => t("Date for the domain to get activated unless dot2code administrators haven't accepted it by then. If you leave this field blank, domain will be activated as soon as the administrators accept it. Value must follow the format dd-mm-yyyy (ex. '05-10-2011')"),
    '#default_value' => $domain->start,
    '#required' => FALSE,
    '#size' => 10,
    '#element_validate' => array('d2c_settings_domain_date_validator')
  );
  // Duration
  $form['d2c_domain']['duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration'),
    '#description' => t('Number of days from domain activation after wich it will be canceled automatically. You can leave it blank to avoid the automatic cancellation of your domain.'),
    '#default_value' => $domain->duration,
    '#required' => FALSE,
    '#size' => 10
  );
  // Contact email
  $form['d2c_domain']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact email'),
    '#description' => t('E-mail address that dot2code will use to communicate with you. It is important that this address be correct, as all data you need to use your D2C domain will be sent to it.'),
    '#default_value' => $domain->email,
    '#required' => TRUE
  );
  // Preferred locale for admin
  $form['d2c_domain']['locale'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#description' => t('The preferred language to be used on email notifications from dot2code. This applies only to admin emails, the site\'s users will be able to choose their own language.'),
    '#default_value' => $domain->locale,
    '#required' => TRUE,
    '#options' => D2CDomain::locales()
  );  

  //Profile fields
  $form['d2c_domain']['profile_entries'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Profile fields'),
    '#description' => t('Data that the user must provide to access your domain. That data will be sent to your application for each visitor.'),
    '#default_value' => $domain->profile_entries,
    '#options' => D2CDomain::profile_fields(TRUE),
    '#prefix' => '<div id="profile-entries"' . ($domain->community ? ' style="display: none"' : '') . '>', 
    '#suffix' => '</div>'
  );

  //Domain comments
  $form['d2c_domain']['comments'] = array(
    '#type' => 'textarea',
    '#title' => t('Domain comments'),
    '#description' => t('Please, write here a small text telling us what kind of use you will give to your D2C domain, the kind of page you will associate it to, etc. This is very important, specially if you ask for a domain type of 1 to 5, so the administrators can decide if this type is indeed the one that best suits your needs.'),
    '#required' => TRUE,
  );
  
  //Form button to apply for a domain    
  $form['d2c_domain']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Apply for a D2C domain'),
    '#submit' => array('d2c_settings_request_domain_submit'),
  );

  return $form;
}

function d2c_settings_domain_date_validator($element, &$form_state) {
  $regexp = "/^\d\d-\d\d-\d\d\d\d$/";
  if (!empty($element['#value']) and !preg_match($regexp, $element['#value'])) {
    form_error($element, t('Start data must follow the following format: dd-mm-yyyy'));
  }
}

function d2c_settings_request_domain_submit($form, &$form_state) {

  global $base_url;
  
  $dir = d2c_settings_get_files_path('domains');
    
  if ($file = file_save_upload('header', array(), $dir)) {
    $form_state['values']['header'] = $file->fid;
  }
  
  $local_id = $form_state['values']['local_id']; 
  $domain = D2CDomain::find($local_id);
  $domain->set_attributes($form_state['values']);
  // We don't want to include any language specific stuff in the backend URL.
  if (variable_get('clean_url', '0')) {
    $domain->backend_url = $base_url . '/d2c/' . $local_id;
  } else {
    $domain->backend_url = $base_url . '/?q=d2c/' . $local_id;
  }
  
  try {
    $domain->remote_save($form_state['values']['comments']);
    drupal_set_message(t('Your request for a new domain has been sent to the dot2code team. You will receive an email as soon as it is approved.'));
    $form_state['redirect'] = 'admin/config/d2c/domains/' . $local_id . '/connect';
  }
  catch (D2CTransportException $e) {
    watchdog('d2c_core', 'There has been an error trying to create a D2C domain in dot2code: ' . $e->getMessage(), NULL, WATCHDOG_ERROR);
    drupal_set_message(t('There has been an error trying to create the D2C domain in dot2code.'), 'error');
  } 
}

/**
 * Page callback for admin/config/d2c/send_domain_secret/%
 *
 * Send secret key to e-mail address.
 */
function d2c_settings_send_domain_secret_form($form, &$form_state, $local_id) {

  $domain = D2CDomain::find($local_id);
  
  $form = array();
  
  // Domain
  $form['d2c_domain'] = array(
    '#type' => 'fieldset',
    '#title' => t('Domain Settings'),
    '#description' => t("Press the button and an e-mail will be sent to your e-mail address containing the secret key for this D2C domain. This will be efective only if your D2C domain has already been activated by dot2code's administrators."));
  
  // Local id
  $form['d2c_domain']['local_id'] = array(
    '#type' => 'hidden',    
    '#default_value' => $domain->local_id
  );
  
  // Name
  $form['d2c_domain']['domain_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain name'),
    '#default_value' => $domain->name,
    '#disabled' => TRUE
  );  
  
  //Email
  $form['d2c_domain']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),    
    '#default_value' => $domain->email,
    '#disabled' => TRUE
  );
  
  $form['d2c_domain']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send me my key!'),
    '#submit' => array('d2c_settings_send_domain_secret_submit'),
  );

  return $form;
}

function d2c_settings_send_domain_secret_submit($form, &$form_state) {
  
  $domain = D2CDomain::find($form_state['values']['local_id']);
  
  try {
    $domain->send_secret_key();
    drupal_set_message(t('Your secret key has been sent to') . ' ' . $domain->email);
    $form_state['redirect'] = 'admin/config/d2c/domains/' . $domain->local_id . '/connect';
  }
  catch(D2CTransportException $e) {
    watchdog('d2c_core', 'There has been an error trying to send a secret key: ' . $e->getMessage(), NULL, WATCHDOG_ERROR);
    drupal_set_message(t('There has been an error trying to send your secret key.'), 'error');
  } 
}

/**
 * Page callback for admin/config/d2c/domains/%/set_as_default
 *
 * Select the default domain.
 */ 
function d2c_set_as_default_domain($local_id) {
  $domain = D2CDomain::find($local_id);
  $domain->set_as_default();
  drupal_set_message(t('The D2C domain %id has been set as default.',array('%id' => $domain->name)));
  drupal_goto('admin/config/d2c/domains');
}

/**
 * Page callback for admin/config/d2c/domains/%
 *
 * Show configuration for a D2C domain.
 */
function d2c_settings_domain($local_id) {
  
  drupal_add_css(drupal_get_path('module','d2c_core') . '/assets/d2c_core.css');

  $domain = D2CDomain::find($local_id);
  
  $out = '<div id="d2c-domain">';
  
  $out .= '<h3></h3>';
  if ($header = $domain->header_image()) {
    $out .= '<img src="' . url($header) . '" />';
  }
  $out .= '<div><strong>' . t('Id') . ':</strong> ' . $domain->id . '</div>';
  $out .= '<div><strong>' . t('Name') . ':</strong> ' . $domain->name . '</div>';
  $out .= '<div><strong>' . t('Status') . ':</strong> ' . t($domain->status) . '</div>';
  $out .= '<div><strong>' . t('URL') . ':</strong> ' . $domain->backend_url . '</div>';
  $out .= '<div><strong>' . t('Description') . ':</strong> ' . $domain->description . '</div>';
  
  $out .= '<h3>' . t('Codes information') . '</h3>';
  $out .= '<div><strong>' . t('Type') . ':</strong> ' . $domain->domain_type . '</div>';
  $out .= '<div><strong>' . t('Codes used') . ':</strong> ' . $domain->used_codes() . '</div>';
  $out .= '<div><strong>' . t('Codes left') . ':</strong> ' . $domain->codes_left() . '</div>';
  
  $out .= '<h3>' . t('Behaviour information') . '</h3>';
  $out .= '<div><strong>' . t('Mode') . ':</strong> ' . ($domain->community ? ('Community') : t('Premium')) . '</div>';
  $out .= '<div><strong>' . t('Repeatable') . ':</strong> ' . ($domain->repeatable ? t('True') : t('False')) . '</div>';
  
  $out .= '<h3>' . t('Contact information') . '</h3>';
  $out .= '<div><strong>' . t('Contact email') . ':</strong> ' . $domain->email . '</div>';
  $locales = D2CDomain::locales();
  $out .= '<div><strong>' . t('Default locale') . ':</strong> ' . $domain->locale . ' (' . $locales[$domain->locale] . ') </div>';
  
  $out .= '<h3>' . t('Schedule information') . '</h3>';
  $out .= '<div><strong>' . t('Start date') . ':</strong> ' . ($domain->start ? date('d-m-Y', $domain->start) : '-') . '</div>';
  $out .= '<div><strong>' . t('Duration') . ':</strong> ' . ($domain->duration ? $domain->duration . ' ' . t('days') : '') . '</div>';
  $out .= '<div><strong>' . t('End date') . ':</strong> ' . ($domain->duration ? date('d-m-Y', $domain->start + $domain->duration * 24 * 60 * 60) : '') . '</div>';
  
  $out .= '<h3>' . t('Selected user data') . '</h3>';
  $out .= '<ul>';
  foreach($domain->profile_entries as $key => $value) {
    $out .= '<li>' . print_r($value, TRUE) . '</li>';
  }
  $out .= '</ul>';
  
  $out .= drupal_render(drupal_get_form('d2c_settings_reload_domain_form', $domain));
  
  return $out;
}

function d2c_settings_reload_domain_form($form, &$form_state, $domain) {

  $form = array();
  $form['#local_id'] = $domain->local_id;
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Reload info from dot2code'),
    '#submit' => array('d2c_settings_reload_domain_submit'),
  );
  return $form;
}

function d2c_settings_reload_domain_submit($form, &$form_state) {
  
  $domain = D2CDomain::find($form['#local_id']);
  try {
    $domain->remote_reload();
    drupal_set_message(t('Data has been successfully reloaded from dot2code.'));
    $form_state['redirect'] = 'admin/config/d2c/domains/' . $domain->local_id;
  }
  catch (D2CTransportException $e) {
    watchdog('d2c_core', 'There has been an error trying to reload data from dot2code: ' . $e->getMessage(), NULL, WATCHDOG_ERROR);
    drupal_set_message(t('There has been an error trying to reload data from dot2code.'), 'error');
  }
}

/**
 * Page callback for admin/config/d2c/domains/%/edit
 *
 * Edit a D2C domain.
 */
function d2c_settings_edit_domain_form($form, &$form_state, $local_id) {
  
  $domain = D2CDomain::find($local_id);
  
  $form = array();

  //Domain
  $form['d2c_domain'] = array(
    '#type' => 'fieldset',
    '#title' => t('Domain settings'),
    '#description' => ''
  );
  
  //Local id
  $form['d2c_domain']['local_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $domain->local_id
  );
  //Name
  $form['d2c_domain']['domain_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain name'),
    '#default_value' => $domain->name,
    '#disabled' => TRUE
  );
  // Domain description
  $form['d2c_domain']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('A brief description about your domain. Just for administrative purposes (only users with the administrative role will see it).'),
    '#default_value' => $domain->description
  );
  //Contact email
  $form['d2c_domain']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact email'),
    '#description' => t('A contact email.'),
    '#default_value' => $domain->email,
    '#required' => TRUE
  );
  //Preferred locale for admin
  $form['d2c_domain']['locale'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#description' => t('The preferred language for the email notifications from dot2code. This applies only to admin emails, the site\'s users will be able to choose their own language.'),
    '#default_value' => $domain->locale,
    '#required' => TRUE,
    '#options' => D2CDomain::locales()
  );
  
  //Submit
  $form['d2c_domain']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
    '#submit' => array('d2c_settings_edit_domain_submit'),
  );
  
  return $form;
}

function d2c_settings_edit_domain_submit($form, &$form_state) {

  $domain = D2CDomain::find($form_state['values']['local_id']);
    
  try {
    $domain->update($form_state['values']);
    drupal_set_message(t('The D2C domain has been successfully updated.'));
    $form_state['redirect'] = 'admin/config/d2c/domains';
  }
  catch (D2CTransportException $e) {
    watchdog('d2c_core', 'There has been an error trying to update a D2C domain in dot2code: ' . $e->getMessage(), NULL, WATCHDOG_ERROR);
    drupal_set_message(t('There has been an error trying to update the D2C domain in dot2code.'), 'error');
  }
}

/**
 * Page callback for admin/config/d2c/domains/%/deactivate
 *
 * Deactivate a D2C domain in dot2code's servers.
 */
function d2c_settings_deactivate_domain_form($form, &$form_state, $local_id) {

  $domain = D2CDomain::find($local_id);
  
  $form = array();
  
  //Domain
  $form['d2c_domain'] = array(
    '#type' => 'fieldset',
    '#description' => t("This will deactivate your D2C domain in dot2code's servers. No user will be able to access it once in the 'closed' status.")
  );
  
  //Local id
  $form['d2c_domain']['local_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $domain->local_id,
    '#disabled' => TRUE
  );
  
  //Name
  $form['d2c_domain']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain name'),
    '#default_value' => $domain->name,
    '#disabled' => TRUE
  );

  $form['d2c_domain']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Deactivate it!'),
    '#submit' => array('d2c_settings_deactivate_domain_submit'),
  );
  
  return $form;
}

function d2c_settings_deactivate_domain_submit($form, &$form_state) {

  $domain = D2CDomain::find($form_state['values']['local_id']);
    
  try {
    $domain->deactivate();
    drupal_set_message(t('The D2C domain has been successfully deactivated.'));
    $form_state['redirect'] = 'admin/config/d2c/domains';
  }
  catch (D2CTransportException $e) {
    watchdog('d2c_core', 'There has been an error trying to deactivate a D2C domain in dot2code: ' . $e->getMessage(), NULL, WATCHDOG_ERROR);
    drupal_set_message(t('There has been an error trying to deactivate the D2C domain in dot2code.'), 'error');
  } 
}

/**
 * Page callback for admin/config/d2c/domains/%/reactivate
 *
 * Reactivate a D2C domain in dot2code's servers.
 */
function d2c_settings_reactivate_domain_form($form, &$form_state, $local_id) {

  $domain = D2CDomain::find($local_id);
  
  $form = array();
  
  $description = '<p>' . t("To reactivate your domain, please send an e-mail to <a href=\"mailto:!mail?subject=Reactivation of domain !id\">!mail</a> explaining your case.", array('!mail' => 'domains@dot2code.com', '!id' => $domain->id)) . '</p>';
  $description .= '<p>' . t("Once you have received a confirmation from dot2code, come back to this section and proceed with the reactivation process by submitting the form below.") . '</p>';
  
  //Domain
  $form['d2c_domain'] = array(
    '#type' => 'fieldset',
    '#description' => $description
  );
  
  //Local id
  $form['d2c_domain']['local_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $domain->local_id,
    '#disabled' => TRUE
  );
  
  //Name
  $form['d2c_domain']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain name'),
    '#default_value' => $domain->name,
    '#disabled' => TRUE
  );

  $form['d2c_domain']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Reactivate it!'),
    '#submit' => array('d2c_settings_reactivate_domain_submit'),
  );
  
  return $form;
}

function d2c_settings_reactivate_domain_submit($form, &$form_state) {
  
  $domain = D2CDomain::find($form_state['values']['local_id']);

  try {
    $domain->reactivate();
    if ($domain->status == 'closed') {
      drupal_set_message(t("The D2C domain is still closed in dot2code's servers. Make sure you have contacted dot2code and that you have received an e-mail confirmating the reactivation of your domain."), 'error');
    }
    else {
      drupal_set_message(t('The D2C domain has been reactivated!'));
      $form_state['redirect'] = 'admin/config/d2c/domains/';
    }
  }
  catch (D2CTransportException $e) {
    watchdog('d2c_core', 'There has been an error trying to reload data from dot2code: ' . $e->getMessage(), NULL, WATCHDOG_ERROR);
    drupal_set_message(t('There has been an error trying to reload data from dot2code.'), 'error');
  }
}

/**
 * Page callback for admin/config/d2c/domains/%/delete
 *
 * Delete a D2C domain from the local system.
 */
function d2c_settings_delete_domain_form($form, &$form_state, $local_id) {

  $domain = D2CDomain::find($local_id);
  
  $form = array();
  
  //Domain
  $form['d2c_domain'] = array(
    '#type' => 'fieldset',
    '#description' => t("This will delete the domain from your database, NOT from dot2code's servers. You will still be able to contact dot2code's administrators to ask for its reactivation.")
  );
  
  //Local id
  $form['d2c_domain']['local_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $domain->local_id
  );
  
  //Name
  $form['d2c_domain']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain name'),
    '#default_value' => $domain->name,
    '#disabled' => TRUE
  );

  $form['d2c_domain']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete it!'),
    '#submit' => array('d2c_settings_delete_domain_submit'),
  );
  
  return $form;
}

function d2c_settings_delete_domain_submit($form, &$form_state) {

  D2CDomain::delete($form_state['values']['local_id']); 
  drupal_set_message(t('The D2C domain has been deleted from your database.'));
  $form_state['redirect'] = 'admin/config/d2c/domains';
}

