<?php

/**
 * @copyright  2011, (c) dot2code Technologies S.L.
 * @author     Carlos Abalde <cabalde@dot2code.com>
 */

require_once 'AbstractD2CQRCodeGenerator.php';

/**
 * D2CQRCodeSVGGenerator class.
 */
class D2CQRCodeSVGGenerator extends AbstractD2CQRCodeGenerator {

  /**
   * 
   */
  public function __construct($options) {
    parent::__construct($options);
  }
  
  /**
   * 
   */
  protected function doGeneration($mask) {
    // Fetch customizations.
    $margin = $this->getMargin();
    $moduleSide = $this->getModuleSide();
    
    // Generate SVG contents.
    ob_start();
    $codeSide = 2*$margin*$moduleSide + count($mask)*$moduleSide;
    print '<?xml version="1.0" encoding="iso-8859-1"?>';
    print '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.0//EN" "http://www.w3.org/TR/2001/ REC-SVG-20010904/DTD/svg10.dtd"><svg width="' . $codeSide . '" height="' . $codeSide . '" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">';
    print '<rect x="0" y="0" width="' . $codeSide . '" height="' . $codeSide . '" style="stroke:white;fill:white"/>';
    print '<g style="stroke:black;fill:black">';
    for ($row=0; $row < count($mask); $row++) {
      for ($column=0; $column < strlen($mask[$row]); $column++) {
        if ($mask[$row][$column] == '1') {
          $x = $margin*$moduleSide + $column * $moduleSide;
          $y = $margin*$moduleSide + $row * $moduleSide;
          print '<rect x="' . $x . '" y="' . $y . '" width="' . $moduleSide . '" height="' . $moduleSide . '" style="stroke-width:0"/>';
        }
      }
    }
    print '</g></svg>';
    $contents = ob_get_contents();
    ob_end_clean();

    // Write output contents.
    $output = $this->getOutput();
    if ($output === null) {
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");
      header('Content-disposition: attachment; filename="' . $this->getDownloadName() . '"');
      header("Content-type: image/svg+xml");
      print($contents);
    } else {
      file_put_contents($output, $contents); 
    }
    // Done!
    return TRUE;
  }

  /**
   * 
   */
  protected function getExtesion() {
    return 'svg';
  }
  
}
