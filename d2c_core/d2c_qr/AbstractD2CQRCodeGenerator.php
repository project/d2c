<?php

/**
 * @copyright  2011, (c) dot2code Technologies S.L.
 * @author     Carlos Abalde <cabalde@dot2code.com>
 */

/**
 * AbstractD2CQRCodeGenerator class.
 */
abstract class AbstractD2CQRCodeGenerator {

  private static $DEFAULT_MARGIN = 2;
  private static $DEFAULT_MODULE_SIDE = 50;
  
  private static $QR_LEVEL = QR_ECLEVEL_H;
  private static $QR_VERSION = 3;
  private static $QR_MODULES = 29;
  
  private $options = array();

  /**
   * 
   */
  public function __construct($options) {
    $this->options = $options;
  }
  
  /**
   * 
   */
  public final function generate($codeId) {
    try {
      $mask = $this->getMask('http://d2c.es/' . $codeId);
      if ($mask != NULL) {
        return $this->doGeneration($mask);
      }
    } catch (Exception $e) {
      // Catch & hide weird error to upper layers.
    }
    return FALSE;
  }
  
  /**
   * 
   */
  protected function getMargin() {
    return $this->getOption('margin', self::$DEFAULT_MARGIN);
  }

  /**
   * 
   */
  protected function getModuleSide() {
    $moduleSide = $this->getOption('module_side');
    if ($moduleSide == null) {
      $max_code_side = $this->getOption('max_code_side');
      if ($max_code_side != null) {
        $margin = $this->getMargin();
        $moduleSide = floor($max_code_side / (2*$margin + self::$QR_MODULES));
      } else {
        $moduleSide = self::$DEFAULT_MODULE_SIDE;
      }
    }
    return $moduleSide;
  }

  /**
   * 
   */
  protected function getOutput() {
    return $this->getOption('output');
  }
  
  /**
   * 
   */
  protected function getDownloadName() {
    return $this->getOption('name', 'qr' . $this->getExtesion());
  }
  
  /**
   * 
   */
  protected function getOption($key, $default = null) {
    if (array_key_exists($key, $this->options)) {
      return $this->options[$key];
    }
    return $default;
  }
  
  /**
   * 
   */
  protected abstract function doGeneration($mask);
  
  /**
   * 
   */
  protected abstract function getExtesion();
  
  /**
   * Calculates a binary mask representation of a D2C QR code linking to $url.
   * Note that version/size and error correction level of the code are both
   * fixed to the default ones in the dot2code platform.
   *
   * @return Array of binary strings.
   */
  private function getMask($url) {
    $margin = 0;
    $encoder = QRencode::factory(self::$QR_LEVEL, self::$QR_VERSION, $margin);
    return QRtools::binarize($encoder->encodeRAW($url, false));
  }
  
}
