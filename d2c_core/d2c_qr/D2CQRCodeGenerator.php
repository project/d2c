<?php

/**
 * 
 * Utility stuff for QR code generation: different formats (PNG, SVG, EPS, PDF...),
 * versions, error correction capabilities, customizations, etc.
 * 
 * NOTES:
 *
 *   - FPDF (apt-get install php-fpdf) should be installed in the system in order to generate
 *     QR codes in PDF format.
 *   - Current implementation is based in PHP QR Code library (http://phpqrcode.sourceforge.net),
 *     which is used to calculate the QR code mask.
 *   - Alternatively, a native implementation based in libqrencode (http://fukuchi.org/works/qrencode)
 *     could be used using this basic PHP binding: http://hirokawa.netflowers.jp/entry/4900/
 *   - Another simple implementation for QR generation: http://prgm.spipu.net/php_qrcode.
 * 
 * @copyright  2011, (c) dot2code Technologies S.L.
 * @author     Carlos Abalde <cabalde@dot2code.com>
 */

/**
 * Import merged version (i.e. disabled cache of PHP QR Code library).
 */
require_once 'phpqrcode/phpqrcode.php';

/**
 * D2CQRCodeGenerator class.
 * 
 * Options available for all generators:
 * 
 *   - 'output'. File where the generated output will be written to. If this options is not
 *     provided, the output will be directly streamed to the client.
 *   - 'margin'. Number of modules that will be reserved for the quiet area.
 *   - 'module_side'. Width & height in pixels of each module.
 *   - 'max_code_side'. If 'module_side' option is not found, this option will be user to
 *     calculate it.
 * 
 * Usage example:
 * 
 *   D2CQRCodeGenerator::getInstance()->svg('AB4bsFKf',
 *                                          array('margin' => 2,
 *                                                'module_side' => 30));
 * 
 */
class D2CQRCodeGenerator {

  /**
   * @var D2CQRCodeGenerator Singleton reference.
   */
  private static $instance = NULL;  
  
  /**
   * Class constructor.
   */
  private function __construct() {
  }

  /**
   * Gets singleton object reference.
   * @return D2CQRCodeGenerator Singleton reference.
   */
  public static function getInstance() {
    if (self::$instance === NULL) {
      self::$instance = new D2CQRCodeGenerator();
    }
    return self::$instance;
  }

  /**
   * 
   */
  public function png($codeId, $options) {
    return D2CQRCodeGeneratorFactory::getInstance()->newGenerator('png', $options)->generate($codeId);
  }
  
  /**
   * 
   */
  public function svg($codeId, $options) {
    return D2CQRCodeGeneratorFactory::getInstance()->newGenerator('svg', $options)->generate($codeId);
  }
  
  /**
   * 
   */
  public function pdf($codeId, $options) {
    return D2CQRCodeGeneratorFactory::getInstance()->newGenerator('pdf', $options)->generate($codeId);
  }
  
}

/**
 * D2CQRCodeGeneratorFactory class.
 */
class D2CQRCodeGeneratorFactory {

  /**
   * @var D2CQRCodeGeneratorFactory Singleton reference.
   */
  private static $instance = NULL;  
  
  /**
   * Class constructor.
   */
  private function __construct() {
  }

  /**
   * Gets singleton object reference.
   * @return D2CQRCodeGeneratorFactory Singleton reference.
   */
  public static function getInstance() {
    if (self::$instance === NULL) {
      self::$instance = new D2CQRCodeGeneratorFactory();
    }
    return self::$instance;
  }

  /**
   * 
   */
  public function newGenerator($id, $options) {
    switch ($id) {
      case 'svg':
        require_once 'D2CQRCodeSVGGenerator.php';
        return new D2CQRCodeSVGGenerator($options);
        break;
      case 'pdf':
        require_once 'D2CQRCodePDFGenerator.php';
        return new D2CQRCodePDFGenerator($options);
        break;
      case 'png':
        require_once 'D2CQRCodePNGGenerator.php';
        return new D2CQRCodePNGGenerator($options);
        break;
      default:
        return null;
    }
  }
  
}
