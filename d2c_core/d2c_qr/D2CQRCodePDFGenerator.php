<?php

/**
 * @copyright  2011, (c) dot2code Technologies S.L.
 * @author     Carlos Abalde <cabalde@dot2code.com>
 */

require_once 'AbstractD2CQRCodeGenerator.php';
require 'fpdf/fpdf.php';

/**
 * D2CQRCodePDFGenerator class.
 */
class D2CQRCodePDFGenerator extends AbstractD2CQRCodeGenerator {

  /**
   * 
   */
  public function __construct($options) {
    parent::__construct($options);
  }
  
  /**
   * 
   */
  protected function doGeneration($mask) {
    // Fetch customizations.
    $margin = $this->getMargin();
    $moduleSide = $this->getModuleSide();
    
    // Generate PDF.
    $codeSide = 2*$margin*$moduleSide + count($mask)*$moduleSide;
    $fpdf = new FPDF('P', 'pt', array($codeSide, $codeSide));
    $fpdf->AddPage();
    $fpdf->SetDrawColor(0, 0, 0);
    $fpdf->SetFillColor(255, 255, 255);
    $fpdf->Rect(0, 0, $codeSide, $codeSide, 'F');
    $fpdf->SetFillColor(0, 0, 0);
    $fpdf->SetLineWidth(0);
    for ($row=0; $row < count($mask); $row++) {
      for ($column=0; $column < strlen($mask[$row]); $column++) {
        if ($mask[$row][$column] == '1') {
          $x = $margin*$moduleSide + $column * $moduleSide;
          $y = $margin*$moduleSide + $row * $moduleSide;
          $fpdf->Rect($x, $y, $moduleSide, $moduleSide, 'F');
        }
      }
    }
    
    // Write output contents.
    $output = $this->getOutput();
    if ($output === null) {
      $fpdf->Output($this->getDownloadName(), 'I');
    } else {
      $fpdf->Output($output, 'F');
    }
    $fpdf->Close();

    // Done!
    return TRUE;
  }

  /**
   * 
   */
  protected function getExtesion() {
    return 'pdf';
  }
  
}
