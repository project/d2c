<?php

/**
 * @copyright  2011, (c) dot2code Technologies S.L.
 * @author     Carlos Abalde <cabalde@dot2code.com>
 */

require_once 'D2CQRCodeBitmapGenerator.php';

/**
 * D2CQRCodePNGGenerator class.
 */
class D2CQRCodePNGGenerator extends D2CQRCodeBitmapGenerator {

  /**
   * 
   */
  public function __construct($options) {
    parent::__construct($options);
  }

  /**
   * 
   */
  protected function streamBitmap($image) {
    header("Content-type: image/png");
    ImagePng($image);
  }

  /**
   * 
   */
  protected function writeBitmap($image, $filename) {
    ImagePng($image, $filename);
  }

  /**
   * 
   */
  protected function getExtesion() {
    return 'png';
  }
  
}
