<?php

/**
 * @copyright  2011, (c) dot2code Technologies S.L.
 * @author     Carlos Abalde <cabalde@dot2code.com>
 */

require_once 'AbstractD2CQRCodeGenerator.php';

/**
 * D2CQRCodeBitmapGenerator class.
 */
abstract class D2CQRCodeBitmapGenerator extends AbstractD2CQRCodeGenerator {

  /**
   * 
   */
  public function __construct($options) {
    parent::__construct($options);
  }

  /**
   * 
   */
  protected function doGeneration($mask) {
    // Fetch customizations.
    $margin = $this->getMargin();
    $moduleSide = $this->getModuleSide();
    
    // Generate bitmap.
    $codeSide = 2*$margin*$moduleSide + count($mask)*$moduleSide;
    $image =ImageCreate($codeSide, $codeSide);
    $bgcolor = ImageColorAllocate($image, 255, 255, 255);
    $fgcolor = ImageColorAllocate($image, 0, 0, 0);
    imagefill($image, 0, 0, $bgcolor);
    for ($row=0; $row < count($mask); $row++) {
      for ($column=0; $column < strlen($mask[$row]); $column++) {
        if ($mask[$row][$column] == '1') {
          for ($x = 0; $x < $moduleSide; $x++) {
            for ($y = 0; $y < $moduleSide; $y++) {
              ImageSetPixel($image, 
                            $margin*$moduleSide + $column*$moduleSide + $x,
                            $margin*$moduleSide + $row*$moduleSide + $y,
                            $fgcolor); 
            }
          }
        }
      }
    }
    
    // Write output.
    $output = $this->getOutput();
    if ($output === null) {
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");
      header('Content-disposition: attachment; filename="' . $this->getDownloadName() . '"');
      $this->streamBitmap($image);
    } else {
      $this->writeBitmap($image, $output);
    }
    
    // Destroy stuff.
    ImageDestroy($image);

    // Done!
    return TRUE;
  }

  /**
   * 
   */
  protected abstract function streamBitmap($image);

  /**
   * 
   */
  protected abstract function writeBitmap($image, $filename);
  
}
