<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

module_load_include('inc', 'd2c_core', 'd2c_campaign');

/**
 * Page callback for admin/config/d2c/campaigns
 *
 * List of available campaigns.
 */
function d2c_settings_campaigns() {

  $campaigns = D2CCampaign::all();
  
  drupal_add_css(drupal_get_path('module','d2c_core') . '/assets/d2c_core.css');
  
  $out = t('<p>Use campaigns to define custom groups of QR Codes. This will facilitate later searches and will let you compare the statistics for all codes in the same campaign (as long as the D2C Analytics module has been activated).</p>');
  
  if (empty($campaigns)) {
    $out .= '<p>' . t('You haven\'t defined any campaign yet. You may click in the <a href="@new">New campaign</a> link below to create one.', array('@new' => url('admin/config/d2c/campaigns/new'))) . '</p>';
  }
  else {
    $header = array(t('Id'), t('Name'), t('Description'), t('# Codes'), '');
    $rows = array();
    foreach($campaigns as $campaign) {
      $row = array();
      $row[] = $campaign->id;
      $row[] = $campaign->name;
      $row[] = $campaign->description;
      $row[] = $campaign->codes_number();
      $action_links = array();      
      $action_links[] = array('title' => 'edit', 'target' => url("admin/config/d2c/campaigns/{$campaign->id}/edit"));
      if (module_exists('d2c_analytics_main_consumer')) {
        $action_links[] = array('title' => 'statistics', 'target' => url('admin/config/d2c/stats', array('query' => array('type' => 'campaign', 'id' => $campaign->id))));
      }
      $action_links[] = array('title' => 'delete', 'target' => url("admin/config/d2c/campaigns/{$campaign->id}/delete"));
      $row[] = array('data' => theme('d2c_core_action_links', array('links' => $action_links)), 'class' => 'links');
      $rows[] = $row;
    }
    $out .= theme('table', array('header' => $header, 'rows' => $rows));
  }
  $new_campaign_link = l(t('New campaign'), 'admin/config/d2c/campaigns/new');
  return $out . '<div class="new_object_link">' . $new_campaign_link . '</div>';
  
}

/**
 * Page callback for admin/config/d2c/campaigns/new
 *
 * Create a new campaign in the local system.
 */
function d2c_settings_new_campaign_form($form) {

  $form = array();

  // Campaign
  $form['d2c_campaign'] = array(
    '#type' => 'fieldset'
  );  
  // Campaign name
  $form['d2c_campaign']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE
  );  
  // Domain description
  $form['d2c_campaign']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('A brief description about the campaign. Just for administrative purposes (only users with the administrative role will see it).')
  );
  // Submit button    
  $form['d2c_campaign']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
    '#submit' => array('d2c_settings_new_campaign_submit'),
  );

  return $form;
}

function d2c_settings_new_campaign_submit($form, &$form_state){
  
  $campaign = new D2CCampaign($form_state['values']);

  if ($campaign->save()) {
    drupal_set_message(t('Your campaign has been created.'));
    $form_state['redirect'] = 'admin/config/d2c/campaigns';
  }
  else {
    drupal_set_message(t('There has been an error trying to create the campaign. Is there any problem with the database?'), 'error');
  } 
  
}

/**
 * Page callback for admin/config/d2c/campaigns/%/edit
 *
 * Edit a campaign.
 */
function d2c_settings_edit_campaign_form($form, &$form_state, $local_id) {
  
  $campaign = D2CCampaign::find($local_id);
  
  $form = array();

  //Domain
  $form['d2c_campaign'] = array(
    '#type' => 'fieldset',
    '#title' => t('Campaign settings')
  );
  
  //Id
  $form['d2c_campaign']['id'] = array(
    '#type' => 'hidden',
    '#default_value' => $campaign->id
  );
  //Name
  $form['d2c_campaign']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign name'),
    '#default_value' => $campaign->name
  );
  // Description
  $form['d2c_campaign']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('A brief description about your campaign. Just for administrative purposes (only users with the administrative role will see it).'),
    '#default_value' => $campaign->description
  );
    
  //Submit    
  $form['d2c_campaign']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
    '#submit' => array('d2c_settings_edit_campaign_submit'),
  );
  
  return $form;
  
}

function d2c_settings_edit_campaign_submit($form, &$form_state) {

  $campaign = D2CCampaign::find($form_state['values']['id']);
    
  if ($campaign->update($form_state['values'])) {
    drupal_set_message(t('The campaign has been successfully updated.'));
    $form_state['redirect'] = 'admin/config/d2c/campaigns';
  }
  else {
    drupal_set_message(t('There has been an error trying to update the campaign. Is there any problem with the database?'), 'error');
  }
  
}

/**
 * Page callback for admin/config/d2c/campaigns/%/delete
 *
 * Delete a campaign from the database.
 */
function d2c_settings_delete_campaign_form($form, &$form_state, $id) {

  $campaign = D2CCampaign::find($id);
  
  $form = array();
  
  //Campaign
  $form['d2c_campaign'] = array(
    '#type' => 'fieldset'
  );
  
  //Id
  $form['d2c_campaign']['id'] = array(
    '#type' => 'hidden',
    '#default_value' => $campaign->id,
    '#disabled' => TRUE
  );
  
  //Name
  $form['d2c_campaign']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $campaign->name,
    '#disabled' => TRUE
  );
  
  //Cascade
  $form['d2c_campaign']['cascade'] = array(
    '#type' => 'checkbox',
    '#title' => t('Delete all its QR codes'),
    '#default_value' => FALSE,
    '#description' => t('If checked, all QR codes assigned to this campaign will also be deleted. If unchecked, they will simply be unassigned and left outside any campaign.')
  );

  $form['d2c_campaign']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete it!'),
    '#submit' => array('d2c_settings_delete_campaign_submit'),
  );
  
  return $form;
  
}

function d2c_settings_delete_campaign_submit($form, &$form_state) {

  $campaign = D2CCampaign::find($form_state['values']['id']);
  $campaign->destroy($form_state['values']['cascade']); 
  drupal_set_message(t('The campaign has been deleted from your database.'));
  $form_state['redirect'] = 'admin/config/d2c/campaigns';
  
}

?>