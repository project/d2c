<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

module_load_include('inc', 'd2c_core', 'd2c_code');

/**
 * Page callback for admin/config/d2c/codes
 *
 * List of available QR codes.
 */
function d2c_settings_codes() {
  
  $page = @$_GET['page'] ? $_GET['page'] : 0;
  $per_page = 10;
  
  $codes = D2CCode::filter(@$_GET['domain_id'], @$_GET['campaign_id'], @$_GET['name'], @$_GET['behaviour'], @$_GET['code_type'], @$_GET['active'], $page, $per_page, 1);
  if (empty($codes) && @$_GET['domain_id'] == NULL && @$_GET['campaign_id'] == NULL && @$_GET['behaviour'] == NULL && @$_GET['code_type'] == NULL && @$_GET['active'] == NULL) {
    if (D2CDomain::default_domain()) {
      $out = '<p>' . t('You haven\'t created any QR code yet. You can get your first code by clicking on the <a href="@new">New QR code</a> link below and fill a simple form.', array('@new' => url('admin/config/d2c/codes/new'))) . '</p>';
    }
    else {
      $out = '<p>' . t('You haven\'t got any active D2C domain that you could use to generate a QR code. Head for the <a href="@domains">D2C domains section</a> and make sure you have created a local D2C domain and that it is properly connected to an accepted or active D2C domain at dot2code, before trying to generate any QR code.', array('@domains' => url('admin/config/d2c/domains'))) . '</p>';
      return $out;
    }
  }
  else {
    
    drupal_add_css(drupal_get_path('module','d2c_core') . '/assets/d2c_core.css');
    drupal_add_js(drupal_get_path('module', 'd2c_core') .'/assets/d2c_core.js');
    
    $out = drupal_render(drupal_get_form('d2c_settings_codes_filter_form', @$_GET['domain_id'], @$_GET['campaign_id'], @$_GET['name'], @$_GET['behaviour'], @$_GET['code_type'], @$_GET['active']));
    
    $header = array('', t('Local Id'), t('Name'), t('D2C Id'), t('Behaviour'), t('Type'), t('Active'), t('D2C Domain'), t('Campaign'), '', '');
    $rows = array();
    $behaviours = D2CCode::behaviours();
    $code_types = D2CAPI::code_types();
    if (count($codes) <= $per_page) {
      $show_next_link = FALSE;
    }
    else {
      unset($codes[count($codes) - 1]);
      $show_next_link = TRUE;
    }
  
    foreach($codes as $code) {
      
      $checkbox_attributes = array('class' => array('toggle'));
      $checkbox = theme('checkbox', array('element' => array(
        '#name' => 'active',
        '#id' => 'active_' . $code->local_id,
        '#return_value' => TRUE,
        '#attributes' => $checkbox_attributes,
        '#parents' => array(),
        '#default_value' => $code->active,
        '#checked' => $code->active? 'checked' : ''))); //theme_checkbox()
      
      $row = array();
      $image = d2c_settings_code_image_path($code);
      $row[] = '<a href="' . $image . '"><img class="d2c-qr-code" id="d2c-qr-code-' . $code->local_id . '" src="' . $image . '" /></a>';
      $row[] = $code->local_id;
      $row[] = $code->name;
      $row[] = array('data' => $code->id, 'class' => 'd2c-qr-code-id');
      $row[] = l($behaviours[$code->behaviour], 'd2c/codes/' . $code->local_id);
      $row[] = $code_types[$code->code_type];
      $row[] = $checkbox;
      $domain = $code->domain();
      $row[] = $domain ? $domain->name : '';
      $campaign = $code->campaign();
      $row[] = $campaign ? $campaign->name : '';
      $action_links = array();
      $action_links[] = array('title' => 'edit', 'target' => url("admin/config/d2c/codes/{$code->local_id}/edit"));
      if (module_exists('d2c_analytics_main_consumer')) {
        $action_links[] = array('title' => 'statistics', 'target' => url('admin/config/d2c/stats', array('query' => array('type' => 'code', 'id' => $code->local_id))));
      }
      $action_links[] = array('title' => 'delete', 'target' => url("admin/config/d2c/codes/{$code->local_id}/delete"));
      $row[] = array('data' => theme('d2c_core_action_links', array('links' => $action_links)), 'class' => 'links');
      $download_links = array();
      $download_links[] = array('title' => 'png', 'target' => url("admin/config/d2c/codes/{$code->local_id}/png"));
      $download_links[] = array('title' => 'pdf', 'target' => url("admin/config/d2c/codes/{$code->local_id}/pdf"));
      $download_links[] = array('title' => 'svg', 'target' => url("admin/config/d2c/codes/{$code->local_id}/svg"));
      $row[] = array('data' => theme('d2c_core_action_links', array('links' => $download_links)), 'class' => 'links');
      $rows[] = $row;
    }   
    $out .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('d2c-codes')))); //theme_table

    if (empty($codes)) {
      $out .= '<div id="d2c-settings-codes-no-result">' . t('No QR code was found matching your searching criteria.') . '</div>';
    }
    else {
      $out .= '<div id="div#d2c-settings-codes-links">';
      $query_filter = array('page', 'form_build_id', 'form_token', 'form_id', 'op'); 
      if ($page > 0) {
        $out .= ('<a href="' . url("admin/config/d2c/codes", array('query' => array('page' => $page - 1))) . '">' . t('< previous') . "</a>");
        $out .= '&nbsp;&nbsp;&nbsp;&nbsp;';
      }
      if ($show_next_link) {
        $out .= ('<a href="' . url("admin/config/d2c/codes", array('query' => array('page' => $page + 1))) . '">' . t('next >') . "</a>");
      }
      $out .= '</div>';
    }

  }
  $new_code_link = l(t('New QR code'), 'admin/config/d2c/codes/new');
  return $out . '<div class="new_object_link">' . $new_code_link . '</div>';
}

function d2c_settings_codes_filter_form($form, &$form_state, $domain_id, $campaign_id, $name, $behaviour, $code_type, $active) {
  
  $form = array();
  
  $campaigns = array("" => "", "0" => t("None"));
  foreach(D2CCampaign::all() as $campaign) {
    $campaigns[$campaign->id] = substr($campaign->name, 0 , 20) . (strlen($campaign->name) > 20 ? '...' : '');
  }

  $domains = array("" => "");
  foreach(D2CDomain::all() as $domain) {
    $domains[$domain->local_id] = substr($domain->name, 0, 20) . (strlen($domain->name) > 20 ? '...' : '');
  }
  
  // Code Filter
  $form['d2c_code_filter'] = array(
    '#title' => t('Filter options'),
    '#type' => 'fieldset',
    '#description' => t('Navigate across your QR codes using the following filters. <a href="#" onclick="@clear">[Clear filters]</a>', array('@clear' => '(function ($) { $("#d2c-settings-codes-filter-form").clearForm(); })(jQuery); return false;')),
    '#collapsible' => TRUE,
    '#collapsed' => !$domain_id && !strlen($campaign_id) && !strlen($name) && !strlen($behaviour) && !strlen($code_type) && !strlen($active)
  );
  $form['d2c_code_filter']['domain_id'] = array(
    '#title' => t('Domain'),
    '#type' => 'select',
    '#default_value' => $domain_id,
    '#options' => $domains,
  );  
  $form['d2c_code_filter']['campaign_id'] = array(
    '#title' => t('Campaign'),  
    '#type' => 'select',
    '#default_value' => $campaign_id,
    '#options' => $campaigns,
  );
  $form['d2c_code_filter']['name'] = array(
    '#title' => t('Name'),  
    '#type' => 'textfield',
    '#value' => $name,
    '#size' => 30,
    '#autocomplete_path' => 'admin/config/d2c/codes/autocomplete',
  );
  $form['d2c_code_filter']['behaviour'] = array(
    '#title' => t('Behaviour'),
    '#type' => 'select',
    '#default_value' => $behaviour,
    '#options' => array_merge(array('' => ''), D2CCode::behaviours())
  );
  $form['d2c_code_filter']['code_type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#default_value' => $code_type,
    '#options' => array_merge(array('' => ''), D2CAPI::code_types())
  );
  $form['d2c_code_filter']['active'] = array(
    '#title' => t('Active'),
    '#type' => 'select',
    '#default_value' => $active,
    '#options' => array('' => '', '1' => 'Yes', '0' => 'No')
  );
  $form['d2c_code_filter']['submit'] = array(
    '#type' => 'submit',
    '#method' => 'get',
    '#value' => t('Search'),
    '#prefix' => '<div class="form-buttons">',
    '#suffix' => '</div>',
    '#submit' => array('d2c_settings_codes_filter_submit')
  );
  
  return $form;
  
}

/**
 * 
 */
function d2c_settings_codes_filter_submit($form, &$form_state) {
  drupal_goto('admin/config/d2c/codes', array('query' => $form_state['values']));
}

/**
 * Page callback for admin/config/d2c/codes/autocomplete
 * 
 * Provides QR code search results to an AJAX interface. May be used for code name autocompletion in text fields.
 * Searches can be made by code name.
 * 
 * @param $string
 *   The current search string.
 */
function d2c_core_d2c_codes_autocomplete($string = '') {
  $matches = array();
  foreach(D2CCode::filter(NULL, NULL, $string) as $code) {
    $matches[$code->name] = "<div class=\"reference-autocomplete\">". check_plain($code->name) ." [id:$code->local_id]</div>";    
  } 
  drupal_json_output($matches);
}

/**
 * Page callback for admin/config/d2c/codes/new
 *
 * Create a new QR code both locally and in dot2code's servers.
 */
function d2c_settings_new_code_form($form, &$form_state) {

  $code = new D2CCode($_GET);
  
  $campaigns = array("" => "");
  foreach(D2CCampaign::all() as $campaign) {
    $campaigns[$campaign->id] = $campaign->name;
  }

  $domains = array();
  foreach(D2CDomain::usable_domains() as $domain) {
    $domains[$domain->local_id] = $domain->name . ' (' . $domain->codes_left()  . ')';
  }

  $form = array();

  // Code
  $form['d2c_code'] = array(
    '#type' => 'fieldset'
  );
  // Domain
  $form['d2c_code']['domain_id'] = array(
    '#type' => 'select',
    '#title' => t('D2C domain'),
    '#description' => t('D2C Domain that will be used to generate the QR Code. You can see for each one the number of QR codes it has left.'),
    '#default_value' => $code->domain_id,
    '#required' => TRUE,
    '#options' => $domains
  );
  // Code name
  $form['d2c_code']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('A suitable name for your QR code.'),    
    '#default_value' => $code->name,
    '#required' => TRUE
  );
  // Code description
  $form['d2c_code']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('A brief description about the code. Just for administrative purposes (only users with the administrative role will see it).'),
    '#default_value' => $code->description,
  );
  // Campaign
  $form['d2c_code']['campaign_id'] = array(
    '#type' => 'select',
    '#title' => t('Campaign'),
    '#description' => t('Campaign to which this code will be assigned. This is mainly for organization purposes. You can leave this field blank.'),
    '#default_value' => $code->campaign_id,
    '#options' => $campaigns
  );
  $code_types = D2CAPI::code_types();
  $code_type_description = t('This determines the kind of access control that will be used in dot2code\'s servers anytime a user tries to use a QR code. Possible values are:');
  $code_type_description .= '<p><ul>';
  $code_type_description .= '<li><strong>' .  $code_types[D2CAPI::CODE_TYPE_ANONYMOUS] . ':</strong> ' . t('Anybody can use the code, even without a D2C user account. No information about the user will be sent to your application, as dot2code will have no way of recognising the visitor.') . '</li>';
  $code_type_description .= '<li><strong>' .  $code_types[D2CAPI::CODE_TYPE_REUSABLE] . ':</strong> ' . t('These codes require visitors to have a D2C user account to automatically retrieve valuable information about their profiles and optionally link their accounts for delegated authentication.') . '</li>';
  $code_type_description .= '<li><strong>' .  $code_types[D2CAPI::CODE_TYPE_SINGLE_USER] . ':</strong> ' . t('These codes behave like reusable codes but may only be used by a single user. The first user to capture it will invalidate the code for any other visitor, but may still use it several times himself.') . '</li>';
  $code_type_description .= '<li><strong>' .  $code_types[D2CAPI::CODE_TYPE_DISPOSABLE] . ':</strong> ' . t('More restrictive than the types before, these codes can only be used once. After a valid D2C user has captured it, the code becomes invalid.') . '</li>';
  $code_type_description .= '</ul></p><p><b>';
  $code_type_description .= t('Please, select the desired type carefully. It will not be possible to change it later.');
  $code_type_description .= '</b></p>';
  
  // Code type
  $form['d2c_code']['code_type'] = array(
    '#type' => 'select',
    '#title' => t('Code type'),
    '#description' => $code_type_description,
    '#default_value' => $code->code_type,
    '#required' => TRUE,
    '#options' => $code_types
  );
  // Behaviour
  d2c_settings_insert_code_behaviour_selection($form, $code, $form_state);
  // Other modules contributed attributes
  foreach(module_invoke_all('d2c_core_custom_code_form_fields_hook', $code) as $fields) {
    $form['d2c_code'] = array_merge($form['d2c_code'], $fields);
  }
  // Submit button
  $form['d2c_code']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate'),
    '#submit' => array('d2c_settings_new_code_submit'),
  );

  return $form;
}

function d2c_settings_insert_code_behaviour_selection(&$form, $code, $form_state = array('post' => array())) {
  if (@$form_state['post']['behaviour']) {
    $current_behaviour = $form_state['post']['behaviour'];
  } else {
    $current_behaviour = $code->behaviour;
  } 

  drupal_add_js(drupal_get_path('module', 'd2c_core') .'/assets/d2c_core.js');
  
  if (@$form['#attributes']) {
    $form['#attributes']['enctype'] = "multipart/form-data";
  }
  else {
    $form['#attributes'] = array('enctype' => 'multipart/form-data');
  }
  
  $behaviours = D2CCode::behaviours();
  $behaviours_description = t('The type of behaviour this QR code will have when any user tries to access your application through it. Possible values are:');
  $behaviours_description .= '<p><ul>';
  $behaviours_description .= '<li><strong>' .  $behaviours[D2CCode::NodeRedirectionBehaviour] . ':</strong> ' . t('Redirect users to a certain node.') . '</li>';
  $behaviours_description .= '<li><strong>' .  $behaviours[D2CCode::URLRedirectionBehaviour] . ':</strong> ' . t('Redirect users to a full URL in other or the same domain. This includes links to other URI schemes such as FTP (ftp://) or application stores like the Android Market (market://search?q=pname:whatever).') . '</li>';
  $behaviours_description .= '<li><strong>' .  $behaviours[D2CCode::SendFileBehaviour] . ':</strong> ' . t('Let the users download a static file (images, PDFs, etc.).') . '</li>';
  $behaviours_description .= '<li><strong>' .  $behaviours[D2CCode::VCardBehaviour] . ':</strong> ' . t('Generate a personal visit card for you and your company and let the users download it. They will be able to add your data to their mobiles agenda instantly (only on supported phones).') . '</li>';
  $behaviours_description .= '<li><strong>' .  $behaviours[D2CCode::CustomBehaviour] . ':</strong> ' . t('Advanced use: implement the provided hook in any of your modules and give your codes the custom behaviour you want.') . '</li>';
  $behaviours_description .= '</ul></p>'; 
  
  $default_roles = array('all'); // All roles.

  $form['d2c_code']['behaviour_wrapper_start'] = array(
    '#markup' => '<div class="d2c-code-behaviour-wrapper">',
  );
  
  $form['d2c_code']['behaviour'] = array(
    '#type' => 'select',
    '#title' => t('Behaviour type'),
    '#description' => $behaviours_description,
    '#default_value' => $code->behaviour,
    '#required' => TRUE,
    '#options' => D2CCode::behaviours(),
    '#attributes' => array('class' => array('d2c-code-behaviour-selection')) 
  );
  
  // D2CCode::NodeRedirectionBehaviour
  $form['d2c_code']['behaviour_params_' . D2CCode::NodeRedirectionBehaviour . '_nid'] = array(
    '#prefix' => '<div class="behaviour_param" id="behaviour_params_for_' . D2CCode::NodeRedirectionBehaviour . '" style="display: ' . ($current_behaviour == D2CCode::NodeRedirectionBehaviour ? 'block' : 'none') . '">',
    '#title' => t('Node (NID)'),
    '#type' => 'textfield',
    '#default_value' => ($code->behaviour == D2CCode::NodeRedirectionBehaviour) ? $code->behaviour_params['nid'] : '',
    '#autocomplete_path' => 'admin/config/d2c/autocomplete_node',
    '#description' => t('You may also write a node title and select any of the nodes that will appear in a list. It\'s NID will be automatically filled.'),
    '#suffix' => '</div>',
    '#element_validate' => array('d2c_settings_code_nid_validator')
  );
  
  // D2CCode::URLRedirectionBehaviour
  $form['d2c_code']['behaviour_params_' . D2CCode::URLRedirectionBehaviour . '_url'] = array(
    '#prefix' => '<div class="behaviour_param" id="behaviour_params_for_' . D2CCode::URLRedirectionBehaviour . '" style="display: ' . ($current_behaviour == D2CCode::URLRedirectionBehaviour ? 'block' : 'none') . '">',
    '#title' => t('URL'),
    '#type' => 'textfield',
    '#default_value' => ($code->behaviour == D2CCode::URLRedirectionBehaviour) ? $code->behaviour_params['url'] : '',
    '#suffix' => '</div>'
  );
  
  // D2CCode::SendFileBehaviour  
  $form['d2c_code']['behaviour_params_' . D2CCode::SendFileBehaviour . '_file'] = array(
    '#prefix' => '<div class="behaviour_param" id="behaviour_params_for_' . D2CCode::SendFileBehaviour . '" style="display: ' . ($current_behaviour == D2CCode::SendFileBehaviour ? 'block' : 'none') . '">',
    '#title' => t('File to upload'),
    '#type' => 'file'
  );
  
  $form['d2c_code']['behaviour_params_' . D2CCode::SendFileBehaviour . '_selected_roles'] = d2c_settings_roles_selection_checkboxes($current_behaviour == D2CCode::SendFileBehaviour ? $code->behaviour_params['selected_roles'] : $default_roles);
  $form['d2c_code']['behaviour_params_' . D2CCode::SendFileBehaviour . '_selected_roles']['#suffix'] = '</div>';
  
  if(@$code->behaviour_params['fid'] and $uploaded_file = file_load($code->behaviour_params['fid'])) {
    $form['d2c_code']['behaviour_params_' . D2CCode::SendFileBehaviour . '_file']['#prefix'] .= '<div class="form-item"><label>' . t('Current file') . ':</label> ' . $uploaded_file->filename . '</div>';
  }
  
  // D2CCode::VCardBehaviour
  $form['d2c_code']['behaviour_params_' . D2CCode::VCardBehaviour . '_first_name'] = array(
    '#prefix' => '<div class="behaviour_param" id="behaviour_params_for_' . D2CCode::VCardBehaviour . '" style="display: ' . ($current_behaviour == D2CCode::VCardBehaviour ? 'block' : 'none') . '">',
    '#title' => t('First name'),
    '#type' => 'textfield',
    '#default_value' => ($code->behaviour == D2CCode::VCardBehaviour) ? $code->behaviour_params['first_name'] : ''
  );
  
  $form['d2c_code']['behaviour_params_' . D2CCode::VCardBehaviour . '_last_name'] = array(
    '#title' => t('Last name'),
    '#type' => 'textfield',
    '#default_value' => ($code->behaviour == D2CCode::VCardBehaviour) ? $code->behaviour_params['last_name'] : ''
  );
  
  $form['d2c_code']['behaviour_params_' . D2CCode::VCardBehaviour . '_company'] = array(
    '#title' => t('Company'),
    '#type' => 'textfield',
    '#default_value' => ($code->behaviour == D2CCode::VCardBehaviour) ? $code->behaviour_params['company'] : ''
  );
  
  $form['d2c_code']['behaviour_params_' . D2CCode::VCardBehaviour . '_title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#default_value' => ($code->behaviour == D2CCode::VCardBehaviour) ? $code->behaviour_params['title'] : ''
  );
  
  $form['d2c_code']['behaviour_params_' . D2CCode::VCardBehaviour . '_home_tel'] = array(
    '#title' => t('Telephone'),
    '#type' => 'textfield',
    '#default_value' => ($code->behaviour == D2CCode::VCardBehaviour) ? $code->behaviour_params['home_tel'] : ''
  );
  
  $form['d2c_code']['behaviour_params_' . D2CCode::VCardBehaviour . '_email1'] = array(
    '#title' => t('Email'),
    '#type' => 'textfield',
    '#default_value' => ($code->behaviour == D2CCode::VCardBehaviour) ? $code->behaviour_params['email1'] : ''
  );
  
  $form['d2c_code']['behaviour_params_' . D2CCode::VCardBehaviour . '_birthday'] = array(
    '#title' => t('Birthday'),
    '#type' => 'textfield',
    '#default_value' => ($code->behaviour == D2CCode::VCardBehaviour) ? $code->behaviour_params['birthday'] : '',
    '#description' => 'Format: yyyy-mm-dd' 
  );
  
  $form['d2c_code']['behaviour_params_' . D2CCode::VCardBehaviour . '_home_address'] = array(
    '#title' => t('Address'),
    '#type' => 'textfield',
    '#default_value' => ($code->behaviour == D2CCode::VCardBehaviour) ? $code->behaviour_params['home_address'] : ''
  );
  
  $form['d2c_code']['behaviour_params_' . D2CCode::VCardBehaviour . '_url'] = array(
    '#title' => t('Homepage URL'),
    '#type' => 'textfield',
    '#default_value' => ($code->behaviour == D2CCode::VCardBehaviour) ? $code->behaviour_params['url'] : ''
  );
  
  $form['d2c_code']['behaviour_params_' . D2CCode::VCardBehaviour . '_selected_roles'] = d2c_settings_roles_selection_checkboxes($current_behaviour == D2CCode::VCardBehaviour ? $code->behaviour_params['selected_roles'] : $default_roles);
  $form['d2c_code']['behaviour_params_' . D2CCode::VCardBehaviour . '_selected_roles']['#suffix'] = '</div>';
  
  // D2CCode::CustomBehaviour
  $form['d2c_code']['behaviour_params_' . D2CCode::CustomBehaviour . '_function'] = array(
    '#prefix' => '<div class="behaviour_param" id="behaviour_params_for_' . D2CCode::CustomBehaviour . '" style="display: ' . ($current_behaviour == D2CCode::CustomBehaviour ? 'block' : 'none') . '">',
    '#title' => t('Hook to call'),
    '#type' => 'textfield',
    '#default_value' => 'd2c_core_user_request_hook',
    '#disabled' => TRUE
  );
  
  $form['d2c_code']['behaviour_params_' . D2CCode::CustomBehaviour . '_selected_roles'] = d2c_settings_roles_selection_checkboxes($current_behaviour == D2CCode::CustomBehaviour ? $code->behaviour_params['selected_roles'] : $default_roles);
  $form['d2c_code']['behaviour_params_' . D2CCode::CustomBehaviour . '_selected_roles']['#suffix'] = '</div>';

  $form['d2c_code']['behaviour_wrapper_end'] = array(
    '#markup' => '</div>',
  );
  
}

function d2c_settings_roles_selection_checkboxes($default_value) {
  $roles = array();
  $roles['all'] = t('All existing roles');
  foreach (user_roles() as $id => $name) {
    $roles[$id] = $name;
  }
  return array(
    '#type' => 'checkboxes', 
    '#title' => t('Roles that can access this resource'), 
    '#default_value' => $default_value,
    '#options' => $roles
  );
}

function d2c_settings_code_nid_validator($element, &$form_state) {
  if ($form_state['values']['behaviour'] == D2CCode::NodeRedirectionBehaviour && (empty($element['#value']) || !node_load($element['#value']))) {
    form_error($element, t('NID is invalid.'));
  }
}

function d2c_settings_new_code_submit($form, &$form_state) {
  
  d2c_settings_prepare_code_behaviour_params($form_state['values']);
  
  $code = new D2CCode($form_state['values']);

  try {
    $code->remote_save();
    drupal_set_message(t('Your QR code has been created.'));
    module_invoke_all('d2c_core_process_custom_code_form_fields_hook', $code, $form_state['values']);
    if (@$form_state['values']['destination']) {
      $form_state['redirect'] = $form_state['values']['destination'];
    }
    else {
      $form_state['redirect'] = 'admin/config/d2c/codes/';
    }
  }
  catch(D2CTransportException $e) {
    watchdog('d2c_core', 'There has been an error trying to create a QR code: ' . $e->getMessage(), NULL, WATCHDOG_ERROR);
    drupal_set_message(t('There has been an error trying to create the QR code.'), 'error');
  } 
  
}

function d2c_settings_prepare_code_behaviour_params(&$attributes) {
  
  $behaviour = $attributes['behaviour'];
  $behaviour_params = array();
  $main_regexp = "/^behaviour_params/";
  $selected_regexp = "/^behaviour_params_" . $behaviour . "_(.*)$/";
  foreach($attributes as $attr => $value) {   
    if (preg_match($main_regexp, $attr)) {
      $matches = array();
      if (preg_match($selected_regexp, $attr, $matches)) {
        $behaviour_params[$matches[1]] = $value;
      }
      unset($attributes[$attr]);
    }
  }
  $attributes['behaviour_params'] = $behaviour_params;
  
  // Send file special case
  if ($behaviour == D2CCode::SendFileBehaviour) {
    $dir = d2c_settings_get_files_path(D2CCode::UploadedFileDirname);
    if ($file = file_save_upload('behaviour_params_' . D2CCode::SendFileBehaviour . '_file', array(), $dir, FILE_EXISTS_REPLACE)) {
      $attributes['behaviour_params']['fid'] = $file->fid;
      $file->status |= FILE_STATUS_PERMANENT;
      $file = file_save($file);
      unset($attributes['behaviour_params']['file']);
    }
    else {
      if ($attributes['behaviour_params']['file']) {
        // If given a new file to upload but an error arised, show an error message.
        form_set_error('behaviour_params_' . D2CCode::SendFileBehaviour . '_file', t('There\'s been a problem trying to upload your file.'));
      }
      else {
        // If no file was uploaded, unset the behaviour_params file option and preserve possible old values.
        $attributes['behaviour_params']['fid'] = 0;
        unset($attributes['behaviour_params']['file']);
      }
    }
  }
  
}

/**
 * Page callback for admin/config/d2c/codes/%/toggle_activation
 *
 * Toggle activation of a QR code.
 */
function d2c_settings_toggle_activation_code($local_id) {
  
  $code = D2CCode::find($local_id);
  
  if ($code->active) {
    $code->deactivate();
  }
  else {
    $code->activate();
  }

  if (@$_GET['destination']) {
    drupal_goto($_GET['destination']);
  }
  else {
    drupal_goto('admin/config/d2c/codes');
  }
  
}

/**
 * Page callback for admin/config/d2c/codes/%/edit
 *
 * Edit a QR code.
 */
function d2c_settings_edit_code_form($form, &$form_state, $local_id) {
  
  $code = D2CCode::find($local_id);

  $campaigns = array("" => "");
  foreach(D2CCampaign::all() as $campaign) {
    $campaigns[$campaign->id] = $campaign->name;
  }
  
  $form = array();

  // Code
  $form['d2c_code'] = array(
    '#type' => 'fieldset'
  );
  // Domain
  $form['d2c_code']['domain_id'] = array(
    '#type' => 'select',
    '#title' => t('D2C domain'),
    '#description' => t('D2C Domain that will be used to generate the QR Code. You can see for each one the number of QR codes it has left.'),
    '#default_value' => $code->domain_id,
    '#disabled' => TRUE,
    '#options' => array($code->domain()->name)
  );
  // Code local id
  $form['d2c_code']['local_id'] = array(
    '#type' => 'hidden',    
    '#default_value' => $code->local_id
  );
  // Code name
  $form['d2c_code']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('A suitable name for your QR code.'),    
    '#default_value' => $code->name,
    '#required' => TRUE
  );
  // Code description
  $form['d2c_code']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('A brief description about the code. Just for administrative purposes (only users with the administrative role will see it).'),
    '#default_value' => $code->description,
  );
  // Campaign
  $form['d2c_code']['campaign_id'] = array(
    '#type' => 'select',
    '#title' => t('Campaign'),
    '#description' => t('Campaign to which this code will be assigned. This is mainly for organization purposes. You can leave this field blank.'),
    '#default_value' => $code->campaign_id,
    '#options' => $campaigns
  );
  // Code type
  $form['d2c_code']['code_type'] = array(
    '#type' => 'select',
    '#title' => t('Code type'),
    '#description' => t('This determines the kind of access control that will be used in dot2code\'s servers anytime a user tries to use this code.'),
    '#default_value' => $code->code_type,
    '#disabled' => TRUE,
    '#options' => D2CAPI::code_types()    
  );
  // Behaviour
  d2c_settings_insert_code_behaviour_selection($form, $code, $form_state);
  // Other modules contributed attributes
  foreach(module_invoke_all('d2c_core_custom_code_form_fields_hook', $code) as $fields) {
    $form['d2c_code'] = array_merge($form['d2c_code'], $fields);
  }
  // Submit button
  $form['d2c_code']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('d2c_settings_edit_code_submit'),
  );
  
  return $form;
  
}

function d2c_settings_edit_code_submit($form, &$form_state) {
  
  d2c_settings_prepare_code_behaviour_params($form_state['values']);

  $code = D2CCode::find($form_state['values']['local_id']);
  $code->set_attributes($form_state['values']);
  
  if ($code->save()) {
    drupal_set_message(t('The QR code has been successfully updated.'));
    module_invoke_all('d2c_core_process_custom_code_form_fields_hook', $code, $form_state['values']);
    $form_state['redirect'] = 'admin/config/d2c/codes';
  }
  else {
    drupal_set_message(t('There has been an error trying to update the QR code. Is there any problem with the database?'), 'error');
  }
  
}

/**
 * Page callback for admin/config/d2c/codes/%/delete
 *
 * Delete a QR code both locally and in dot2code's servers.
 */
function d2c_settings_delete_code_form($form, &$form_state, $local_id) {

  $code = D2CCode::find($local_id);
  
  $form = array();
  
  //Code
  $form['d2c_code'] = array(
    '#type' => 'fieldset',
  );
  
  //Local id
  $form['d2c_code']['local_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $code->local_id
  );
  
  //Name
  $form['d2c_code']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Code name'),
    '#default_value' => $code->name,
    '#disabled' => TRUE
  );

  $form['d2c_code']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete it!'),
    '#submit' => array('d2c_settings_delete_code_submit'),
  );
  
  return $form;
  
}

function d2c_settings_delete_code_submit($form, &$form_state) {

  $code = D2CCode::find($form_state['values']['local_id']);
  
  try {
    $code->destroy(); 
    drupal_set_message(t('The QR code has been deleted.'));
    $form_state['redirect'] = 'admin/config/d2c/codes';
  }
  catch (D2CTransportException $e) {
    watchdog('d2c_core', 'There has been an error trying to create a QR code: ' . $e->getMessage(), NULL, WATCHDOG_ERROR);
    drupal_set_message(t('There has been an error trying to create the QR code.'), 'error');
  }
  
}

function d2c_settings_download_code($local_id, $format) {
  $code = D2CCode::find($local_id);
  $code->stream($format);
}

/**
 * Helper functions
 */

function d2c_settings_code_image_path($code) {
  $image = base_path() . drupal_get_path('module','d2c_core') . '/assets/temp_qr.png';
  if ($code) {
    $image = $code->image();
    if ($image) {
      $image = file_create_url($image);
    }
  }
  return $image;
}
