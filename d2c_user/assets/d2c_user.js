(function ($) {
  Drupal.behaviors.d2cUserBehavior = {
    attach: function(context){
      /*
   * JS for d2c_user_d2c_core_custom_settings
   */
      // If user role mapping is disabled, mapping configuration options are hidden. 
      if (!$('#edit-d2c-user-role-mapping-enabled:checked').val()) {
        $('#d2c-user-roles-mapping').css("display", "none");
      }
      
      // Enabling / disabling role mapping toggles visibility of mapping configuration options.
      $('#edit-d2c-user-role-mapping-enabled').bind('change', function(){
        $('#d2c-user-roles-mapping').toggle('slow');
      });
    }
  };
})(jQuery);
