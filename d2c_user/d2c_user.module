<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */


/**
 * Drupal hooks 
 */


/**
 * Implements hook_help
 */
function d2c_user_help($path, $arg) {
  $output = '';
  switch ($path) {
    case "admin/help#d2c_user":
      $output = '<p>' . t("The D2C user module lets you use D2C user information to authenticate all users coming from dot2code. It also lets you modify user roles to all D2C users.") . '</p>';
      break;
  }
  return $output;
}

/**
 * Implements hook_boot.
 * 
 * Change in the roles of an user and its authetincation state need to be done asap. So,
 * we user hook_boot instead of hook_init because it gets executed before any menu item is loaded
 * and before any access check in the menu system is performed.
 */
function d2c_user_boot() {
  // If this is a valid incoming D2C request, check fot autologin options.
  if (d2c_is_valid_incoming_d2c_request()) {
    // We need to do a full bootstrap in order to do autologins.
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    // If this is an anonymous user, try to autologin using D2C UID (if configured to do so).
    if ((!user_is_logged_in()) && (d2c_user_settings_get_accounts_link_try_d2c_uid_enabled()) && (!@empty($_SESSION['d2c_data']['uid']))) {
      $uid = db_select('d2c_user_accounts_links')
        ->fields('d2c_user_accounts_links', array('uid'))
        ->condition('d2c_uid', $_SESSION['d2c_data']['uid'])
        ->execute()
        ->fetchField();
      if ($uid) {
        // Log user in.
        $form_state = array();
        $form_state['uid'] = $uid;
        user_login_submit(array(), $form_state);
        drupal_set_message(t('Logged in as %name.', array('%name' => $GLOBALS['user']->name)));
      }
    }
    // If still anonymous, try to autologin using the e-mail account (if configured to do so).
    if (!user_is_logged_in() && d2c_user_settings_get_accounts_link_try_mail_enabled() && (!@empty($_SESSION['d2c_data']['email']))) {
      $account = user_load_by_mail($_SESSION['d2c_data']['email']);
      if ($account) {
        // Log user in.
        $form_state = array();
        $form_state['uid'] = $account->uid;
        user_login_submit(array(), $form_state);
        drupal_set_message(t('Logged in as %name.', array('%name' => $GLOBALS['user']->name)));
      }
    }
  }
  
  // If this is a D2C request (incoming or not), map roles.
  if (d2c_is_valid_d2c_request()) {
    drupal_load('module', 'user');
    if (user_is_logged_in()) {
      if (d2c_user_settings_get_role_mapping_enabled()) {
        global $user;
        $connection_role = @$user->roles[DRUPAL_AUTHENTICATED_RID] ? DRUPAL_AUTHENTICATED_RID : DRUPAL_ANONYMOUS_RID; 
        $roles = user_roles();
        $new_roles = array($connection_role => $roles[$connection_role]);
        foreach($user->roles as $role_id => $role_name) {
          foreach(d2c_user_settings_get_role_mapping($role_id) as $new_role) {
            if ($roles[$new_role]) {
              $new_roles[$new_role] = $roles[$new_role];
            }
          }
        }
        $user->roles = $new_roles;
      }
    }
  }
  
}

/**
 * Implements hook_user_login
 * 
 * If in a D2C session, link accounts (if configured to do so).
 */
function d2c_user_user_login(&$edit, $account) {
  if ((d2c_user_settings_get_accounts_link_enabled()) && (d2c_is_valid_d2c_request())) {
    // Depending on the scenario, this hook can be invoked when the D2C session is
    // completely initialized, or in the boot phase, when the incoming D2C request has
    // been decrypted, checked and temporarily stored in the current session (see hook_boot
    // in d2c_core module).
    $d2c_uid = NULL;
    // D2C session ready?
    $d2c_user = d2c_current_user();
    if ($d2c_user) {
      $d2c_uid = $d2c_user->uid;
    }
    // Boot phase?
    elseif (!@empty($_SESSION['d2c_data']['uid'])) {
      $d2c_uid = $_SESSION['d2c_data']['uid'];
    }
    // If a D2C uid was found, create the link with the user account.
    if ($d2c_uid != NULL) {
      db_merge('d2c_user_accounts_links')  // Insert if nonexistent, otherwise update.
        ->key(array('d2c_uid' => $d2c_uid))
        ->fields(array('uid' => $account->uid))
        ->execute();
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 * 
 * Adds a custom submit function to the user-admin-role form, to detect role deletions.
 */
function d2c_user_form_user_admin_role_alter(&$form, $form_state, $form_id) {
  $form['#submit'][] = 'd2c_user_user_admin_role_submit';
}

/**
 * Implements hook_form_FORM_ID_alter().
 * 
 * Sets the user email to the d2c email
 */
function d2c_user_form_user_register_alter(&$form, $form_state, $form_id) {
  if (($d2c_user = d2c_current_user()) && $d2c_user->email) {
    $form['mail']['#value'] = $d2c_user->email;
  } 
}

/**
 * Implements hook_form_FORM_ID_alter().
 * 
 * Add d2c specific settings to user profile form.
 */
function d2c_user_form_user_profile_form_alter(&$form, $form_state, $form_id) {
  if ($d2c_uid = d2c_user_link_for_user($form_state['user']->uid)) {
    $form['d2c_user'] = array(
      '#type' => 'fieldset',
      '#title' => t('D2C account link'),
      '#collapsible' => TRUE,
      '#description' => t('Your user account is currently linked with the account \'' . $d2c_uid . '\' at <a href="@d2c">dot2code</a>.', array('@d2c' => 'http://dot2code.com'))
    );
    return $form;
  }
}


/**
 * D2C hooks
 */


/**
 * Implements d2c_core_custom_settings_hook
 * 
 * Adds custom configuration fields to D2C settings form.
 */
function d2c_user_d2c_core_custom_settings_hook() {
  
  drupal_add_js(drupal_get_path('module', 'd2c_user') .'/assets/d2c_user.js');
  drupal_add_css(drupal_get_path('module', 'd2c_user') . '/assets/d2c_user.css');
  
  $form = array();
  
  $form['d2c_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('D2C User settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  
  // User roles mapping settings
  $roles = user_roles(true); //All but anonymous, which is never used
  
  $form['d2c_user']['d2c_user_role_mapping_enabled'] = array(
    '#title' => t('Change user roles for incoming D2C users'),
    '#type' => 'checkbox',
    '#default_value' => d2c_user_settings_get_role_mapping_enabled(),
    '#description' => t('This will change each role for the ones you select. Note that the "authenticated user" role is always preserved.'),
    '#disabled' => (!(count($roles) > 1))
  );
  
  if (count($roles) > 1) {
    $first_role = reset($roles);
    $last_role = end($roles);
    foreach($roles as $role_id => $role_name) {
      $form['d2c_user'][$role_id . '_map'] = array(
        '#title' => t('Roles to assign to role') . " '$role_name'",
        '#type' => 'select',
        '#multiple' => TRUE,
        '#options' => array_diff($roles, array(DRUPAL_AUTHENTICATED_RID => $roles[DRUPAL_AUTHENTICATED_RID])),
        '#default_value' => d2c_user_settings_get_role_mapping($role_id),
        '#attributes' => array('class' => array('d2c-user-role-mapping')),
        '#prefix' => ($first_role == $role_name) ? '<div id="d2c-user-roles-mapping">' : '',
        '#suffix' => ($last_role == $role_name) ? '</div>' : ''
      );
    }
  }
  
  // Accounts link settings
  
  $form['d2c_user']['d2c_user_accounts_link_enabled'] = array(
    '#title' => t('Link local Drupal and remote D2C user accounts on user login'),
    '#type' => 'checkbox',
    '#default_value' => d2c_user_settings_get_accounts_link_enabled(),
    '#description' => t('The first time a user arrives at the Drupal site through a D2C QR code and is logged in -automatically or by providing a login/password-, the linking between their local and D2C accounts is stored in the database.')
  );

  $autologin_warning = '<b>' . t('In order to avoid user authentication bypass based on replay of hijacked D2C access tokens,  if you enable this option you are strongly encouraged to enable checking of timestamps and/or auth tokens of incoming D2C requests (see D2C Core module settings).') . '</b>';
  
  $form['d2c_user']['d2c_user_accounts_link_try_d2c_uid_enabled'] = array(
    '#title' => t('Auto-login local users based on the D2C user id provided by dot2code'),
    '#type' => 'checkbox',
    '#default_value' => d2c_user_settings_get_accounts_link_try_d2c_uid_enabled(),
    '#description' => t('When a user arrives at the Drupal site through a D2C QR code, if their local account is already linked to their D2C account, they will be automatically logged in.') . ' ' . $autologin_warning
  );
  
  $form['d2c_user']['d2c_user_accounts_link_try_mail_enabled'] = array(
    '#title' => t('Auto-login local users based on the user e-mail provided by dot2code'),
    '#type' => 'checkbox',
    '#default_value' => d2c_user_settings_get_accounts_link_try_mail_enabled(),
    '#description' => t('When a user arrives at the Drupal site through a D2C QR code, if the user e-mail provided by dot2code is found in the local database, the user will be automatically logged in.') . ' ' . $autologin_warning
  );
  
  return $form;
}

/**
 * Implements d2c_core_process_custom_settings_hook
 * 
 * Processes the values given to the custom settings fields in the D2C settings form.
 */
function d2c_user_d2c_core_process_custom_settings_hook($values) {
  d2c_user_settings_set_role_mapping_enabled($values['d2c_user_role_mapping_enabled']);
  $roles = user_roles(true); // All but anonymous, which is never used.
  foreach($roles as $role_id => $role_name) {
    $value = @$values[$role_id . '_map'] ? $values[$role_id . '_map'] : array();
    d2c_user_settings_set_role_mapping($role_id, $value);
  }
  d2c_user_settings_set_accounts_link_enabled($values['d2c_user_accounts_link_enabled']);
  d2c_user_settings_set_accounts_link_try_d2c_uid_enabled($values['d2c_user_accounts_link_try_d2c_uid_enabled']);
  d2c_user_settings_set_accounts_link_try_mail_enabled($values['d2c_user_accounts_link_try_mail_enabled']);
}


/**
 * Helper functions
 */


function d2c_user_user_admin_role_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Delete role')) {
    d2c_user_settings_delete_role_mapping($form_state['values']['rid']);
    // If the only roles left are anonymous and authenticated user, role mapping gets disabled.
    $roles = user_roles();
    if (!(count($roles) > 2)) {
      d2c_user_settings_set_role_mapping_enabled(FALSE);
    }
  }
}

/**
 * Returns the d2c_uid linked to a local uid, if any.
 * 
 * @param $uid
 *   The local uid.
 * 
 * @return
 *   The linked d2c_uid if any. NULL otherwise.
 */
function d2c_user_link_for_user($uid) {
  $result = db_select('d2c_user_accounts_links')
    ->fields('d2c_user_accounts_links', array('d2c_uid'))
    ->condition('uid', $uid)
    ->execute()
    ->fetchField();
    
  return $result!==FALSE? $result : NULL;
}

/**
 * Unlinks all D2C User accounts for a certain local user.
 * 
 * @param $uid
 *   The local uid.
 */
function d2c_user_unlink_user($uid) {
  db_delete('d2c_user_accounts_links')->condition('uid', $uid)->execute();
}

/**
 * User roles mapping enabled variable. 
 */

function d2c_user_settings_get_role_mapping_enabled() {
  return variable_get('d2c_user_settings_role_mapping_enabled', FALSE);
}

function d2c_user_settings_set_role_mapping_enabled($enabled) {
  variable_set('d2c_user_settings_role_mapping_enabled', $enabled);
}

function d2c_user_settings_delete_role_mapping_enabled() {
  variable_del('d2c_user_settings_role_mapping_enabled');
}

/**
 * User roles mapping variables. 
 */

function d2c_user_settings_get_role_mapping($role_id) {
  return variable_get('d2c_user_settings_role_mapping_' . $role_id, array($role_id));
}

function d2c_user_settings_set_role_mapping($role_id, $target_role_ids) {
  variable_set('d2c_user_settings_role_mapping_' . $role_id, $target_role_ids);
}

function d2c_user_settings_delete_role_mapping($role_id) {
  variable_del('d2c_user_settings_role_mapping_' . $role_id);
}

function d2c_user_settings_delete_roles_mapping() {
  foreach(user_roles() as $role_id => $role_name) {
    d2c_user_settings_delete_role_mapping($role_id);
  }
}

/**
 * Accounts link enabled variable.
 */

function d2c_user_settings_get_accounts_link_enabled() {
  return variable_get('d2c_user_settings_accounts_link_enabled', TRUE);
}

function d2c_user_settings_set_accounts_link_enabled($enabled) {
  variable_set('d2c_user_settings_accounts_link_enabled', $enabled);
}

function d2c_user_settings_delete_accounts_link_enabled() {
  variable_del('d2c_user_settings_accounts_link_enabled');
}

/**
 * Accounts link try D2C uid enabled variable.
 */

function d2c_user_settings_get_accounts_link_try_d2c_uid_enabled() {
  return variable_get('d2c_user_settings_accounts_link_try_d2c_uid_enabled', FALSE);
}

function d2c_user_settings_set_accounts_link_try_d2c_uid_enabled($enabled) {
  variable_set('d2c_user_settings_accounts_link_try_d2c_uid_enabled', $enabled);
}

function d2c_user_settings_delete_accounts_link_try_d2c_uid_enabled() {
  variable_del('d2c_user_settings_accounts_link_try_d2c_uid_enabled');
}

/**
 * Accounts link try mail enabled variable.
 */

function d2c_user_settings_get_accounts_link_try_mail_enabled() {
  return variable_get('d2c_user_settings_accounts_link_try_mail_enabled', FALSE);
}

function d2c_user_settings_set_accounts_link_try_mail_enabled($enabled) {
  variable_set('d2c_user_settings_accounts_link_try_mail_enabled', $enabled);
}

function d2c_user_settings_delete_accounts_link_try_mail_enabled() {
  variable_del('d2c_user_settings_accounts_link_try_mail_enabled');
}
