===============================================================================
 DESCRIPTION
===============================================================================

This Drupal module provides integration with the dot2code QR code management
service (http://d2c.es). This will enable mobile users to just point at a QR
code with their phones, and the dot2code (D2C) service will (optionally)
authenticate them, capture some users data when required by you (i.e. age,
location, etc.), and redirect them to certain contents in your Drupal site.

You will be able to create custom QR codes as PNG, PDF and SVG images, optionally
bound to your site nodes, and linked to contents such as Drupal nodes, static
files, VCF cards, etc.

A configurable Drupal block is provided as well as a fully configurable new Drupal
field type (known as CCK field in previous Drupal versions). This way you will be
able to auto-generate and attach QR codes to any entity such as nodes, users, etc.

Roles, themes and permissions for users accessing contents through QR codes
can be fine tuned. Even users visiting your site could be authenticated
through D2C if you are interested in this kind of cross-site authentication.

All the created QR codes can be tracked in order to analyze their performance.
Several data visualization tools are provided, as well as some CSV export
facilities for further/advanced analysis.

In short, using this module you’ll get a completely free and extensible QR
code management and tracking dashboard integrated in your Drupal site.

===============================================================================
 MAIN FEATURES
===============================================================================

  - On demand generation of QR codes linking to Drupal nodes, static contents
    (i.e. a PDF brochure, some multimedia content, etc.), VCF cards, etc.
    Ability to fine tune users roles able to access each linked content.

  - Generation of QR codes in PNG, PDF and SVG formats.

  - Attaching of QR codes to any Drupal entity such as nodes, users, etc. A
    fully configurable new field type is provided, as well as a simple
    node-based solution for simpler scenarios.

  - New block to display QR codes attached to nodes.

  - Dashboard with a whole set of tools for codes, domains and campaigns
    management.

  - Integrated analytics service: code, domain and campaign performance
    analysis tools (number of scanned codes, visit length, number of hits per
    visit, etc.).

  - Optional remote cross-site authentication of your Drupal users via D2C
    services.

  - Custom UI theme selection and user role mapping for those users accessing
    your site through a QR code.

  - Raw D2C API access and whole set of Drupal hooks for further module
    customization.

===============================================================================
 SCREENSHOTS
===============================================================================

  - Campaign management dashboard.

      http://d2c.es/images/libraries/drupal/screenshot_campaigns.png

  - Code creation form and  management dashboard.

      http://d2c.es/images/libraries/drupal/screenshot_code_creation.png
      http://d2c.es/images/libraries/drupal/screenshot_code_management.png

  - Node with bound codes.

      http://d2c.es/images/libraries/drupal/screenshot_bound_node_codes.png
      http://d2c.es/images/libraries/drupal/screenshot_attached_codes.png

  - Data visualization tools: code visits, code performance and user
    segmentation by age ranges.

      http://d2c.es/images/libraries/drupal/screenshot_code_visits.png
      http://d2c.es/images/libraries/drupal/screenshot_campaign_code_performance.png
      http://d2c.es/images/libraries/drupal/screenshot_code_age_ranges.png

===============================================================================
 COMPONENTS
===============================================================================

This module is split in several submodules you can enable/disable depending
on your site requirements.

  - D2C Core. Provides the core integration with dot2code's QR code management
    system through its public API (http://d2c.es/help/api). This includes the
    new field type that will allow you to attach QR codes to any Drupal entity.
  
  - D2C Node. Makes it easy to bind your Drupal nodes to D2C QR codes. Each
    node will get a new tab with all its bound codes and a new block will be
    available.

  - D2C User. Lets you link local user accounts with D2C user accounts and
    apply different authentication strategies using data provided by dot2code.
    It also lets you change the roles and themes of incoming visitors to show
    different contents to regular users and D2C powered users.

  - D2C Analytics. Populates the database with statistics about your visitors
    and the D2C QR Codes usage they made. This information will later be
    processed by consumer modules to produce graphs of your interest.
    
    This module behavior can be extended with additional user-defined statistics
    consumers implemented as Drupal modules. Right now, the available consumer
    plugins provided with this package are:
    
      - D2C Analytics Main Consumer. Processes all statistics information
        provided by the D2C Analytics module, building a simple interface to
        show interactive graphs about the use of your D2C QR codes. This
        interface will be reused by different consumer modules that may add
        their own graphs to it.
        
        Currently, all charts are rendered through the great Open Flash Chart
        library (http://teethgrinder.co.uk/open-flash-chart-2/).

      - D2C Analytics Preferences Consumer. Processes user preferences
        statistics like the preferred locales, used interfaces (desktop vs.
        mobile) and favorite browsers.

      - D2C Analytics Profiles Consumer. Processes D2C user profiles statistics
        (such as top age ranges, used e-mails servers, etc.) depending on the
        configuration of your D2C domains.

===============================================================================
 PROJECT ROADMAP
===============================================================================

  - Extended D2C API integration.
  - Improved user experience for codes, domains and campaigns management.
  - Generic data tracking service for improved user-defined metrics.
  - Improved analytics dashboard with more metrics, alternative chart views
    and new tools for better analysis of tracked statistics.
  - New data aggregation strategies for better performance of the analytics
    backend.

===============================================================================
 CONTACT
===============================================================================

  - For general inquiries, please, contact us at info@dot2code.com.  
  - For bug reports and feature suggestions, please, use the module issue
    tracker at github (http://github.com/dot2code/drupal/issues).

===============================================================================
 LINKS
===============================================================================

  - Module homepage at drupal.org (http://drupal.org/project/d2c)
  - Module homepage at github (http://github.com/dot2code) 
  - D2C QR code management service homepage (http://d2c.es)


Copyright 2010-2011, (c) dot2code Technologies S.L. (http://d2c.es)
