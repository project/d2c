<div class="d2c-analytics-main-consumer-d2c-code">
  <img class="d2c-qr-code" id="d2c-qr-code-<?php print $d2c_code->local_id ?>" src="<?php print d2c_settings_code_image_path($d2c_code) ?>" />
  <div class="info">
    <div class="name"><?php print t('QR code:') . ' ' . $d2c_code->name ?></div>
    <div><span class="header"><?php print t('Type') ?>:</span> <?php $types = D2CApi::code_types(); print $types[$d2c_code->code_type] ?></div>
    <div><span class="header"><?php print t('Behaviour') ?>:</span> <?php $behaviours = D2CCode::behaviours(); print $behaviours[$d2c_code->behaviour] ?></div>
    <div><span class="header"><?php print t('Domain') ?>:</span> <?php print D2CDomain::find($d2c_code->domain_id)->name ?></div>
    <?php if ($d2c_code->campaign_id) :?>
      <div><span class="header"><?php print t('Campaign') ?>:</span> <?php print D2CCampaign::find($d2c_code->campaign_id)->name ?></div>
    <?php endif ?>
  </div>
  <div class="buttons">
    <a href="<?php print url('admin/config/d2c/codes/' . $d2c_code->local_id . '/edit') ?>">edit</a> ·
    <a href="#" onclick="(function ($) { $('#d2c_node_toggle_activation_code_form_<?php print $d2c_code->local_id ?>').submit(); })(jQuery);"><?php print($d2c_code->active ? t('deactivate') : t('activate')) ?></a> ·
    <a href="<?php print url('admin/config/d2c/codes/' . $d2c_code->local_id . '/delete') ?>"><?php print(t('delete')) ?></a>
    <form id="d2c_node_toggle_activation_code_form_<?php print $d2c_code->local_id ?>" action="<?php print url('admin/config/d2c/codes/' . $d2c_code->local_id . '/toggle_activation') ?>" method="post">
      <input type="hidden" name="destination" value="/admin/config/d2c/stats?<?php print drupal_http_build_query(drupal_get_query_parameters($_GET, array('q'))) ?>" />
    </form>
  </div>
</div>