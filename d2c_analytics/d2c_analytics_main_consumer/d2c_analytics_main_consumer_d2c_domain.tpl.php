<div class="d2c-analytics-main-consumer-d2c-domain">
  <div class="info">
    <div class="name"><?php print t('D2C domain:') . ' ' . $d2c_domain->name ?></div>
    <div><span class="header"><?php print t('Status') ?>:</span> <?php print $d2c_domain->status ?></div>
    <div><span class="header"><?php print t('Number of used codes') ?>:</span> <?php print $d2c_domain->used_codes() ?></div>
    <div><span class="header"><?php print t('Number of codes left') ?>:</span> <?php print $d2c_domain->codes_left() ?></div>
  </div>
  <div class="buttons">
    <?php print l(t('edit'), 'admin/config/d2c/domains/' . $d2c_domain->id . '/edit') ?> ·
    <?php print l(t('delete'), 'admin/config/d2c/domains/' . $d2c_domain->id . '/delete') ?>
  </div>
</div>