<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

/**
 * Objects of this class will maintain access statistics (visits per time unit) extracted with a
 * certain query to the access statistics table in the database.
 * 
 * They will also offer methods to obtain different representations of that encapsulated information.
 */
class D2CAnalyticsMainConsumerAccessStatistics extends D2CAnalyticsMainConsumerStatistics {

  public $granularity;
  public $visits;
  
  /**
   * Creates a new instance to hold the result of a query to the access statistics DB table.
   * 
   * @param $codes
   *   An array with D2CCode instances representing all the selected codes.
   * @param $start
   *   A timestamp indicating the start of the time window.
   * @param $end
   *   A timestamp indicating the end of the time window.
   * @param $granularity
   *   A granularity value.
   * 
   * @return
   *   An instance of D2CAnalyticsMainConsumerAccessStatistics or NULL if any problem arised.
   */
  public static function find($codes, $start, $end, $granularity) {
    foreach($codes as $code) {
      $codes_ids[] = $code->local_id; 
    }
    $cid = md5('d2c_analytics_main_consumer_access_statistics' . implode($codes_ids) . $start . $end . $granularity);
    if (($cached_result = cache_get($cid, 'cache')) && ($cached_result->expire > REQUEST_TIME)) {
      return $cached_result->data;
    }
    $db_query = "SELECT anonymous_access, authenticated_access, timestamp FROM {d2c_analytics_access_stats} WHERE code_local_id IN (:codes_ids)";
    $db_query .= " AND timestamp >= :start AND timestamp <= :end ORDER BY timestamp ASC"; 
    $result = db_query($db_query, array(':codes_ids' => $codes_ids, ':start' => $start, ':end' => $end));
    if ($result) {      
      $visits = D2CAnalyticsMainConsumerStatistics::timestamps_for_time_window($start, $end, $granularity);
      foreach($result as $row) {
        $timestamp = D2CAnalyticsMainConsumerStatistics::adjust_date($row->timestamp, $granularity);
        $visits[$timestamp] += $row->anonymous_access + $row->authenticated_access;
      }
      $stats = new D2CAnalyticsMainConsumerAccessStatistics($granularity, $visits);
      cache_set($cid, $stats, 'cache', D2CAnalyticsMainConsumerStatistics::expire_for_time_window($start, $end));
      return $stats;
    }   
    else {
      return NULL;
    }
  }
  
  /**
   * Class constructor.
   *
   * @param $granularity
   * @param $visits
   *  
   */
  public function __construct($granularity, $visits = array()) {
    $this->granularity = $granularity;
    $this->visits = $visits;
  }
  
  /**
   * Returns a CSV representation of visits made per time unit.
   * 
   * @return
   *   A string in CSV format.
   */
  public function csv() {
    $csv_stats = array();
    foreach($this->visits as $timestamp => $visits) {
      $csv_stats[] = date('c', $timestamp) . ",$visits";
    }
    return implode("\n", $csv_stats);
  }
  
  /**
   * Returns a JSON representation of visits made per time unit to feed a Open Flash Chart object.
   * 
   * @return
   *   A string in JSON format representing a chart.
   */
  public function line_chart() {
    
    $palette = $this->palette();
    $granularities = D2CAnalyticsMainConsumerStatistics::granularities();
    
    $chart = new OFC_Chart();    
    $chart->set_title(new OFC_Elements_Title(t('Visits') . ' (' . $granularities[$this->granularity] . ')'));
    $chart->bg_colour = $palette['background'];
    
    if ($this->visits) {
      $line_dot = new OFC_Charts_Line();
      $line_dot->set_values(array_values($this->visits));
      $line_dot->colour = $palette['top'];
      $line_dot->{'on-show'} = array('type' => 'pop-up', 'cascade' => '0.5', "delay" => '0.5');
      $chart->add_element($line_dot);
        
      foreach($this->visits as $timestamp => $visits) {
        switch ($this->granularity) {
          case D2CAnalyticsMainConsumerStatistics::GranularityHour:
            $x_labels[] = date("d\nM\nH:i", $timestamp);
            break;
          case D2CAnalyticsMainConsumerStatistics::GranularityDay:
            $x_labels[] = date("d\nM", $timestamp);
            break;
          case D2CAnalyticsMainConsumerStatistics::GranularityWeek:
            $x_labels[] = date("d\nM", $timestamp);
            break;
          default:
            $x_labels[] = date(" M\nY", $timestamp);
            break;
        }
        
        $x_labels_set = new OFC_Elements_Axis_X_Label_Set();
        $x_labels_set->set_labels($x_labels);     
        $x = new OFC_Elements_Axis_X();
        $x->set_labels($x_labels_set);
        $x->set_colours($palette['text'], $palette['bottom']);
        $chart->set_x_axis($x);
        $y = new OFC_Elements_Axis_Y();
        $max = max($this->visits);
        if ($max < 20) {
          $steps = 1;
        } else {
          $steps = pow(10, (strlen((string) $max) - 1)) / 2;
        }
        $y->set_range(0, $max - ($max % $steps) + $steps, $steps);
        $y->set_colours($palette['text'], $palette['bottom']);
        $chart->set_y_axis($y);
      }
      
    }   
    
    return $chart->toPrettyString();
    
  }  
  
}
