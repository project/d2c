<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

/**
 * Objects of this class will maintain access statistics for each QR Code, extracted with a
 * certain query to the access statistics table in the database.
 * 
 * They will also offer methods to obtain different representations of that encapsulated information.
 */
class D2CAnalyticsMainConsumerCodesAccessStatistics extends D2CAnalyticsMainConsumerStatistics {
  
  public $anonymous_access;
  public $authenticated_access;
  public $invalid_data;
  
  /**
   * Creates a new instance to hold the result of a query to the access statistics DB table.
   * 
   * @param $codes
   *   An array with D2CCode instances representing all the selected codes.
   * @param $start
   *   A timestamp indicating the start of the time window.
   * @param $end
   *   A timestamp indicating the end of the time window.
   * 
   * @return
   *   An instance of D2CAnalyticsMainConsumerCodesAccessStatistics or NULL if any problem arised.
   */
  public static function find($codes, $start, $end) {
    foreach($codes as $code) {
      $codes_ids[] = $code->local_id; 
    }
    $cid = md5('d2c_analytics_main_consumer_codes_access_statistics' . implode($codes_ids) . $start . $end);
    if (($cached_result = cache_get($cid, 'cache')) && ($cached_result->expire > REQUEST_TIME)) {
      return $cached_result->data;
    }
    $sql = "SELECT code_local_id, SUM(invalid_data) AS sum_invalid_data, SUM(anonymous_access) AS sum_anonymous_access, SUM(authenticated_access) AS sum_authenticated_access";
    $sql .= " FROM {d2c_analytics_access_stats} WHERE code_local_id IN (:codes_ids)";
    $sql .= " AND timestamp >= :start AND timestamp <= :end GROUP BY code_local_id";
    $result = db_query($sql, array(':codes_ids' => $codes_ids, ':start' => $start, ':end' => $end)); 
    if ($result) {
      $anonymous_access = $authenticated_access = array();
      $invalid_data = 0;
      foreach($result as $row) {
        if ($row->sum_invalid_data) {
          $invalid_data += $row->sum_invalid_data;
        }
        else {
          $anonymous_access[$row->code_local_id] = $row->sum_anonymous_access;
          $authenticated_access[$row->code_local_id] = $row->sum_authenticated_access;
        }
      }
      $stats = new D2CAnalyticsMainConsumerCodesAccessStatistics($anonymous_access, $authenticated_access, $invalid_data);
      cache_set($cid, $stats, 'cache', D2CAnalyticsMainConsumerStatistics::expire_for_time_window($start, $end));
      return $stats;
    }   
    else {
      return NULL;
    }
  }
  
  /**
   * Class constructor.
   *
   * @param $anonymous_access
   * @param $authenticated_access
   * @param $invalid_data
   *  
   */
  public function __construct($anonymous_access = array(), $authenticated_access = array(), $invalid_data = 0) {
    $this->anonymous_access = $anonymous_access;
    $this->authenticated_access = $authenticated_access;
    $this->invalid_data = $invalid_data;
  }
  
  /**
   * Returns a JSON representation of the performance of each accessed QR code, to feed a Open Flash Chart object.
   * 
   * @return
   *   A string in JSON format representing a chart.
   */
  public function codes_pie_chart() {
    
    $values = array();
    
    if ($this->invalid_data) {
      $values['0'] = $this->invalid_data;
    }
    
    $codes = $this->codes();
    
    foreach($codes as $code) {
      $visits = $this->anonymous_access[$code->local_id] + $this->authenticated_access[$code->local_id];
      $values["{$code->name} ({$code->local_id})"] = $visits;
    }
    
    return $this->pie_chart($values);
    
  }
  
  /**
   * Returns data to fill up a table representation of this object.
   * 
   * @return
   *   An indexed array with a 'headers' element and a 'rows' element.
   */
  public function table() {
    $palette = array_values($this->get_palette());
    
    $rows = array();
    $codes = $this->codes();
    $total = 0;
    foreach($codes as $code) {
      $total += $this->anonymous_access[$code->local_id] + $this->authenticated_access[$code->local_id];
    }
    
    if ($this->invalid_data) {
      $total += $this->invalid_data;
      $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[0] . '">', t('Invalid data'), $this->invalid_data, number_format($this->invalid_data * 100 / $total, 2) . '%');
    }
    
    $i = 1;
    foreach($codes as $code) {
      $visits = $this->anonymous_access[$code->local_id] + $this->authenticated_access[$code->local_id];
      $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[$i++] . '">', l($code->name, 'admin/config/d2c/stats', array('query' => array('type' => 'code', 'id' => $code->local_id))), $visits, $total ? (round($visits * 100 / $total, 2) . '%') : '-');
    }
    
    // Order by total
    $totals = array();
    foreach ($rows as $row) {
      $totals[] = $row[2];
    }
    array_multisort($totals, SORT_DESC, $rows);  
    
    return array(
      'headers' => array('', t('Name'), t('Visits'), t('Percentage')),
      'rows' => $rows
    );
  }
  
  /**
   * Private methods
   */
  
  /**
   * Returns the list of codes for which this instance has information about.
   * 
   * @return
   *   An array with D2CCode instances.
   */  
  private function codes() {
    if (empty($this->anonymous_access)) {
      return array();
    }
    else {
      return D2CCode::find(array_keys($this->anonymous_access));
    }
  }
  
}
