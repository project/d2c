<div class="d2c-analytics-main-consumer-d2c-site">
  <div class="info">
    <div class="name"><?php print t('Site statistics') ?></div>
    <br/>
    <div class="item">• <?php echo t('Click <a href="@here">here</a> for code stats', array('@here' => url('admin/config/d2c/codes'))); ?></div>
    <div class="item">• <?php echo t('Click <a href="@here">here</a> for aggregated campaign stats', array('@here' => url('admin/config/d2c/campaigns'))); ?></div>
    <div class="item">• <?php echo t('Click <a href="@here">here</a> for aggregated domain stats', array('@here' => url('admin/config/d2c/domains'))); ?></div>
  </div>
</div>