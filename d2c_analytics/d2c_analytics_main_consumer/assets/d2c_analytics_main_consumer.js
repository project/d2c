(function ($) {
  Drupal.behaviors.d2cAnalyticsMainConsumerBehavior = {
    attach: function(context){
    
      /*
     * JS for AJAX operations over tabular content in statistics page.
     */
      $(context).find('#d2c-analytics-main-consumer-statistics-tabs a').bind('click', function(){
        container = $('#d2c-analytics-main-consumer-statistics-tab-content');
        var link = $(this);
        container.load(link.attr('href'), function(responseText, textStatus, XMLHttpRequest){
          Drupal.behaviors.d2cAnalyticsMainConsumerBehavior.attach(container);
          $(context).find('#d2c-analytics-main-consumer-statistics-tabs li').removeClass('active');
          link.parent('li').addClass('active');
        });
        return false;
      });
      
      autoload_tab_content = $(context).find('#d2c-analytics-main-consumer-statistics-tab-content.autoload');
      
      if (autoload_tab_content) {
        autoload_tab_content.removeClass('autoload');
        $(context).find('#d2c-analytics-main-consumer-statistics-tabs a:first').click();
      }
      
      $(context).find('#d2c-analytics-main-consumer-statistics-subtabs a').bind('click', function(){
        container = $('#d2c-analytics-main-consumer-statistics-subtab-content');
        var link = $(this);
        container.load(link.attr('href'), function(responseText, textStatus, XMLHttpRequest){
          $(context).find('#d2c-analytics-main-consumer-statistics-subtabs a').removeClass('active');
          link.addClass('active');
        });
        return false;
      });
      
      autoload_subtab_content = $(context).find('#d2c-analytics-main-consumer-statistics-subtab-content.autoload');
      
      if (autoload_subtab_content) {
        autoload_subtab_content.removeClass('autoload');
        $(context).find('#d2c-analytics-main-consumer-statistics-subtabs a:first').click();
      }
      
      // Date picker and tabs behaviour is applied if JQuery UI is available.
      if (typeof($.ui) !== 'undefined') {
        $(context).find('input.datepicker').datepicker({
          dateFormat: 'dd-mm-yy'
        });
      };
    }
  }
})(jQuery);