<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

/**
 * Objects of this class will maintain aggregated access statistics (totals, averages, ...) extracted
 * with a certain query to the access statistics table in the database.
 * 
 * They will also offer methods to obtain different representations of that encapsulated information.
 */
class D2CAnalyticsMainConsumerAggregateAccessStatistics extends D2CAnalyticsMainConsumerStatistics {

  public $sum_invalid_data;
  public $sum_invalid_timestamp;
  public $sum_invalid_auth_token;
  public $sum_anonymous_access;
  public $sum_authenticated_access;
  public $sum_op_login;
  public $sum_op_register;
  
  public $avg_anonymous_access;
  public $avg_authenticated_access;
  public $avg_session_length;
  public $avg_hits;
  
  /**
   * Creates a new instance to hold the result of a query to the access statistics DB table.
   * 
   * @param $codes
   *   An array with D2CCode instances representing all the selected codes.
   * @param $start
   *   A timestamp indicating the start of the time window.
   * @param $end
   *   A timestamp indicating the end of the time window.
   * 
   * @return
   *   An instance of D2CAnalyticsMainConsumerAggregateAccessStatistics or NULL if any problem arised.
   */
  public static function find($codes, $start, $end) {
    foreach($codes as $code) {
      $codes_ids[] = $code->local_id; 
    }
    $cid = md5('d2c_analytics_main_consumer_aggregate_access_statistics' . implode($codes_ids) . $start . $end);
    if (($cached_result = cache_get($cid, 'cache')) && ($cached_result->expire > REQUEST_TIME)) {
      return $cached_result->data;
    }
      
    $db_query = 'SELECT SUM(invalid_data) AS sum_invalid_data, SUM(invalid_timestamp) AS sum_invalid_timestamp,';
    $db_query .= ' SUM(invalid_auth_token) AS sum_invalid_auth_token, SUM(anonymous_access) AS sum_anonymous_access,';
    $db_query .= ' SUM(authenticated_access) as sum_authenticated_access, SUM(op_login) as sum_op_login,';
    $db_query .= ' SUM(op_register) as sum_op_register,';
    $db_query .= ' AVG(anonymous_access) AS avg_anonymous_access, AVG(authenticated_access) AS avg_authenticated_access,';
    $db_query .= ' AVG(avg_session_length) AS avg_session_length, AVG(avg_hits) AS avg_hits';
    $db_query .= ' FROM {d2c_analytics_access_stats}';
    $db_query .= ' WHERE code_local_id IN (:codes_ids) AND timestamp >= :start AND timestamp <= :end';
    $result = db_query($db_query, array(':codes_ids' => $codes_ids, ':start' => $start, ':end' => $end));
    if($result && ($row = $result->fetchAssoc())) {
      $stats = new D2CAnalyticsMainConsumerAggregateAccessStatistics($row);
      cache_set($cid, $stats, 'cache', D2CAnalyticsMainConsumerStatistics::expire_for_time_window($start, $end));
      return $stats;
    }
    else {
      return NULL;
    }
  }
  
  /**
   * Class constructor.
   *
   * @param $attributes
   *  
   */
  public function __construct($attributes = array()) {
    foreach($attributes as $key => $value) {
      $this->$key = (int) $value;
    }
  }
  
  /**
   * Returns the total of visits.
   * 
   * @return
   *   An integer with the total of visits.
   */
  public function visits() {
    return $this->sum_anonymous_access + $this->sum_authenticated_access;
  }
  
  /**
   * Returns the average of visits.
   * 
   * @return
   *   An integer with the average of visits.
   */
  public function avg_visits() {
    return $this->avg_anonymous_access + $this->avg_authenticated_access;
  }
  
  /**
   * Returns the percentage of visits that ended in a login operation.
   * 
   * @return
   *   A float value with the percentage of login operations.
   */
  public function per_op_login() {
    return $this->visits() ? ($this->sum_op_login * 100 / $this->visits()) : 0;
  }
  
  /**
   * Returns the percentage of visits that ended in a registration operation.
   * 
   * @return
   *   A float value with the percentage of registration operations.
   */
  public function per_op_register() {
    return $this->visits() ? ($this->sum_op_register * 100 / $this->visits()) : 0;
  }
  
  /**
   * Returns a JSON representation of the type of accesses that have been made, to feed a Open Flash Chart object.
   * 
   * @return
   *   A string in JSON format representing a chart.
   */
  public function access_pie_chart() {
    
    return $this->pie_chart(array(
      t('Invalid data') => $this->sum_invalid_data,
      t('Invalid timestamp') => $this->sum_invalid_timestamp,
      t('Invalid token') => $this->sum_invalid_auth_token,
      t('Anonymous') => $this->sum_anonymous_access,
      t('Authenticated') => $this->sum_authenticated_access
    ));
    
  }
  
  /**
   * Returns data to fill up a table representation of this object.
   * 
   * @return
   *   An indexed array with a 'headers' element and a 'rows' element.
   */
  public function table() {
    $palette = array_values($this->get_palette());
    $total = $this->sum_invalid_data + $this->sum_invalid_timestamp + $this->sum_invalid_auth_token + $this->sum_anonymous_access + $this->sum_authenticated_access;
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[0] . '">', t('Invalid data'), $this->sum_invalid_data, $total ? (number_format($this->sum_invalid_data * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[1] . '">', t('Invalid timestamp'), $this->sum_invalid_timestamp, $total ? (number_format($this->sum_invalid_timestamp * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[2] . '">', t('Invalid auth token'), $this->sum_invalid_auth_token, $total ? (number_format($this->sum_invalid_auth_token * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[3] . '">', t('Anonymous'), $this->sum_anonymous_access, $total ? (number_format($this->sum_anonymous_access * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[4] . '">', t('Authenticated'), $this->sum_authenticated_access, $total ? (number_format($this->sum_authenticated_access * 100 / $total, 2) . '%') : '-');
    // Order by total
    foreach ($rows as $row) {
      $totals[] = $row[2];
    }
    array_multisort($totals, SORT_DESC, $rows);
    return array(
      'headers' => array('', t('Access type'), t('Total'), t('Percentage')),
      'rows' => $rows
    );
  }
  
}

?>