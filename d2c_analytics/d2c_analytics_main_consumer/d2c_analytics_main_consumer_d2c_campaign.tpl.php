<div class="d2c-analytics-main-consumer-d2c-campaign">
  <div class="info">
    <div class="name"><?php print t('D2C campaign:') . ' ' . $d2c_campaign->name ?></div>
    <div><span class="header"><?php print t('Number of codes') ?>:</span> <?php print $d2c_campaign->codes_number() ?></div>
  </div>
  <div class="buttons">
    <?php print l(t('edit'), 'admin/config/d2c/campaigns/' . $d2c_campaign->id . '/edit') ?> ·
    <?php print l(t('delete'), 'admin/config/d2c/campaigns/' . $d2c_campaign->id . '/delete') ?>
  </div>
</div>