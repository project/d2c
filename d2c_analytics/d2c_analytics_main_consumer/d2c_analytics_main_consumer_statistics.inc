<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

/**
 *
 */
class D2CAnalyticsMainConsumerStatistics {
  
  /**
   * Granularity constants.
   */
  const GranularityHour = 3600;
  const GranularityDay = 86400;
  const GranularityWeek = 604800;
  const GranularityMonth = 2682000;
  
  /**
   * Returns the array of available granularities constants, with a human readable name for each of them.
   * 
   * @return
   *   An indexed array. Indexes are the constants and values are their human readable names.
   */
  public static function granularities() {
    return array(
      D2CAnalyticsMainConsumerStatistics::GranularityHour => t('per hour'),
      D2CAnalyticsMainConsumerStatistics::GranularityDay => t('per day'),
      D2CAnalyticsMainConsumerStatistics::GranularityWeek => t('per week'),
      D2CAnalyticsMainConsumerStatistics::GranularityMonth => t('per month')
    );
  }
  
  /**
   * Defines an appropriate time window from a initial start and end date.
   * 
   * A nice granularity will be selected depeding on the start and end timestamps, and then both of
   * them will be adjusted to conform to that granularity.
   * 
   * @param $start
   *   An initial timestamp.
   * @param $end
   *   A final timestamp.
   *   
   * @return
   *   An array with three elements: the new start timestamp, the new end timestamp and the used granularity.
   */
  public static function time_window($start, $end) {
    $granularity = D2CAnalyticsMainConsumerStatistics::granularity_for_time_window($start, $end);
    $start = D2CAnalyticsMainConsumerStatistics::adjust_date($start, $granularity);
    $end = D2CAnalyticsMainConsumerStatistics::adjust_date($end, $granularity);
    $end = D2CAnalyticsMainConsumerStatistics::adjust_date($end + $granularity, $granularity);
    return array($start, $end, $granularity);
  }
  
  /**
   * Adjusts a certain timestamp to a valid value considering a certain granularity.
   * 
   * @param $timestamp
   *   A timestamp to adjust.
   * @param $granularity
   *   An integer with the granularity to use (One of the constants defined in this class should be used).
   * 
   * @return
   *   The new adjusted timestamp.
   */
  public static function adjust_date($timestamp, $granularity) {
    switch ($granularity) {
      case D2CAnalyticsMainConsumerStatistics::GranularityMonth:
        $time_data = getdate($timestamp);
        $timestamp = mktime(0, 0, 0, $time_data['mon'], 1, $time_data['year']);
        break;
      default:
        $timestamp = $timestamp - $timestamp % $granularity;
    }
    return $timestamp;
  }
  
  /**
   * Returns an indexed array with entries for all timestamps within a time window
   * and with a certain granularity. All of the entries will be initialized to a 0 value.
   * 
   * @param $start
   *   An initial timestamp.
   * @param $end
   *   A final timestamp.
   * @param $granularity
   *   An integer with the granularity to use (One of the constants defined in this class should be used).
   *   
   * @return
   *   An indexed array.
   */
  public static function timestamps_for_time_window($start, $end, $granularity) {
    $timestamps = array();
    $i = $start;
    do {    
      $timestamps[$i] = 0;
      $i = D2CAnalyticsMainConsumerStatistics::adjust_date($i + $granularity, $granularity);
    } while ($i <= $end);
    return $timestamps;
  }
  
  /**
   * Returns an expiration time for data in a certain time window.
   * 
   * @param $start
   *   An initial timestamp.
   * @param $end
   *   A final timestamp.
   *   
   * @return
   *   A timestamp for the expiration date.
   */
  public static function expire_for_time_window($start, $end) {
    $time_window_length = $end - $start;
    $time = REQUEST_TIME;
    // Time windows ending in the past (yesterday or before).
    if ($end < $time - 24 * 60 * 60) {
      // 1 hour for expiration time.
      return $time + 60 * 60;
    }
    // Time windows including the present day.
    else {
      // Time window for up to one day.
      if ($time_window_length <= 24 * 60 * 60) {
        // 5 minutes for expiration time.
        return $time + 5 * 60;
      }
      // Time window for up to one month.
      elseif ($time_window_length <= 31 * 24 * 60 * 60) {
        // 10 minutes for expiration time.
        return $time + 10 * 60;
      }
      else {
        // 15 minutes for expiration time.
        return $time + 15 * 60;
      }
    }
  }
  
  /**
   * Returns the palette to use in all charts.
   * 
   * @return
   *   An indexed array with the colours to use.
   */
  public static function get_palette() {
    // Prepend with the major hexadecimal color codes.
    $palette = array('red' => '#FF0000',
                     'grass green' => '#408080',
                     'orange' => '#FF8040',
                     'dark blue' => '#0000A0',
                     'dark purple' => '#800080',
                     'brown' => '#804000',
                     'yellow' => '#FFFF00',
                     'burgundy' => '#800000',
                     'pastel green' => '#00FF00',
                     'forest green' => '#808000',
                     'turquoise' => '#00FFFF',
                     'light blue' => '#0000FF',
                     'pink' => '#FF00FF',
                     'light purple' => '#FF0080',
                     'light grey' => '#C0C0C0',
                    );
    // Add some other element dependant colors.
    $palette['base'] = '#0072b9';
    $palette['link'] = '#027ac6';
    $palette['top'] = '#2385c2';
    $palette['bottom'] = '#5ab5ee';
    $palette['text'] = '#494949';
    $palette['background'] = '#FFFFFF';
    if (module_exists('color')) {
      init_theme();
      global $theme_key;
      if(color_get_info($theme_key)) {
        $palette = array_merge($palette, color_get_palette($theme_key));
      }
    }
    return $palette;
  }
  
  
  /**
   * Instance methods
   */
  
  
  /**
   * Returns the palette to use in all charts.
   * 
   * @return
   *   An indexed array with the colours to use.
   */
  protected function palette() {
    return D2CAnalyticsMainConsumerStatistics::get_palette();
  }
  
  
  /**
   * Private methods
   */
  
  
  /**
   * Returns the most suitable granularity to use for a certain time window.
   * 
   * @param $start
   *   A timestamp indicating the start of the time window.
   * @param $end
   *   A timestamp indicating the end of the time window.
   *   
   * @return
   *   An integer with the selected granularity.
   */
  private static function granularity_for_time_window($start, $end) {
    $difference = $end - $start;
    if ($difference <= 1 * 24 * 60 * 60) {
      return D2CAnalyticsMainConsumerStatistics::GranularityHour;
    }
    elseif ($difference <= 31 * 24 * 60 * 60) {
      return D2CAnalyticsMainConsumerStatistics::GranularityDay;
    }
    elseif ($difference <= 6 * 31 * 24 * 60 * 60) {
      return D2CAnalyticsMainConsumerStatistics::GranularityWeek;
    }
    else {
      return D2CAnalyticsMainConsumerStatistics::GranularityMonth;
    }
  }
  
  /**
   * Generates a JSON representation of some information to be shown by a Open Flash Chart
   * as a pie chart.
   */
  protected function pie_chart($values, $title='') {
    
    $palette = $this->palette();
    
    $pie = new OFC_Charts_Pie();
    $pie->set_animate(FALSE);
    $pie->tip = t('#val# of #total# [#percent#]');
    $pie->colours = array_values($palette);
    $pie->set_start_angle(0);
    
    $pie_values = array();
    foreach($values as $label => $value) {
      $pie_values[] = new OFC_Charts_Pie_Value($value, $label);
    }
    
    $pie->values = $pie_values;
    $pie->set_animate(TRUE);
    $pie->alpha = 1;
    $pie->{'gradient-fill'} = 'true';
    $pie->tip = '#label#<br>#val# (#percent#)';
    $pie->{'no-labels'} = true;

    $chart = new OFC_Chart();
    $chart->add_element($pie);
    $chart->set_title($title);
    $chart->bg_colour = $palette['background'];
    
    return $chart->toPrettyString();
    
  }
  
}
