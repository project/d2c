<?php print theme('table', array('header' => $tabular_info['headers'], 'rows' => $tabular_info['rows'])); ?>

<div id="d2c-analytics-stats-subtab-chart"></div>

<script type="text/javascript">
  <?php $query = array('type' => $_GET['type'], 'id' => $_GET['id'], 'start' => $_GET['start'], 'end' => $_GET['end'], 'granularity' => $_GET['granularity']) ?>
  swfobject.embedSWF("<?php print drupal_get_path('module', 'd2c_analytics_main_consumer') . '/assets/open-flash-chart.swf'?>", "d2c-analytics-stats-subtab-chart", "400", "300", "9.0.0", "expressInstall.swf", {"data-file":"<?php print urlencode(url($chart_data, array('query' => $query, 'absolute' => TRUE))) ?> "});
</script>
