<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

/**
 * 
 * 
 */
class D2CAnalyticsSessionStats {
  
  /**
   * Constants for the different session status.
   */
  const StatusOK = 0;
  const StatusInvalidData = 1;
  const StatusInvalidTimestamp = 2;
  const StatusInvalidAuthToken = 3;
  
  /**
   * Constants for the different known user agents.
   */
  const UserAgentUnknown = 0;
  const UserAgentChrome = 1;
  const UserAgentFF = 2;
  const UserAgentIE7 = 3;
  const UserAgentIE8 = 4;
  const UserAgentSafari = 5;
  
  public $session_id;
  public $session_status;
  public $auth_status;
  public $timestamp;
  public $last_timestamp;
  public $hits;
  public $uid;
  public $ip;
  public $user_agent;
  public $locale;
  public $code_local_id;
  public $interface;
  public $user_profile;
  public $op_login;
  public $op_register;
  public $op_unregister;
  public $processed;
  
  private $new_record;
  
  /**
   *  Class methods
   */
  
  /**
   * Finds a certain D2C analytics session stats object in the local database.
   * 
   * @param $session_id
   *   An integer with the session_id for the target object. 
   *   
   * @return
   *   A D2CAnalyticsSessionStats instance if the object was found. Null, otherwise.
   */
  public static function find($session_id) {
    $result = db_query("SELECT * FROM {d2c_analytics_session_stats} WHERE session_id = :session_id", array(':session_id' => $session_id));
    
    if ($attributes = $result->fetchAssoc()) {
        return new D2CAnalyticsSessionStats($attributes);
    }
    return NULL;
  }
  
  /**
   * Searches for D2CAnalyticSessionStats that fit to the given restrictions
   * 
   * @param $processed
   *   A boolean indicating if searching for processed stats (TRUE) or unprocessed stats (FALSE).
   * @param $window_start
   *   (optional) A timestamp. No stat logged before it will be returned.
   * @param $window_end
   *   (optional) A timestamp. No stat logged after it will be returned.
   * @param $limit
   *   (optional) A integer to determine the limit of rows to return.
   *   
   * @return
   *   An array of D2CAnalyticSessionStats. A single D2CAnalyticSessionStats instance if $limit = 1 (and
   *   the search returned a row).
   */
  public static function filter($processed, $window_start = NULL, $window_end = NULL, $limit = NULL) {
    $query = db_select('d2c_analytics_session_stats')
      ->fields('d2c_analytics_session_stats')
      ->condition('processed', $processed);
    if ($window_start) {
      $query = $query->condition('timestamp', $window_start, '>=');
    }
    if ($window_end) {
      $query = $query->condition('timestamp', $window_end, '<=');
    }
    if ($limit) {
      $query = $query->range(0, $limit);
    }
    $result = $query->execute()->fetchAll();
    $rows = array();
    foreach($result as $row){
      if ($row->user_profile) {
        $row->user_profile = unserialize($row->user_profile);
      }
      $rows[] = (array) $row;
    }
    if($rows) {
      if ($limit == 1) {
        return $rows[0];
      } else {
        return $rows;
      }
    } else {
      return array();
    }
  }
  
  /**
   * Sets the processed mark to all D2CAnalyticsSessionStats between two timstamps.
   * 
   * @param $window_start
   *   Start timestamp.
   * @param $window_end
   *   End timestamp.
   */
  public static function set_as_processed($window_start, $window_end) {
    db_update('d2c_analytics_session_stats')
      ->fields(array('processed' => 1))
      ->condition('processed', 0)
      ->condition('timestamp', $window_start, '>=')
      ->condition('timestamp', $window_end, '<=')
      ->execute();
  }
  
  /**
   * Checks if a certain QR code has its statistics enabled.
   * 
   * @param $code_id
   *   (optional) The local id for the QR code. If not supplied, the default value for a new QR code will be returned.
   *   
   * @return
   *   A boolean indicating if the statistics are enabled or not.
   */
  public static function enabled_for_code($code_id = NULL) {
    if ($code_id) {
      $result = db_query("SELECT * FROM {d2c_analytics_enabled_codes} WHERE code_local_id = :code_id", array(':code_id' => $code_id));
      if ($attributes = $result->fetchAssoc()) {
        return $attributes['enabled'];
      }
    }
    return TRUE;
  }
  
  /**
   * Enables / Disables statistics for a certain QR code.
   * 
   * @param $code_id
   *   The local id for the QR code.
   * @param $enabled
   *   A boolean indicating if the statistics must be enabled or not for this QR code.
   */
  public static function set_enabled_for_code($code_id, $enabled) {
    db_merge('d2c_analytics_enabled_codes')
      ->key(array('code_local_id' => $code_id))
      ->fields(array('enabled' => $enabled))
      ->execute();
  }
  
  /**
   * Deletes the enabled / disabled statistics information for a certain QR code.
   * 
   * @param $code_id
   *   The local id for the QR code.
   */
  public static function delete_enabled_for_code($code_id) {
    db_delete('d2c_analytics_enabled_codes')
      ->condition('code_local_id', $code_id)
      ->execute();
  }
  
  /**
   * Process a user agent string and returns a constant representing the browser used.
   * 
   * @param $user_agent
   *   A user agent string.
   *   
   * @return
   *   One of the constants defined in this class representing a browser.
   */
  public static function process_user_agent($user_agent) {
  
    switch(TRUE) {
      case preg_match("/chrome/i", $user_agent):
        return D2CAnalyticsSessionStats::UserAgentChrome;
      case preg_match("/firefox/i", $user_agent):
        return D2CAnalyticsSessionStats::UserAgentFF;
      case preg_match("/msie 7/i", $user_agent):
        return D2CAnalyticsSessionStats::UserAgentIE7;
      case preg_match("/msie 8/i", $user_agent):
        return D2CAnalyticsSessionStats::UserAgentIE8;
      case preg_match("/applewebkit/i", $user_agent):
        return D2CAnalyticsSessionStats::UserAgentSafari;
      default:
        return D2CAnalyticsSessionStats::UserAgentUnknown;
    }
    
  }
  
  /**
   * Class constructor.
   *
   * Default values will be given to all attributes unless explicit values are provided for them.
   *
   * @param $attributes
   *   (optional) An associative array with values for one or more of the QR code's attributes. 
   */
  public function __construct($attributes = array()) {
    if (@$attributes['session_id']) {
      $this->new_record = FALSE;
    }
    else {
      $this->new_record = TRUE;
      $this->session_id = REQUEST_TIME + rand(100, 999) * 1000000000;
    }
    $this->set_defaults();
    $this->set_attributes($attributes);
  }

  
  /**
   *  Instance methods
   */
  
  /**
   * Sets values for all provided attributes.
   * 
   * Unkown attributes will be ignored.
   * 
   * @param $attributes
   *   An associative array with values for one or more of the object's attributes.
   */
  public function set_attributes($attributes) {
    foreach($attributes as $attr => $value) {
      try {
        // Behaviour params data is unserialized if needed.
        if (($attr == 'user_profile') and is_string($value)) {
          $value = unserialize($value);
        }
        $this->$attr = $value;
      }
      catch (Exception $e) {}
    }
  }
  
  /**
   * Saves the instance to the local database. This can be both an INSERT (for new instances) or an UPDATE.
   * 
   * @return
   *   Failure to write a record will return FALSE. Otherwise SAVED_NEW or SAVED_UPDATED is returned
   *   depending on the operation performed.
   */
  public function save() {
    $res =  drupal_write_record('d2c_analytics_session_stats', $this, $this->new_record? array() : "session_id");
    return $res;
  }

  /**
   *  Private methods
   */
  
  /**
   * Sets default values for the attributes in this instance.
   */
  private function set_defaults() {
    $this->session_status = D2CAnalyticsSessionStats::StatusOK;
    $this->timestamp = REQUEST_TIME;
    $this->last_timestamp = REQUEST_TIME;
    $this->hits = 0;
    $this->op_login = FALSE;
    $this->op_register = FALSE;
    $this->op_unregister = FALSE;
    $this->processed = FALSE;
  }
  
}
