<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

/**
 * Objects of this class will maintain aggregated preferences statistics extracted with a certain
 * query to the preferences statistics table in the database.
 * 
 * They will also offer methods to obtain different representations of that encapsulated information.
 */
class D2CAnalyticsPreferencesConsumerAggregateStatistics extends D2CAnalyticsMainConsumerStatistics {

  public $sum_desktop;
  public $sum_mobile;
  public $sum_locale_en;
  public $sum_locale_es;
  public $sum_locale_gl;
  public $sum_locale_other;
  public $sum_user_agent_chrome;
  public $sum_user_agent_ff;
  public $sum_user_agent_ie7;
  public $sum_user_agent_ie8;
  public $sum_user_agent_safari;
  public $sum_user_agent_unknown;
  
  /**
   * Creates a new instance to hold the result of a query to the preferences statistics DB table.
   * 
   * @param $codes
   *   An array with D2CCode instances representing all the selected codes.
   * @param $start
   *   A timestamp indicating the start of the time window.
   * @param $end
   *   A timestamp indicating the end of the time window.
   * 
   * @return
   *   An instance of D2CAnalyticsPreferencesConsumerAggregateStatistics or NULL if any problem arised.
   */
  public static function find($codes, $start, $end) {
    foreach($codes as $code) {
      $codes_ids[] = $code->local_id; 
    }
    $cid = md5('d2c_analytics_preferences_consumer_aggregate_statistics' . implode($codes_ids) . $start . $end);
    if (($cached_result = cache_get($cid, 'cache')) && ($cached_result->expire > REQUEST_TIME)) {
      return $cached_result->data;
    }
    $sql = 'SELECT SUM(desktop) AS sum_desktop, SUM(mobile) AS sum_mobile,';
    $sql .= ' SUM(locale_en) AS sum_locale_en, SUM(locale_es) AS sum_locale_es, SUM(locale_gl) AS sum_locale_gl,';
    $sql .= ' SUM(locale_other) AS sum_locale_other, SUM(user_agent_chrome) AS sum_user_agent_chrome,';
    $sql .= ' SUM(user_agent_ff) AS sum_user_agent_ff, SUM(user_agent_ie7) as sum_user_agent_ie7,';
    $sql .= ' SUM(user_agent_ie8) AS sum_user_agent_ie8, SUM(user_agent_safari) AS sum_user_agent_safari,';
    $sql .= ' SUM(user_agent_unknown) AS sum_user_agent_unknown';
    $sql .= ' FROM {d2c_analytics_preferences_stats}';
    $sql .= ' WHERE code_local_id IN (:codes_ids) AND timestamp >= :start AND timestamp <= :end';
    $result = db_query($sql, array(':codes_ids' => $codes_ids, ':start' => $start, ':end' => $end));
    if($result && ($row = $result->fetchAssoc())) {
      $stats = new D2CAnalyticsPreferencesConsumerAggregateStatistics($row);
      cache_set($cid, $stats, 'cache', D2CAnalyticsMainConsumerStatistics::expire_for_time_window($start, $end));
      return $stats;
    }
    else {
      return NULL;
    }
  }
  
  /**
   * Class constructor.
   *
   * @param $attributes
   *  
   */
  public function __construct($attributes = array()) {
    foreach($attributes as $key => $value) {
      $this->$key = (int) $value;
    }
  }
  
  /**
   * Returns data to fill up a table representation of the interfaces information in this object.
   * 
   * @return
   *   An indexed array with a 'headers' element and a 'rows' element.
   */
  public function interfaces_table() {
    $palette = array_values($this->get_palette());
    $total = $this->sum_desktop + $this->sum_mobile;
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[0] . '">', t('Desktop'), $this->sum_desktop, $total ? (number_format($this->sum_desktop * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[1] . '">', t('Mobile'), $this->sum_mobile, $total ? (number_format($this->sum_mobile * 100 / $total, 2) . '%') : '-');
    // Order by total
    foreach ($rows as $row) {
      $totals[] = $row[2];
    }
    array_multisort($totals, SORT_DESC, $rows); 
    return array(
      'headers' => array('', t('Interface'), t('Total'), t('Percentage')),
      'rows' => $rows
    );
  }
  
  /**
   * Returns data to fill up a table representation of the locales information in this object.
   * 
   * @return
   *   An indexed array with a 'headers' element and a 'rows' element.
   */
  public function locales_table() {
    $palette = array_values($this->get_palette());
    $total = $this->sum_locale_en + $this->sum_locale_es + $this->sum_locale_gl + $this->sum_locale_other;
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[0] . '">', t('English'), $this->sum_locale_en, $total ? (number_format($this->sum_locale_en * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[1] . '">', t('Spanish'), $this->sum_locale_es, $total ? (number_format($this->sum_locale_es * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[2] . '">', t('Galician'), $this->sum_locale_gl, $total ? (number_format($this->sum_locale_gl * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[3] . '">', t('Other'), $this->sum_locale_other, $total ? (number_format($this->sum_locale_other * 100 / $total, 2) . '%') : '-');
    // Order by total
    foreach ($rows as $row) {
      $totals[] = $row[2];
    }
    array_multisort($totals, SORT_DESC, $rows); 
    return array(
      'headers' => array('', t('Preferred locale'), t('Total'), t('Percentage')),
      'rows' => $rows
    );
  }
  
  /**
   * Returns data to fill up a table representation of the user agents information in this object.
   * 
   * @return
   *   An indexed array with a 'headers' element and a 'rows' element.
   */
  public function user_agents_table() {
    $palette = array_values($this->get_palette());
    $total = $this->sum_user_agent_chrome + $this->sum_user_agent_ff + $this->sum_user_agent_ie7 + $this->sum_user_agent_ie8 + $this->sum_user_agent_safari + $this->sum_user_agent_unknown;
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[0] . '">', 'Google Chrome', $this->sum_user_agent_chrome, $total ? (number_format($this->sum_user_agent_chrome * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[1] . '">', 'Firefox', $this->sum_user_agent_ff, $total ? (number_format($this->sum_user_agent_ff * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[2] . '">', 'Internet Explorer 7', $this->sum_user_agent_ie7, $total ? (number_format($this->sum_user_agent_ie7 * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[3] . '">', 'Internet Explorer 8', $this->sum_user_agent_ie8, $total ? (number_format($this->sum_user_agent_ie8 * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[4] . '">', 'Safari', $this->sum_user_agent_safari, $total ? (number_format($this->sum_user_agent_safari * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[5] . '">', t('Unknown'), $this->sum_user_agent_unknown, $total ? (number_format($this->sum_user_agent_unknown * 100 / $total, 2) . '%') : '-');
    // Order by total
    foreach ($rows as $row) {
      $totals[] = $row[2];
    }
    array_multisort($totals, SORT_DESC, $rows); 
    return array(
      'headers' => array('', t('User browser'), t('Total'), t('Percentage')),
      'rows' => $rows
    );
  }
  
  /**
   * Returns a JSON representation of the interfaces information in this object, to feed a Open Flash Chart object.
   * 
   * @return
   *   A string in JSON format representing a chart.
   */
  public function interfaces_chart() {
    
    return $this->pie_chart(array(
      t('Desktop') => $this->sum_desktop,
      t('Mobile') => $this->sum_mobile
    ));
  }
  
  /**
   * Returns a JSON representation of the locales information in this object, to feed a Open Flash Chart object.
   * 
   * @return
   *   A string in JSON format representing a chart.
   */
  public function locales_chart() {
    
    return $this->pie_chart(array(
      t('English') => $this->sum_locale_en,
      t('Spanish') => $this->sum_locale_es,
      t('Galician') => $this->sum_locale_gl,
      t('Other') => $this->sum_locale_other,
    ));
  }
    
  /**
   * Returns a JSON representation of the user agents information in this object, to feed a Open Flash Chart object.
   * 
   * @return
   *   A string in JSON format representing a chart.
   */
  public function user_agents_chart() {
    
    return $this->pie_chart(array(
      'Chrome' => $this->sum_user_agent_chrome,
      'FF' => $this->sum_user_agent_ff,
      'IE 7' => $this->sum_user_agent_ie7,
      'IE 8' => $this->sum_user_agent_ie8,
      'Safari' => $this->sum_user_agent_safari,
      t('UnknownOther') => $this->sum_user_agent_unknown,
    ));
  }
  
}
