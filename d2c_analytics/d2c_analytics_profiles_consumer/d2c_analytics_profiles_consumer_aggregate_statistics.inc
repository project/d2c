<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

/**
 * Objects of this class will maintain aggregated profile statistics extracted with a certain
 * query to the profiles statistics table in the database.
 * 
 * They will also offer methods to obtain different representations of that encapsulated information.
 */
class D2CAnalyticsProfilesConsumerAggregateStatistics extends D2CAnalyticsMainConsumerStatistics {

  public $sum_mail_gmail;
  public $sum_mail_hotmail;
  public $sum_mail_yahoo;
  public $sum_mail_other;
  public $sum_mail_unknown;
  public $sum_age_10;
  public $sum_age_20;
  public $sum_age_30;
  public $sum_age_40;
  public $sum_age_50;
  public $sum_age_60;
  public $sum_age_70;
  public $sum_age_80;
  public $sum_age_unknown;
  
  /**
   * Creates a new instance to hold the result of a query to the profiles statistics DB table.
   * 
   * @param $codes
   *   An array with D2CCode instances representing all the selected codes.
   * @param $start
   *   A timestamp indicating the start of the time window.
   * @param $end
   *   A timestamp indicating the end of the time window.
   * 
   * @return
   *   An instance of D2CAnalyticsProfilesConsumerAggregateStatistics or NULL if any problem arised.
   */
  public static function find($codes, $start, $end) {
    foreach($codes as $code) {
      $codes_ids[] = $code->local_id; 
    }
    $cid = md5('d2c_analytics_profiles_consumer_aggregate_statistics' . implode($codes_ids) . $start . $end);
    if (($cached_result = cache_get($cid, 'cache')) && ($cached_result->expire > REQUEST_TIME)) {
      return $cached_result->data;
    }
    $sql = 'SELECT SUM(mail_gmail) AS sum_mail_gmail, SUM(mail_hotmail) AS sum_mail_hotmail,';
    $sql .= ' SUM(mail_yahoo) AS sum_mail_yahoo, SUM(mail_other) AS sum_mail_other, SUM(mail_unknown) AS sum_mail_unknown,';
    $sql .= ' SUM(age_10) AS sum_age_10, SUM(age_20) AS sum_age_20, SUM(age_30) AS sum_age_30, SUM(age_40) AS sum_age_40,';
    $sql .= ' SUM(age_50) AS sum_age_50, SUM(age_60) AS sum_age_60, SUM(age_70) AS sum_age_70, SUM(age_80) AS sum_age_80,';
    $sql .= ' SUM(age_unknown) AS sum_age_unknown';
    $sql .= ' FROM {d2c_analytics_profiles_stats}';
    $sql .= ' WHERE code_local_id IN (:codes_ids) AND timestamp >= :start AND timestamp <= :end';
    $result = db_query($sql, array(':codes_ids' => $codes_ids, ':start' => $start, ':end' => $end));
    if($result && ($row = $result->fetchAssoc())) {
      $stats = new D2CAnalyticsProfilesConsumerAggregateStatistics($row);
      cache_set($cid, $stats, 'cache', D2CAnalyticsMainConsumerStatistics::expire_for_time_window($start, $end));
      return $stats;
    }
    else {
      return NULL;
    }
  }
  
  /**
   * Class constructor.
   *
   * @param $attributes
   *  
   */
  public function __construct($attributes = array()) {
    foreach($attributes as $key => $value) {
      $this->$key = (int) $value;
    }
  }
  
  /**
   * Returns data to fill up a table representation of the e-mail servers information in this object.
   * 
   * @return
   *   An indexed array with a 'headers' element and a 'rows' element.
   */
  public function emails_table() {
    $palette = array_values($this->get_palette());
    $total = $this->sum_mail_gmail + $this->sum_mail_hotmail + $this->sum_mail_yahoo + $this->sum_mail_other + $this->sum_mail_unknown;
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[0] . '">', 'GMail', $this->sum_mail_gmail, $total ? (round($this->sum_mail_gmail * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[1] . '">', 'Hotmail', $this->sum_mail_hotmail, $total ? (round($this->sum_mail_hotmail * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[2] . '">', 'Yahoo', $this->sum_mail_yahoo, $total ? (round($this->sum_mail_yahoo * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[3] . '">', t('Other'), $this->sum_mail_other, $total ? (round($this->sum_mail_other * 100 / $total, 2) . '%') : '-');
    $rows[] = array('<div style="width: 1em; height: 1em; background-color:' . $palette[4] . '">', t('Unknown'), $this->sum_mail_unknown, $total ? (round($this->sum_mail_unknown * 100 / $total, 2) . '%') : '-');
    // Order by total
    foreach ($rows as $row) {
      $totals[] = $row[2];
    }
    array_multisort($totals, SORT_DESC, $rows); 
    return array(
      'headers' => array('', t('E-mail server'), t('Total'), t('Percentage')),
      'rows' => $rows
    );
  }
  
  /**
   * Returns data to fill up a table representation of the ages information in this object.
   * 
   * @return
   *   An indexed array with a 'headers' element and a 'rows' element.
   */
  public function ages_table() {
    $total = $this->sum_age_10 + $this->sum_age_20 + $this->sum_age_30 + $this->sum_age_40 + $this->sum_age_50 + $this->sum_age_60 + $this->sum_age_70 + $this->sum_age_80 + $this->sum_age_unknown;
    $rows[] = array('10..19', $this->sum_age_10, $total ? (round($this->sum_age_10 * 100 / $total, 2) . '%') : '-');
    $rows[] = array('20..29', $this->sum_age_20, $total ? (round($this->sum_age_20 * 100 / $total, 2) . '%') : '-');
    $rows[] = array('30..39', $this->sum_age_30, $total ? (round($this->sum_age_30 * 100 / $total, 2) . '%') : '-');
    $rows[] = array('40..49', $this->sum_age_40, $total ? (round($this->sum_age_40 * 100 / $total, 2) . '%') : '-');
    $rows[] = array('50..59', $this->sum_age_50, $total ? (round($this->sum_age_50 * 100 / $total, 2) . '%') : '-');
    $rows[] = array('60..69', $this->sum_age_60, $total ? (round($this->sum_age_60 * 100 / $total, 2) . '%') : '-');
    $rows[] = array('70..79', $this->sum_age_70, $total ? (round($this->sum_age_70 * 100 / $total, 2) . '%') : '-');
    $rows[] = array('> 79', $this->sum_age_80, $total ? (round($this->sum_age_80 * 100 / $total, 2) . '%') : '-');
    $rows[] = array(t('Unknown'), $this->sum_age_unknown, $total ? (round($this->sum_age_unknown * 100 / $total, 2) . '%') : '-');
    // Order by total
    foreach ($rows as $row) {
      $totals[] = $row[1];
    }
    array_multisort($totals, SORT_DESC, $rows); 
    return array(
      'headers' => array(t('Age range'), t('Total'), t('Percentage')),
      'rows' => $rows
    );
  }
  
  /**
   * Returns a JSON representation of the e-mail servers information in this object, to feed a Open Flash Chart object.
   * 
   * @return
   *   A string in JSON format representing a chart.
   */
  public function emails_chart() {
    
    return $this->pie_chart(array(
      'GMail' => $this->sum_mail_gmail,
      'Hotmail' => $this->sum_mail_hotmail,
      'Yahoo' => $this->sum_mail_yahoo,
      t('Other') => $this->sum_mail_other,      
      t('Unknown') => $this->sum_mail_unknown,
    ));
    
  }
    
  /**
   * Returns a JSON representation of the ages information in this object, to feed a Open Flash Chart object.
   * 
   * @return
   *   A string in JSON format representing a chart.
   */
  public function ages_chart() {

    $palette = $this->palette();
    
    $data = array(
      '10..19' => $this->sum_age_10,
      '20..29' => $this->sum_age_20,
      '30..39' => $this->sum_age_30,
      '40..49' => $this->sum_age_40,
      '50..59' => $this->sum_age_50,
      '60..69' => $this->sum_age_60,
      '70..79' => $this->sum_age_70,
      '> 79' => $this->sum_age_80,
      t('Unknown') => $this->sum_age_unknown
    );
    
    $hbar = new OFC_Charts_Bar_Horizontal();    
    foreach($data as $label => $value) {
      $hbar->append_value(new OFC_Charts_Bar_Horizontal_Value(0, $value));
    }
    $hbar->text = t('Visits');
    
    $chart = new OFC_Chart();
    $chart->set_title(new OFC_Elements_Title(t('Age ranges')));
    $hbar->colour = $palette['top'];
    $chart->add_element($hbar);
    
    $x = new OFC_Elements_Axis_X();
    $max = max($data);
    if ($max < 20) {
      $steps = 1;
    } else {
      $steps = pow(10, (strlen((string) $max) - 1)) / 2;
    }
    $x->set_range(0, $max - ($max % $steps) + $steps, $steps);
    $x->set_colours($palette['text'], $palette['bottom']);
    $chart->set_x_axis($x);
    
    $y = new OFC_Elements_Axis_Y();
    $y->set_offset(TRUE);
    $y->set_labels(array_reverse(array_keys($data)));
    $y->set_colours($palette['text'], $palette['bottom']);
    $chart->add_y_axis($y);
    
    $chart->bg_colour = $palette['background'];
    
    return $chart->toPrettyString();
    
  }
  
}
