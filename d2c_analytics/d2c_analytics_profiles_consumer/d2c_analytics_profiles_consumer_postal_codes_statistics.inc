<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

/**
 * Objects of this class will maintain aggregated postal codes statistics extracted with a certain
 * query to the profiles statistics table in the database.
 * 
 * They will also offer methods to obtain different representations of that encapsulated information.
 */
class D2CAnalyticsProfilesConsumerPostalCodesStatistics extends D2CAnalyticsMainConsumerStatistics {

  public $postal_codes;
  
  /**
   * Creates a new instance to hold the result of a query to the profiles statistics DB table.
   * 
   * @param $codes
   *   An array with D2CCode instances representing all the selected codes.
   * @param $start
   *   A timestamp indicating the start of the time window.
   * @param $end
   *   A timestamp indicating the end of the time window.
   * 
   * @return
   *   An instance of D2CAnalyticsProfilesConsumerPostalCodesStatistics or NULL if any problem arised.
   */
  public static function find($codes, $start, $end) {
    foreach($codes as $code) {
      $codes_ids[] = $code->local_id; 
    }
    $cid = md5('d2c_analytics_profiles_consumer_postal_codes_statistics' . implode($codes_ids) . $start . $end);
    if (($cached_result = cache_get($cid, 'cache')) && ($cached_result->expire > REQUEST_TIME)) {
      return $cached_result->data;
    }
    $sql = 'SELECT postal_codes FROM {d2c_analytics_profiles_stats}';
    $sql .= ' WHERE code_local_id IN (:codes_ids) AND timestamp >= :start AND timestamp <= :end';
    $result = db_query($sql, array(':codes_ids' => $codes_ids, ':start' => $start, ':end' => $end));
    if($result) {
      $postal_codes = array();
      foreach($result as $row) {
        foreach(unserialize($row->postal_codes) as $postal_code => $sum) {
          $postal_codes[$postal_code] = $postal_codes[$postal_code] + $sum;
        }
      }
      $stats = new D2CAnalyticsProfilesConsumerPostalCodesStatistics($postal_codes);
      cache_set($cid, $stats, 'cache', D2CAnalyticsMainConsumerStatistics::expire_for_time_window($start, $end));
      return $stats;
    }
    else {
      return NULL;
    }
  }
  
  /**
   * Class constructor.
   *
   * @param $postal_codes
   *  
   */
  public function __construct($postal_codes = array()) {
    $this->postal_codes = $postal_codes;
  }
  
  /**
   * Returns data to fill up a table representation of the postal codes information in this object.
   * 
   * @return
   *   An indexed array with a 'headers' element and a 'rows' element.
   */
  public function postal_codes_table() {
    $total = array_sum($this->postal_codes);
    $top_postal_codes = $this->top_postal_codes();
    $rows = array();
    foreach($top_postal_codes as $postal_code => $sum) {
      $rows[] = array($postal_code, $sum, $total ? (number_format($sum * 100 / $total, 2) . '%') : '-');
    }
    // Order by total
    foreach ($rows as $row) {
      $totals[] = $row[1];
    }
    array_multisort($totals, SORT_DESC, $rows);
    return array(
      'headers' => array(t('Postal code'), t('Total'), t('Percentage')),
      'rows' => $rows
    );
  }
  
  /**
   * Returns a JSON representation of the e-mail servers information in this object, to feed a Open Flash Chart object.
   * 
   * @return
   *   A string in JSON format representing a chart.
   */
  public function postal_codes_chart() {
    
    $palette = $this->palette();
    
    $data = $this->top_postal_codes();
    
    $hbar = new OFC_Charts_Bar_Horizontal();    
    foreach($data as $label => $value) {
      $hbar->append_value(new OFC_Charts_Bar_Horizontal_Value(0, $value));
    }
    $hbar->text = t('Visits');
    
    $chart = new OFC_Chart();
    $chart->set_title(new OFC_Elements_Title(t('Top postal codes')));
    $hbar->colour = $palette['top'];
    $chart->add_element($hbar);
    
    $x = new OFC_Elements_Axis_X();
    $max = max($data);
    if ($max < 20) {
      $steps = 1;
    } else {
      $steps = pow(10, (strlen((string) $max) - 1)) / 2;
    }
    $x->set_range(0, $max - ($max % $steps) + $steps, $steps);
    $x->set_colours($palette['text'], $palette['bottom']);
    $chart->set_x_axis($x);
    
    $y = new OFC_Elements_Axis_Y();
    $y->set_offset(TRUE);
    $y->set_labels(array_reverse(array_keys($data)));
    $y->set_colours($palette['text'], $palette['bottom']);
    $chart->add_y_axis($y);
    
    $chart->bg_colour = $palette['background'];
    
    return $chart->toPrettyString();
    
  }

  /**
   * Private methods
   */
  
  /**
   * Returns a list with the top postal codes.
   * 
   * @return
   *   An indexed array.
   */
  private function top_postal_codes() {
    $top_postal_codes = array();
    if ($this->postal_codes[0]) {
      $unknown = $this->postal_codes[0];
    } else {
      $unknown = 0;
    }
    foreach($this->postal_codes as $postal_code => $sum) {
      if ($postal_code) {
        $top_postal_codes[$postal_code] = $sum;
      }
    }
    if ($top_postal_codes) {
      arsort($top_postal_codes);
      while (count($top_postal_codes) > 10) {
        array_pop($top_postal_codes);
      }
    }
    $top_postal_codes[t('Unknown')] = $unknown;
    return $top_postal_codes;
  }
  
}
