<?php if ($edit === TRUE) { ?>
  <div class="qr-codes">
  <?php foreach ($codes as $code) { ?>
    <div class="d2c-node-qr-code">
      <img class="d2c-qr-code" id="d2c-qr-code-<?php print $code->local_id ?>" src="<?php print d2c_settings_code_image_path($code) ?>" />
      <div><strong><?php print t('Name') ?>:</strong> <?php print $code->name ?></div>
      <div><strong><?php print t('Code type') ?>:</strong> <?php $types = D2CApi::code_types(); print $types[$code->code_type] ?></div>
      <div class="buttons">
        <a href="<?php print url('admin/config/d2c/codes/' . $code->local_id . '/edit', array('query'=>drupal_get_destination())) ?>">edit</a> ·
        <?php if ($code->active) { ?>
          <a href="#" onclick="(function ($) { $('#d2c_node_toggle_visibility_code_form_<?php print $code->local_id ?>').submit() })(jQuery);"><?php print($code->visible ? t('hide on node body') : t('display on node body')) ?></a> ·
        <?php } else { ?>
          <a href="#" onclick="(function ($) { $('#d2c_node_toggle_activation_code_form_<?php print $code->local_id ?>').submit() })(jQuery);"><?php print t('activate') ?></a> ·
        <?php } ?>
        <br/>
        <a href="#" onclick="(function ($) { $('#d2c_node_delete_code_form_<?php print $code->local_id ?>').submit() })(jQuery);"><?php print(t('delete from list')) ?></a>
        <?php if (module_exists('d2c_analytics_main_consumer')) { ?>
          · <a href="<?php print url('admin/config/d2c/stats', array('query' => array('type' => 'code', 'id' => $code->local_id))) ?>">statistics</a>
        <?php } ?>
        <form id="d2c_node_toggle_visibility_code_form_<?php print $code->local_id ?>" action="<?php print url('node/' . $node->nid . '/d2c_codes/' . $code->local_id . '/toggle_visibility') ?>" method="post"></form>
        <form id="d2c_node_toggle_activation_code_form_<?php print $code->local_id ?>" action="<?php print url('admin/config/d2c/codes/' . $code->local_id . '/toggle_activation') ?>" method="post">
          <input type="hidden" name="destination" value="<?php print $_GET['q'] ?>" />
        </form>
        <form id="d2c_node_activate_code_form_<?php print $code->local_id ?>" action="<?php print url('node/' . $node->nid . '/d2c_codes/' . $code->local_id .'/activate') ?>" method="post"></form>
        <form id="d2c_node_delete_code_form_<?php print $code->local_id ?>" action="<?php print url('node/' . $node->nid . '/d2c_codes/' . $code->local_id .'/unlink') ?>" method="post"></form>
      </div>
    </div>
  <?php } ?>
  </div>
<?php } else {
  $icon = base_path() . drupal_get_path('module','d2c_core') . '/assets/d2c_icon.png';
?>
  <div class="attached-qr-codes">
    <div class="title"><?= t('Attached QR codes') ?></div>
    <?php foreach ($codes as $code ) { ?>
      <div class="d2c-node-qr-code" id="d2c-qr-code-<?php print $code->local_id ?>">
        <a href="<?php print d2c_settings_code_image_path($code) ?>">
          <img src="<?php print $icon ?>" class="d2c-icon"/>
          <img src="<?php print d2c_settings_code_image_path($code) ?>" class="d2c-qr-code" />
          <span class="d2c-qr-code-name"><?php print $code->name ?></span>
        </a>
      </div>
    <?php } ?>
  </div>
<?php } ?>  
