<?php

/**
 *
 * @copyright 2011, (c) dot2code Technologies S.L.
 *
 */


/**
 * Implements hook_block_info().
 *
 * This hook declares what blocks are provided by the module.
 */
function d2c_node_block_info() {
  $blocks['d2c_node_attached_codes'] = array(
    'info' => t('Visible QR codes attached to the node'),
    'cache' => DRUPAL_CACHE_GLOBAL,
    'status' => 1,
    'region' => 'sidebar_first',
  );
  return $blocks;
}

/**
 * Implements hook_block_configure().
 *
 * This hook declares configuration options for blocks provided by this module.
 */
function d2c_node_block_configure($delta = '') {
  $form = array();
  switch ($delta) {
    case 'd2c_node_attached_codes':
      $form['d2c_node_attached_codes_block_code_width'] = array(
        '#type' => 'textfield',
        '#title' => t('QR code width'),
        '#default_value' => d2c_node_get_attached_codes_block_code_width(),
        '#element_validate' => array('d2c_node_validate_attached_codes_block_code_width'),
        '#description' => t('Override the default width in pixels of the QR codes displayed in the block, or leave blank to use the default width.'),
      );
      $form['d2c_node_attached_codes_block_display_names_enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('Display code names'),
        '#default_value' => d2c_node_attached_codes_block_display_names_enabled(),
        '#description' => t('When a node has more than one visible & active QR code attached, display code names next to the QR code images.'),
      );
      break;
  }
  return $form;
}

/**
 * 
 */
function d2c_node_validate_attached_codes_block_code_width($element, &$form_state) {
  if (!empty($element['#value']) && !ctype_digit($element['#value'])) {
    form_error($element, t('The QR code width value should be empty or a valid integer value.'));
  }
}

/**
 * Implements hook_block_save().
 *
 * This hook declares how the configured options for a block
 * provided by this module are saved.
 */
function d2c_node_block_save($delta = '', $edit = array()) {
  switch ($delta) {
    case 'd2c_node_attached_codes':
      d2c_node_set_attached_codes_block_code_width($edit['d2c_node_attached_codes_block_code_width']);
      d2c_node_set_attached_codes_block_display_names_enabled($edit['d2c_node_attached_codes_block_display_names_enabled']);
      break;
  }
}

/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function d2c_node_block_view($delta = '') {
  switch ($delta) {
    case 'd2c_node_attached_codes':
      $node = menu_get_object();
      if ($node) {
        $codes = d2c_node_get_visible_codes($node);
        if (count($codes) > 0) {
          $block['subject'] = t('QR codes');
          $block['content'] = array(
            '#theme' => 'd2c_node_attached_codes_block', // view d2c_node_theme()
            '#codes' => $codes,
            '#code_width' => d2c_node_get_attached_codes_block_code_width(),
            '#display_names' => d2c_node_attached_codes_block_display_names_enabled(),
          );
          return $block;
        }
      }
      break;
  }
}

/**
 * 
 */

function d2c_node_get_attached_codes_block_code_width() {
  return variable_get('d2c_node_attached_codes_block_code_width', '150');
}

function d2c_node_set_attached_codes_block_code_width($width) {
  return variable_set('d2c_node_attached_codes_block_code_width', $width);
}

function d2c_node_delete_attached_codes_block_code_width() {
  variable_del('d2c_node_attached_codes_block_code_width');
}

/**
 * 
 */

function d2c_node_attached_codes_block_display_names_enabled() {
  return variable_get('d2c_node_attached_codes_block_display_names_enabled', TRUE);
}

function d2c_node_set_attached_codes_block_display_names_enabled($enabled) {
  return variable_set('d2c_node_attached_codes_block_display_names_enabled', $enabled);
}

function d2c_node_delete_attached_codes_block_display_names_enabled() {
  variable_del('d2c_node_attached_codes_block_display_names_enabled');
}
