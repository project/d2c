<?php

/**
 *
 * @copyright 2010, (c) dot2code Technologies S.L.
 *
 */

class D2CNodeCodeLinks {

  public static function links($nid) {
    $links = array();
    $result = db_query("SELECT * from {d2c_node_code_links} WHERE nid = :nid", array(':nid' => $nid));
    foreach($result as $link){
      $links[] = $link;
    }
    return $links;
  }
  
  public static function linked_codes($nid, $only_visible = FALSE) {
    $code_ids = array();
    $links = D2CNodeCodeLinks::links($nid);
    foreach ($links as $link) {
      if (!$only_visible || $link->visible) {
        $code_ids[] = $link->code_id;
      }
    }
    $codes = D2CCode::find($code_ids);    
    foreach($links as $link) {
      if ($codes[$link->code_id]) {
        $codes[$link->code_id]->visible = $link->visible;
      } 
    }
    return $codes;
  }
  
  public static function link($nid, $code_id) {
    $link = db_query("SELECT * from {d2c_node_code_links} WHERE nid = :nid AND code_id = :code_id", array(':nid' => $nid, 'code_id' => $code_id))->fetchObject();
    if ($link) {
      return $link;
    }
    else {
      return NULL;
    }
  }
  
  public static function add_link($nid, $code_id, $visible = FALSE) {
    $link = (object) array('nid' => $nid, 'code_id' => $code_id, 'visible' => $visible);
    if (!D2CNodeCodeLinks::link($nid, $code_id)) {
      drupal_write_record('d2c_node_code_links', $link);
    }
  }
  
  public static function update_link($nid, $code_id, $visible) {
    $link = (object) array('nid' => $nid, 'code_id' => $code_id, 'visible' => $visible);
    drupal_write_record('d2c_node_code_links', $link, array('nid', 'code_id'));
  }
  
  public static function delete_link($nid, $code_id) {
    db_delete('d2c_node_code_links')->condition('nid', $nid)->condition('code_id', $code_id)->execute();
  }
  
  public static function delete_links($nid) {
    db_delete('d2c_node_code_links')->condition('nid', $nid)->execute();
  }

  public static function delete_links_for_code($code_id) {
    db_delete('d2c_node_code_links')->condition('code_id', $code_id)->execute();
  }
  
}
