(function ($) {
  Drupal.behaviors.d2cNodeBehavior = {
    attach: function(context){
    
      $('div.attached-qr-codes div.d2c-node-qr-code').hover(function(){
        $(this).find('img.d2c-qr-code').show();
      }, function(){
        $(this).find('img.d2c-qr-code').hide();
      });
    }
  };
})(jQuery);