<div class="qr-codes">
<?php
foreach ($codes as $code) {
  if (($display_names) && (count($codes) > 1)) {
    echo '<h2>' . $code->name . '</h2>';
  }
  $width = '';
  if (!empty($code_width)) {
    $width = 'width="' . $code_width . '"';
  }
?>
  <div class="d2c-node-block-qr-code">
    <img <?= $width ?> src="<?= d2c_settings_code_image_path($code) ?>"/>
  </div>
<?php
}
?>
</div>
